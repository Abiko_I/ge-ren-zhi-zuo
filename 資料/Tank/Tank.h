#pragma once
#include "Engine/GameObject/GameObject.h"

//戦車を管理するクラス
class Tank : public IGameObject
{
	const float MOVE_SPEED;			//戦車の移動速度
	const float ROTATE_SPEED;		//戦車の旋回速度
	int _hModel;    //モデル番号

	//キー入力に対する行動
	void Commando();

public:
	//コンストラクタ
	Tank(IGameObject* parent);

	//デストラクタ
	~Tank();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	void Collision();


	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;
};