#include "Tank.h"
#include "TankCannon.h"
#include "Ground.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Tank::Tank(IGameObject * parent)
	:IGameObject(parent, "Tank"), 
	_hModel(-1),
	MOVE_SPEED(0.2f),
	ROTATE_SPEED(2)
{
}

//デストラクタ
Tank::~Tank()
{
}

//初期化
void Tank::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/TankBody.fbx");
	assert(_hModel >= 0);

	CreateGameObject<TankCannon>(this);			//砲台を戦車本体の子供にする　=　本体と砲台を一つに

	//当たり判定の材料（コライダー）
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 0.5, 0), D3DXVECTOR3(1, 1, 1));
	AddCollider(collision);
}

//更新
void Tank::Update()
{
	//キー入力に対する行動
	Commando();

	//地面との衝突判定
	Collision();
}

//地面との衝突判定
void Tank::Collision()
{
	//地面との当たり判定
	Ground *pGround = (Ground*)FindObject("Ground");	//地面
	int hGroundModel = pGround->GetHModel();

	if (hGroundModel != -1)
	{
		RayCastData data;					 //構造体
		data.start = _position;              //レイの発射位置
		data.start.y = 0;
		data.dir = D3DXVECTOR3(0, -1, 0);    //レイの方向
		Model::RayCast(hGroundModel, &data);			 //レイを発射

		if (data.hit == true)
		{
			_position.y = -data.dist;
		}
	}
}

//コマンド
void Tank::Commando()
{
	if (Input::IsKey(DIK_W))
	{
		//奥に移動するべクトル
		D3DXVECTOR3 move = D3DXVECTOR3(0, 0, MOVE_SPEED);

		//_rotate.y°回転させるための行列
		D3DXMATRIX mat;									//行列を入れる変数mat
		D3DXMatrixRotationY(&mat, D3DXToRadian(_rotate.y));			//rotate.y分だけ回転しろ　という命令

		D3DXVec3TransformCoord(&move, &move, &mat);		//行列を使ってmoveベクトルを回転

		_position += move;				//移動
	}
	if (Input::IsKey(DIK_S))
	{
		D3DXVECTOR3 move = D3DXVECTOR3(0, 0, MOVE_SPEED);

		D3DXMATRIX mat;
		D3DXMatrixRotationY(&mat, D3DXToRadian(_rotate.y));

		D3DXVec3TransformCoord(&move, &move, &mat);

		_position -= move;
	}

	if (Input::IsKey(DIK_A))
	{
		_rotate.y -= ROTATE_SPEED;						//回転

		D3DXVECTOR3();
	}
	if (Input::IsKey(DIK_D))
	{
		_rotate.y += ROTATE_SPEED;
	}
	if (Input::IsKeyDown(DIK_M))
	{
		_scale = D3DXVECTOR3(0.2f, 0.2f, 0.2f);			//小さくなる
	}
	if (Input::IsKeyUp(DIK_B) || Input::IsKeyUp(DIK_M))
	{
		_scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);			//元の大きさに
	}
	if (Input::IsKeyDown(DIK_B))
	{
		_scale = D3DXVECTOR3(10.0f, 10.0f, 10.0f);		//大きくなる
	}
}

//描画
void Tank::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Tank::Release()
{
}

// 何かに当たった
void Tank::OnCollision(IGameObject * pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "Enemy")     //本来は文字列は==では比較できない
	{
		KillMe();
		pTarget->KillMe();

		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
	}
}