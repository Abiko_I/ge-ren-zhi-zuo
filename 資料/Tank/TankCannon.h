#pragma once
#include "Engine/GameObject/GameObject.h"

//戦車上部を管理するクラス
class TankCannon : public IGameObject
{
	int _hModel;    //モデル番号
	const float CANNON_ROTATE_SPEED;		//砲台の旋回速度
	int _hSound;    //サウンド番号

	//射撃
	void Shot();

	//砲塔回転
	void Move();

public:
	//コンストラクタ
	TankCannon(IGameObject* parent);

	//デストラクタ
	~TankCannon();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};