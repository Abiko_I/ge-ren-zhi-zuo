#pragma once

#include "Engine/global.h"

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	int _hModel;    //モデル番号

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};