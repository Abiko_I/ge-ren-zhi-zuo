#include "TitleScene.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"),
	_hPict(-1)
{
}

//初期化
void TitleScene::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/Picts/title.jpg");
	assert(_hPict >= 0);
}

//更新
void TitleScene::Update()
{
	//スペースキーが押されていたら
	if (Input::IsKeyDown(DIK_SPACE))    //キーコードはDIK_○○○という形で表される
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");    //FindObject:探す
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void TitleScene::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void TitleScene::Release()
{
}