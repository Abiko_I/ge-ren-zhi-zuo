#include "PlayScene.h"
#include "Tank.h"
#include "TankCannon.h"
#include "Ground.h"
#include "Enemy.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), _hModel(-1)
{
}

//初期化
void PlayScene::Initialize()
{
	CreateGameObject<Tank>(this);
	CreateGameObject<Ground>(this);

	//敵生産
	for (int i = 0; i < 1; i++)
	{
		CreateGameObject<Enemy>(this);
	}
}

//更新
void PlayScene::Update()
{
	if (Enemy::count == 0)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_CLEAR);
	}
}

//描画
void PlayScene::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void PlayScene::Release()
{
}