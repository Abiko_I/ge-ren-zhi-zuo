#pragma once
#include "Engine/GameObject/GameObject.h"

//敵を管理するクラス
class Enemy : public IGameObject
{
	int _hModel;    //モデル番号

public:
	static int count;   //敵の数

	//コンストラクタ
	Enemy(IGameObject* parent);

	//デストラクタ
	~Enemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};