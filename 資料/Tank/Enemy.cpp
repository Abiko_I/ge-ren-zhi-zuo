#include "Enemy.h"
#include "Ground.h"
#include "Engine/ResouceManager/Model.h"

int Enemy::count = 0;

//コンストラクタ
Enemy::Enemy(IGameObject * parent)
	:IGameObject(parent, "Enemy"), 
	_hModel(-1)
{
	count++;
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/Slime.fbx");
	assert(_hModel >= 0);

	//当たり判定の材料（コライダー）
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0.5, 0), 0.8f);
	AddCollider(collision);

	//敵をランダムに配置
	_position.x = (float)(rand() % 201 - 100) / 10;
	_position.y = 0;
	_position.z = (float)(rand() % 201 - 100 ) / 10;
}

//更新
void Enemy::Update()
{
	//地面との当たり判定
	Ground *pGround = (Ground*)FindObject("Ground");	//地面
	int hGroundModel = pGround->GetHModel();

	if (hGroundModel != -1)
	{
		RayCastData data;					 //構造体
		data.start = _position;              //レイの発射位置
		data.start.y = 0;
		data.dir = D3DXVECTOR3(0, -1, 0);    //レイの方向
		Model::RayCast(hGroundModel, &data);			 //レイを発射

		if (data.hit == true)
		{
			_position.y = -data.dist;
		}
	}
}

//描画
void Enemy::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Enemy::Release()
{
	count--;
}