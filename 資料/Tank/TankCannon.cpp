#include "TankCannon.h"
#include "Bullet.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/gameObject/Camera.h"
#include "Engine/ResouceManager/Audio.h"

//コンストラクタ
TankCannon::TankCannon(IGameObject * parent)
	:IGameObject(parent, "TankCannon"),
	_hModel(-1),
	CANNON_ROTATE_SPEED(2),
	_hSound(-1)
{
}

//デストラクタ
TankCannon::~TankCannon()
{
}

//初期化
void TankCannon::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/TankCannon.fbx");
	assert(_hModel >= 0);

	//サウンドデータのロード
	_hSound = Audio::Load("data/Sounds/Laser_Shoot3.wav");
	assert(_hSound >= 0);

	Camera* pCamera = CreateGameObject<Camera>(this);   //カメラを子供にして戦車の方向と視点をあわせる
	pCamera->SetPosition(D3DXVECTOR3(0, 2, -3));
	pCamera->SetTarget(D3DXVECTOR3(0, 1, 0));
}

//更新
void TankCannon::Update()
{
	//移動
	Move();

	//射撃
	Shot();
}

//射撃
void TankCannon::Shot()
{
	if (Input::IsKeyDown(DIK_SPACE))
	{
		Bullet* pBullet;
		pBullet = CreateGameObject<Bullet>(FindObject("PlayScene"));

		D3DXVECTOR3 shotPos = Model::GetBonePosition(_hModel, "ShotPoint");		//砲台の先端の位置を取得
		D3DXVECTOR3 cannonRoot = Model::GetBonePosition(_hModel, "cannonRoot");
		D3DXVECTOR3 move = shotPos - cannonRoot;

		pBullet->SetPosition(shotPos);
		pBullet->SetMove(move);

		Audio::Play(_hSound);
	}
}

//砲台の回転
void TankCannon::Move()
{
	if (Input::IsKey(DIK_LEFT))
	{
		_rotate.y -= CANNON_ROTATE_SPEED;
	}
	if (Input::IsKey(DIK_RIGHT))
	{
		_rotate.y += CANNON_ROTATE_SPEED;
	}
}

//描画
void TankCannon::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void TankCannon::Release()
{
}