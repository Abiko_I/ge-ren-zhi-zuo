#pragma once
#include "Engine/GameObject/GameObject.h"

//弾を管理するクラス
class Bullet : public IGameObject
{

	int _hModel;    //モデル番号
	D3DXVECTOR3 _move;	//移動ベクトル
	int _hSound;    //サウンド番号

public:
	//コンストラクタ
	Bullet(IGameObject* parent);

	//デストラクタ
	~Bullet();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;

	//移動ベクトルのセッター
	void SetMove(D3DXVECTOR3 move)
	{
		D3DXVec3Normalize(&_move, &move);	//正規化 ベクトルを１に
		_move *= 1.2;
	}
};