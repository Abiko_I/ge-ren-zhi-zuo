#include "Bullet.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Audio.h"

//コンストラクタ
Bullet::Bullet(IGameObject * parent)
	:IGameObject(parent, "Bullet"),
	_hModel(-1),
	_move(D3DXVECTOR3(0,0,0)),
	_hSound(-1)
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/Bullet.fbx");
	assert(_hModel >= 0);

	//サウンドデータのロード
	_hSound = Audio::Load("data/Sounds/Explosion.wav");
	assert(_hSound >= 0);

	//球体コライダーをつける
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 0.3f);
	AddCollider(collision);
}

//更新
void Bullet::Update()
{
	if (_position.y < -5)
	{
		KillMe();
	}

	_position += _move * 0.5;
	_move.y -= 0.05;
}

//描画
void Bullet::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Bullet::Release()
{
}

//何かに当たった
void Bullet::OnCollision(IGameObject * pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "Enemy")     //本来は文字列は==では比較できない
	{
		Audio::Play(_hSound);
		KillMe();
		pTarget->KillMe();
	}
}