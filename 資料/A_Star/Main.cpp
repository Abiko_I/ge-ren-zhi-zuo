#include <iostream>
#include <vector>
using namespace std;

#define HEIGHT 15
#define WIDTH 20

//表示する配列
int map[HEIGHT][WIDTH] = {
{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
{ 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1 },
{ 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1 },
{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
{ 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1 },
{ 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1 },
{ 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1 },
{ 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1 },
{ 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1 },
{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
{ 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1 },
{ 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1 },
{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
};

//探索用
int pos[4][2] = {
	{-1, 0},
	{0, -1},
	{0, 1},
	{1, 0}
};

//マジックナンバー防止
enum
{
	ROAD,		//道
	WALL,		//壁
	START,		//始点
	END,		//終点
	SIGN,		//探索済み
	FINISHED,	//探索終了
	MARK		//通る
};

//位置、歩数、推定値、実歩数をまとめたもの
struct Info
{
	int y;			//縦
	int x;			//横
	int dist;		//歩数
	int est;		//推定値
	int realDist;	//推定歩数
	int prevY;		//自分の一つ前のy
	int prevX;		//自分の一つ前のx
};

//表示するマーク：	   道、  壁、 始点、終点、探索済み、探索終了、通る
const char* mark[] = {"□", "■", "◇", "△",	"○",	  "☆",	  "◎"};

//描画
void draw();
//探索
void search(Info currentPos, int eY, int eX, vector<Info>* openList, vector<Info>* closeList);

void Search(Info currentPos, int eY, int eX, vector<Info>* openList, vector<Info>* closeList);

void ReverseSearch(Info currentPos, vector<Info>* closeList);

int main()
{
	//開始位置のY
	int sY = 2;
	//開始位置のX
	int sX = 2;
	//終了位置のY
	int eY = 12;
	//終了位置のX
	int eX = 12;
	//初期コスト
	int cost = 0;

	cout << "Yは1から" << HEIGHT << "の範囲\n";
	cout << "始点Y：";
	cin >> sY;
	cout << "始点X：";
	cin >> sX;

	cout << "Yは1から" << WIDTH << "の範囲\n";
	cout << "終点Y：";
	cin >> eY;
	cout << "終点X：";
	cin >> eX;

	system("pause");

	for (int i = 0; i < 15; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			//map[i][j] = 0;
		}
	}

	//計算中のマスたちを格納
	vector<Info> openList;

	//計算済みのマスたち
	vector<Info> closeList;

	map[sY][sX] = START;
	map[eY][eX] = END;


	//表示
	draw();

	//開始地点
	Info currentPos = {sY, sX, 0, 0, 0, 0, 0};

	//開始地点を計算中リストに格納
	//openList.push_back(currentPos);

	//終点に着いていないのなら
	if (!(currentPos.y == eY && currentPos.x == eX))
	{
		//探索済みリストに追加
		//closeList.push_back(currentPos);

		//初期値を調査中リストに追加
		openList.push_back(currentPos);
	}

	//探索
	//search(currentPos, eY, eX, &openList, &closeList);
	Search(currentPos, eY, eX, &openList, &closeList);

	system("pause");
	return 0;
}

//描画
void draw()
{
	system("cls");

	for (int i = 0; i < 15; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			cout << mark[map[i][j]];
		}
		cout << "\n";
	}
	cout << "\n";
}

//探索
void search(Info currentPos, int eY, int eX, vector<Info>* openList, vector<Info>* closeList)
{
	//上下左右の位置を記録する
	Info nextPos[4];

	for (int i = 0; i < 4; i++)
	{
		//有り得ない値で初期化
		nextPos[i].y = -1;
		nextPos[i].x = -1;
		nextPos[i].dist = 9999;
		nextPos[i].est = 9999;
		nextPos[i].realDist = 9999;
		nextPos[i].prevX = -1;
		nextPos[i].prevY = -1;
	}

	//再帰処理をする時に引数として渡す
	Info retPos = {0, 0, 0, 0, 9999, 0, 0};

	//上下左右を探索
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			if (j == 0)
			{
				nextPos[i].y = currentPos.y + pos[i][j];
			}
			else
			{
				nextPos[i].x = currentPos.x + pos[i][j];
			}
		}

		//始点、終点、壁以外なら印をつける
		if(map[nextPos[i].y][nextPos[i].x] != START &&
			map[nextPos[i].y][nextPos[i].x] != END &&
			map[nextPos[i].y][nextPos[i].x] != WALL)
		{
			map[nextPos[i].y][nextPos[i].x] = SIGN;

			//実歩数を計算
			nextPos[i].dist = currentPos.dist + 1;

			//int a = abs(nextPos[i].y);

			//目的地までの推定値を計算
			nextPos[i].est = abs(nextPos[i].y - eY) + abs(nextPos[i].x - eX);

			//目的地までの推定歩数を計算
			nextPos[i].realDist = nextPos[i].dist + nextPos[i].est;

			bool isExistOpen = false;
			bool isExistClose = false;

			auto openIt = openList->begin();
			auto closeIt = closeList->begin();

			//計算中リストに、今見ている位置と同じ情報があるなら
			for (openIt; openIt < openList->end(); openIt++)
			{
				if ((openIt->x == nextPos[i].x) && (openIt->y == nextPos[i].y))
				{
					//計算中リストにある
					isExistOpen = true;

					//元々の位置の実歩数より、今の位置の実歩数が小さい時
					if (openIt->realDist > nextPos[i].realDist)
					{
						//一旦削除して
						openList->erase(openIt);

						//もう一度追加
						openList->push_back(nextPos[i]);
					}
				}
			}
			//探索済みリストに、今見ている位置と同じ情報があるなら
			for (closeIt; closeIt < closeList->end(); closeIt++)
			{
				if ((closeIt->x == nextPos[i].x) && (closeIt->y == nextPos[i].y))
				{
					//探索済みリストにある
					isExistClose = true;

					//元々の位置の実歩数より、今の位置の実歩数が小さい時
					if (closeIt->realDist > nextPos[i].realDist)
					{
						//計算中リストに追加
						openList->push_back(nextPos[i]);
					}
				}
			}

			//計算中リストにも、探索済みリストにもないなら
			if ((!isExistOpen) && (!isExistClose))
			{
				//計算中リストに追加
				openList->push_back(nextPos[i]);
			}

			//必ず四回ではないのでは
			for (int i = 0; i < 4; i++)
			{
				//現在見ている情報の推定歩数が
				//引数として使おうとしている情報の中の推定歩数より小さいなら
				if (retPos.realDist > nextPos[i].realDist)
				{
					//引数として使う情報を更新
					retPos = nextPos[i];
				}
			}


			//終点に着いたのなら
			if (retPos.y == eY && retPos.x == eX)
			{
				//関数を終了
				return;
			}
			//まだ終点ではないのなら
			else
			{
				//再帰呼び出し
				search(retPos, eY, eX, openList, closeList);
			}
		}

		//表示
		draw();
	}
}

void Search(Info currentPos, int eY, int eX, vector<Info>* openList, vector<Info>* closeList)
{
	//上下左右の位置を記録する
	Info nextPos[4];

	for (int i = 0; i < 4; i++)
	{
		//有り得ない値で初期化
		nextPos[i].y = -1;
		nextPos[i].x = -1;
		nextPos[i].dist = 9999;
		nextPos[i].est = 9999;
		nextPos[i].realDist = 9999;
		nextPos[i].prevX = -1;
		nextPos[i].prevY = -1;
	}

	//再帰処理をする時に引数として渡す
	Info retPos = { 0, 0, 0, 0, 9999, 0, 0 };

	//上下左右を探索
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			//上下
			if (j == 0)
			{
				nextPos[i].y = currentPos.y + pos[i][j];
			}
			//左右
			else
			{
				nextPos[i].x = currentPos.x + pos[i][j];
			}
		}

		bool isExistOpen = false;
		bool isExistClose = false;

		//計算中リストに、今見ている位置と同じ情報があるなら
		for (auto it = openList->begin(); it < openList->end(); it++)
		{
			if ((it->x == nextPos[i].x) && (it->y == nextPos[i].y))
			{
				//計算中リストにある
				isExistOpen = true;
			}
		}
		//探索済みリストに、今見ている位置と同じ情報があるなら
		for (auto it = closeList->begin(); it < closeList->end(); it++)
		{
			if ((it->x == nextPos[i].x) && (it->y == nextPos[i].y))
			{
				//探索済みリストにある
				isExistClose = true;
			}
		}

		//始点、壁、調査中リストにない、調査済みリストにもない位置
		if (map[nextPos[i].y][nextPos[i].x] != START &&
			map[nextPos[i].y][nextPos[i].x] != WALL &&
			!isExistOpen &&
			!isExistClose)
		{
			//終点なら
			if (map[nextPos[i].y][nextPos[i].x] == END)
			{
				//探索終了
				map[nextPos[i].y][nextPos[i].x] = FINISHED;
			}
			//それ以外は
			else
			{
				//探索した
				map[nextPos[i].y][nextPos[i].x] = SIGN;
			}

			//実歩数を計算
			nextPos[i].dist = currentPos.dist + 1;

			//目的地までの推定値を計算
			nextPos[i].est = abs(nextPos[i].y - eY) + abs(nextPos[i].x - eX);

			//目的地までの推定歩数を計算
			nextPos[i].realDist = nextPos[i].dist + nextPos[i].est;

			//前の位置を保存
			nextPos[i].prevX = currentPos.x;
			nextPos[i].prevY = currentPos.y;

			//調査中リストに追加
			openList->push_back(nextPos[i]);

			//表示
			draw();
		}
	}

	//上下左右を探索したら、調査終了
	closeList->push_back(currentPos);

	//計算中リストから同じ位置を探して
	for (auto it = openList->begin(); it < openList->end(); it++)
	{
		if ((it->x == currentPos.x) && (it->y == currentPos.y))
		{
			//調査中リストから削除
			openList->erase(it);
			break;
		}
	}

	//計算中リスト中の最小推定実歩数のものを選択
	for (auto it = openList->begin(); it < openList->end(); it++)
	{
		//現在見ている情報の推定実歩数が
		//引数として使おうとしている情報の中の推定歩数より小さいなら
		if (retPos.realDist > it->realDist)
		{
			//引数として使う情報を更新
			retPos = *it;
		}
	}

	//終点に着いたのなら
	if (retPos.y == eY && retPos.x == eX)
	{
		//終点から逆探索する
		ReverseSearch(retPos, closeList);

		//関数を終了
		return;
	}
	//まだ終点ではないのなら
	else
	{
		//再帰呼び出し
		Search(retPos, eY, eX, openList, closeList);
	}
}

void ReverseSearch(Info currentPos, vector<Info>* closeList)
{
	//始点なら
	if (map[currentPos.y][currentPos.x] == START)
	{
		return;
	}
	else
	{
		map[currentPos.y][currentPos.x] = MARK;
	}

	//計算中リストから同じ位置を探して
	for (auto it = closeList->begin(); it < closeList->end(); it++)
	{
		if ((it->x == currentPos.prevX) && (it->y == currentPos.prevY))
		{
			//再帰で渡す引数を更新
			currentPos = *it;
		}
	}

	//表示
	draw();

	//再帰呼び出し
	ReverseSearch(currentPos, closeList);
}