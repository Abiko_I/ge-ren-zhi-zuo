#pragma once
#include "Engine/GameObject/GameObject.h"

//タイトル画面を管理するクラス
class TitlePict : public IGameObject
{
	int _hPict;    //画像番号

public:
	//コンストラクタ
	TitlePict(IGameObject* parent);

	//デストラクタ
	~TitlePict();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};