#include "Player.h"
#include "Engine/ResouceManager/Model.h"
#include "Bullet.h"
#include "Engine/gameObject/Camera.h"

//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player"), _hModel(-1)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Player.fbx");
	assert(_hModel >= 0);

	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1, 1, 1));  //箱型コライダー
	AddCollider(collision);

	//カメラ
	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 3, -5));
	pCamera->SetTarget(D3DXVECTOR3(0, 2.5, 0));
}

//更新
void Player::Update()
{
	//右が押されていたら
	if (Input::IsKey(DIK_RIGHT))
	{
		_position.x += 0.4;
	}
	//左が押されていたら
	if (Input::IsKey(DIK_LEFT))
	{
		_position.x -= 0.4;
	}

	//スペースが押されていたら
	if (Input::IsKeyDown(DIK_SPACE))
	{
		IGameObject* pBullet;
		pBullet = CreateGameObject<Bullet>(GetParent());
		pBullet->SetPosition(_position);
	}

}

//描画
void Player::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Player::Release()
{
}

// 何かに当たった
void Player::OnCollision(IGameObject * pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "Enemy")     //本来は文字列は==では比較できない
	{
		KillMe();
		pTarget->KillMe();

		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
	}
}