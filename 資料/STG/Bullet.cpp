#include "Bullet.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Bullet::Bullet(IGameObject * parent)
	:IGameObject(parent, "Bullet"), _hModel(-1)
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Bullet.fbx");
	assert(_hModel >= 0);

	//球体コライダーをつける
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 0.3f);
	AddCollider(collision);
}

//更新
void Bullet::Update()
{
	_position.z += 3;
	if ( _position.z > 100 )
	{
		KillMe();
	}
}

//描画
void Bullet::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Bullet::Release()
{
}

//何かに当たった
void Bullet::OnCollision(IGameObject * pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "Enemy")     //本来は文字列は==では比較できない
	{
		KillMe();
		pTarget->KillMe();
	}
}