#pragma once

#include "Engine/global.h"

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	int _hPict;    //画像番号

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化(ファイルをロードして準備)
	void Initialize() override;

	//更新
	void Update() override;

	//描画（表示）
	void Draw() override;

	//開放（シーンの終わり）
	void Release() override;
};