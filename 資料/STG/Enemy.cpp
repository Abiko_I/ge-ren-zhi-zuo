#include "Enemy.h"
#include "Engine/ResouceManager/Model.h"
#include "EnemyBullet.h"

int Enemy::count = 0;

//コンストラクタ
Enemy::Enemy(IGameObject * parent)
	:IGameObject(parent, "Enemy"), _hModel(-1)
{
	count++;
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Enemy.fbx");
	assert(_hModel >= 0);

	//敵をランダムに配置
	_position.x = (float)(rand() % 401 - 100) / 10;
	_position.y = 0;
	_position.z = (float)(rand() % 201 + 750) / 10;
	
	//球体コライダーをつける
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 1.2f);  //1.2f = 半径
	AddCollider(collision);     //Add = 追加
}

//更新
void Enemy::Update()
{
	int i;

	_position.z -= 0.5;
	
	/*
	if (_position.z > 10)
	{
		_position.x += 0.1;
	}
	*/

	if ( ( rand() % 100 ) == 0 )
	{
		IGameObject* pBullet;
		pBullet = CreateGameObject<EnemyBullet>(GetParent());
		pBullet->SetPosition(_position);
	}

	if ( _position.z < -5 )
	{
		//プレイヤーより後ろに進んだら敵を再配置
		_position.x = (float)(rand() % 401 - 100) / 10;
		_position.y = 0;
		_position.z = (float)(rand() % 201 + 750) / 10;
	
	}
}

//描画
void Enemy::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Enemy::Release()
{
	count--;
}