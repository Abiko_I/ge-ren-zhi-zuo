#include "TitleScene.h"
#include "TitlePict.h"

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene")
{
}

//初期化 1/60秒で呼ばれる
void TitleScene::Initialize()
{
	CreateGameObject<TitlePict>(this);   //this = タイトルシーンが親
}

//更新 60秒に一回呼ばれる
void TitleScene::Update()
{
	//スペースキーが押されていたら
	if (Input::IsKeyDown(DIK_SPACE))    //キーコードはDIK_○○○という形で表される
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");    //FindObject:探す
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void TitleScene::Draw()
{
}

//開放
void TitleScene::Release()
{
}