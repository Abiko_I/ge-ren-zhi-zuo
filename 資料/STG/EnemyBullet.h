#pragma once
#include "Engine/GameObject/GameObject.h"

//弾を管理するクラス
class EnemyBullet : public IGameObject
{
	int _hModel;    //モデル番号

public:
	//コンストラクタ
	EnemyBullet(IGameObject* parent);

	//デストラクタ
	~EnemyBullet();

	//初期化
	void Initialize() override;           //overrideをつけると間違いを検知してくれる

										  //更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;
};

