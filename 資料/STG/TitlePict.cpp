#include "TitlePict.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
TitlePict::TitlePict(IGameObject * parent)
	:IGameObject(parent, "TitlePict"), _hPict(-1)     //_hPict = -1;　的な
{
}

//デストラクタ
TitlePict::~TitlePict()
{
}

//初期化
void TitlePict::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/title2.jpg");     //ロード出来れば番号入る
	assert(_hPict >= 0);                        //
}

//更新
void TitlePict::Update()
{
}

//描画
void TitlePict::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void TitlePict::Release()
{
}