#include "PlayScene.h"
#include "Player.h"
#include "Bullet.h"
#include "Enemy.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/gameObject/Camera.h"
#include "EnemyBullet.h"


//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{
	//プレイヤー
	CreateGameObject<Player>(this);

	//敵
	for ( int i = 0; i < 10; i++ )    //本当はデファインで数字をなくす
	{
		CreateGameObject<Enemy>(this);
	}
}

//更新
void PlayScene::Update()
{
	if (Enemy::count == 0)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_CLEAR);
	}
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}