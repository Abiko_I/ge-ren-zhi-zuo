#include "Input.h"

namespace Input
{
	LPDIRECTINPUT8			_pDInput;		//DirectInputオブジェクト
	LPDIRECTINPUTDEVICE8	_pKeyDevice;	//デバイス（キーボード）オブジェクト
	BYTE _keyState[256];		//現在の各キーの状態
	BYTE _prevKeyState[256];	//前フレームでの各キーの状態

	//初期化
	void Initialize(HWND hWnd)
	{
		DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION,
			IID_IDirectInput8, (VOID**)&_pDInput, nullptr);

		_pDInput->CreateDevice(GUID_SysKeyboard, &_pKeyDevice, nullptr);
		_pKeyDevice->SetDataFormat(&c_dfDIKeyboard);
		_pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	}


	//更新
	void Update()
	{
		_pKeyDevice->Acquire();
		memcpy(_prevKeyState, _keyState, sizeof(_keyState));
		_pKeyDevice->GetDeviceState(sizeof(_keyState), &_keyState);
	}


	//キーが押されているか調べる
	BOOL IsKey(int keyCode)
	{
		//押してる
		if (_keyState[keyCode] & 0x80)
		{
			return TRUE;
		}
		return FALSE;
	}


	//キーを今押したか調べる（押しっぱなしは無効）
	BOOL IsKeyDown(int keyCode)
	{
		//今は押してて、前回は押してない
		if (IsKey(keyCode) && !(_prevKeyState[keyCode] & 0x80))
		{
			return TRUE;
		}
		return FALSE;
	}


	//キーを今放したか調べる
	BOOL IsKeyUp(int keyCode)
	{
		//今押してなくて、前回は押してる
		if (!IsKey(keyCode) && _prevKeyState[keyCode] & 0x80)
		{
			return TRUE;
		}
		return FALSE;
	}


	//開放
	void Release()
	{
		SAFE_RELEASE(_pKeyDevice);
		SAFE_RELEASE(_pDInput);
	}
}