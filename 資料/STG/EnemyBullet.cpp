#include "EnemyBullet.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
EnemyBullet::EnemyBullet(IGameObject * parent)
	:IGameObject(parent, "EnemyBullet"), _hModel(-1)
{
}

//デストラクタ
EnemyBullet::~EnemyBullet()
{
}

//初期化
void EnemyBullet::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Bullet.fbx");
	assert(_hModel >= 0);

	//球体コライダーをつける
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 0.3f);
	AddCollider(collision);
}

//更新
void EnemyBullet::Update()
{
	_position.z -= 1;
	if (_position.z < -100)
	{
		KillMe();
	}
}

//描画
void EnemyBullet::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void EnemyBullet::Release()
{
}

//何かに当たった
void EnemyBullet::OnCollision(IGameObject * pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "Player")     //本来は文字列は==では比較できない
	{
		KillMe();
		pTarget->KillMe();

		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
	}
}