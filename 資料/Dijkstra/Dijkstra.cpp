#include <iostream>
#include <conio.h>
using namespace std;

void search(int sX, int sY, int cost);
void mark(int eX, int eY);

//表示する配列
int map[15][20] = {
{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
{ 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1 },
{ 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1 },
{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
{ 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1 },
{ 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1 },
{ 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1 },
{ 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1 },
{ 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1 },
{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
{ 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1 },
{ 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1 },
{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
};

//表示しない中身のコスト計算用配列
int insideMap[15][20];

int main()
{
	for (int i = 0; i < 15; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			if (map[i][j] == 0)
			{
				cout << "□";
			}

			else if (map[i][j] == 1)
			{
				cout << "■";
			}
		}
		cout << "\n";
	}
	cout << "\n";

	for (int i = 0; i < 15; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			insideMap[i][j] = 99;
		}
	}

	//始点、終点
	int sX, sY, eX, eY, cost;

	//開始位置のX
	sX = 1;
	//開始位置のY
	sY = 1;
	//終了位置のX
	eX = 16;
	//終了位置のY
	eY = 10;
	//初期コスト
	cost = 0;

	//開始位置を決める
	insideMap[sY][sX] = cost;

	//探索してコストを計算する
	search(sX, sY, cost);

	//終点から逆探索して最短経路を決める
	mark(eX, eY);

	for (int i = 0; i < 15; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			if (map[i][j] == 0)
			{
				cout << "□";
			}

			else if (map[i][j] == 1)
			{
				cout << "■";
			}

			else if (map[i][j] == 2)
			{
				cout << "●";
			}
		}
		cout << "\n";
	}
	cout << "\n";

	system("pause");

	return 0;
}

//再起でコストを計算する
void search(int x, int y, int cost)
{
	cost++;

	//探索：上　今いる位置のinsideMapの中の数値が、上の位置のinsideMapの中の数値以下で、
	//且つ、今の位置のmapの中身が壁じゃなかったら
	if (insideMap[y][x] <= insideMap[y - 1][x] && map[y - 1][x] != 1)
	{
		insideMap[y - 1][x] = cost;
		search(x, y - 1, cost);
	}

	//探索：右　今いる位置のinsideMapの中の数値が、右の位置のinsideMapの中の数値以下で、
	//且つ、今の位置のmapの中身が壁じゃなかったら
	if (insideMap[y][x] <= insideMap[y][x + 1] && map[y][x + 1] != 1)
	{
		insideMap[y][x + 1] = cost;
		search(x + 1, y, cost);
	}

	//探索：下　今いる位置のinsideMapの中の数値が、下の位置のinsideMapの中の数値以下で、
	//且つ、今の位置のmapの中身が壁じゃなかったら
	if (insideMap[y][x] <= insideMap[y + 1][x] && map[y + 1][x] != 1)
	{
		insideMap[y + 1][x] = cost;
		search(x, y + 1, cost);
	}

	//探索：左　今いる位置のinsideMapの中の数値が、左の位置のinsideMapの中の数値以下で、
	//且つ、今の位置のmapの中身が壁じゃなかったら
	if (insideMap[y][x] <= insideMap[y][x - 1] && map[y][x - 1] != 1)
	{
		insideMap[y][x - 1] = cost;
		search(x - 1, y, cost);
	}
}

//ラインを引く位置に対応する数字を入れる
void mark(int x, int y)
{
	//探索：上　今いる位置のinsideMapの中の数値が、上の位置のinsideMapの中の数値以下で、
	//且つ、今の位置のmapの中身が壁じゃなかったら
	if (insideMap[y][x] > insideMap[y - 1][x])
	{
		map[y - 1][x] = 2;
		mark(x, y - 1);
	}

	//探索：右　今いる位置のinsideMapの中の数値が、右の位置のinsideMapの中の数値以下で、
	//且つ、今の位置のmapの中身が壁じゃなかったら
	else if (insideMap[y][x] > insideMap[y][x + 1])
	{
		map[y][x + 1] = 2;
		mark(x + 1, y);
	}

	//探索：下　今いる位置のinsideMapの中の数値が、下の位置のinsideMapの中の数値以下で、
	//且つ、今の位置のmapの中身が壁じゃなかったら
	else if (insideMap[y][x] > insideMap[y + 1][x])
	{
		map[y + 1][x] = 2;
		mark(x, y + 1);
	}

	//探索：左　今いる位置のinsideMapの中の数値が、左の位置のinsideMapの中の数値以下で、
	//且つ、今の位置のmapの中身が壁じゃなかったら
	else if (insideMap[y][x] > insideMap[y][x - 1])
	{
		map[y][x - 1] = 2;
		mark(x - 1, y);
	}
}
