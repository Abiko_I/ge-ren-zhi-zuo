#include "Enemy.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Audio.h"
#include "Ground.h"

//コンストラクタ
Enemy::Enemy(IGameObject * parent)
	:IGameObject(parent, "Enemy"), _hModel(-1), _hSound(-1)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Model/Enemy.fbx");
	assert(_hModel >= 0);

	//アニメーションの設定
	Model::SetAnimFrame(_hModel, 0, 300, 2.0f);

	//サイズ
	_scale *= 2;

	//位置
	_position.x = rand() % 100 - 50;
	_position.z = rand() % 100 - 50;


	//地面の高さ
	RayCastData data;
	data.start = _position;
	data.dir = D3DXVECTOR3(0, -1, 0);
	int hStageModel = ((Ground*)FindObject("Ground"))->GetModelHandle();
	Model::RayCast(hStageModel, &data);
	_position.y = -data.dist;


	//向き
	_rotate.y = rand() % 360;


	//当たり判定追加
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0.7f, 0), 1.2f);
	AddCollider(collision);

	//サウンドデータのロード
	_hSound = Audio::Load("data/Audio/Explode.wav");
	assert(_hSound >= 0);
}

//更新
void Enemy::Update()
{
}

//描画
void Enemy::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Enemy::Release()
{
}


//何かに当たった
void Enemy::OnCollision(IGameObject * pTarget)
{
	//弾に当たった
	if (pTarget->GetObjectName() == "Bullet")
	{
		//自分を消す
		KillMe();

		//相手（弾）を消す
		pTarget->KillMe();

		//効果音を鳴らす
		Audio::Play(_hSound);
	}


	//プレイヤー（戦車）に当たった
	if (pTarget->GetObjectName() == "Tank")
	{
		//ゲームオーバーシーンへ
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
	}
}