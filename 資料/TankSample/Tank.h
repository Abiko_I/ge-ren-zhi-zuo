#pragma once
#include "Engine/GameObject/GameObject.h"

//戦車（プレイヤー）を管理するクラス
class Tank : public IGameObject
{
	int _hModel;		//モデル番号
	int _hGroundModel;	//地面のモデル番号
	int _hBulletModel;	//弾のモデル番号

	//移動
	void Move();

	//地面に沿わせる
	void FollowGround();

public:
	Tank(IGameObject* parent);
	~Tank();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};