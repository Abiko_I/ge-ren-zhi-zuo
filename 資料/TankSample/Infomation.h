#pragma once
#include "Engine/GameObject/GameObject.h"

//操作説明を管理するクラス
class Infomation : public IGameObject
{
	int _hPict;    //画像番号

public:
	Infomation(IGameObject* parent);
	~Infomation();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};