#include "Tank.h"
#include "Engine/ResouceManager/Model.h"
#include "Ground.h"
#include "TankHead.h"


//コンストラクタ
Tank::Tank(IGameObject * parent)
	:IGameObject(parent, "Tank"), _hModel(-1), _hGroundModel(-1), _hBulletModel(-1)
{
}

//デストラクタ
Tank::~Tank()
{
}

//初期化
void Tank::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Model/TankBody.fbx");
	assert(_hModel >= 0);

	//子供として砲台を作成
	CreateGameObject<TankHead>(this);

	//弾を撃ったときにロードすると処理落ちするので、予めロードしておく
	_hBulletModel = Model::Load("data/Model/Bullet.fbx");
	assert(_hBulletModel >= 0);

	//箱型の当たり判定を作る
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 0.5f, 0), D3DXVECTOR3(1, 1, 1));
	AddCollider(collision);
}



//更新
void Tank::Update()
{
	//移動
	Move();

	//地面に沿わせる
	FollowGround();

	//敵がもういなかったらクリア画面へ
	if (FindObject("Enemy") == nullptr)
	{
		SceneManager* pSm = (SceneManager*)FindObject("SceneManager");
		pSm->ChangeScene(SCENE_ID_CLEAR);
	}
}

//移動
void Tank::Move()
{
	//戦車が向いている方向に回転させる行列
	D3DXMATRIX mRot;
	D3DXMatrixRotationY(&mRot, D3DXToRadian(_rotate.y));

	//移動ベクトル
	D3DXVECTOR3 move;
	D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, 0.2f), &mRot);

	//前進
	if (Input::IsKey(DIK_W))
	{
		_position = _position + move;
	}

	//後退
	if (Input::IsKey(DIK_S))
	{
		_position = _position - move;
	}

	//右旋回
	if (Input::IsKey(DIK_D))
	{
		_rotate.y += 1.0f;
	}

	//左旋回
	if (Input::IsKey(DIK_A))
	{
		_rotate.y -= 1.0f;
	}
}


//地面に沿わせる
void Tank::FollowGround()
{
	//まだ地面のモデル番号を知らない
	if (_hGroundModel == -1)
	{
		//モデル番号を調べる
		_hGroundModel = ((Ground*)FindObject("Ground"))->GetModelHandle();
	}

	//もう地面のモデル番号を知っている
	else
	{
		//レイを撃つ準備
		RayCastData data;
		data.start = _position;					//戦車の原点から
		data.start.y = 0;						//高さ0（地面は一番高いところでもY<0になっている）
		data.dir = D3DXVECTOR3(0, -1, 0);		//真下方向

		//地面に対してレイを撃つ
		Model::RayCast(_hGroundModel, &data);

		//レイが地面に当たったら
		if (data.hit)
		{
			//戦車の高さを地面にあわせる
			//（Y=0の高さからレイ撃って、data.distメートル先に地面があったということは
			//　そこの標高は『-data.distメートル』ということになる）
			_position.y = -data.dist;
		}
	}
}

//描画
void Tank::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Tank::Release()
{
	//弾のモデルを解放
	Model::Release(_hBulletModel);
}