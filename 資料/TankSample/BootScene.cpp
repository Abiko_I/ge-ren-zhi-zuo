#include "bootScene.h"
#include "Engine/global.h"

BootScene::BootScene(IGameObject * parent)
	: IGameObject(parent, "BootScene")
{
}

void BootScene::Initialize()
{
}

void BootScene::Update()
{
}

void BootScene::Draw()
{
}

void BootScene::Release()
{
}
