#pragma once
#include "Engine/GameObject/GameObject.h"

//戦車の砲台を管理するクラス
class TankHead : public IGameObject
{
	int _hModel;				//モデル番号
	const float SWING_SPEED;	//旋回速度




	//旋回
	void Swing();

	//弾を撃つ
	void Shot();

public:
	TankHead(IGameObject* parent);
	~TankHead();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};