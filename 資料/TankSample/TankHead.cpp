#include "TankHead.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/gameObject/Camera.h"
#include "Bullet.h"

//コンストラクタ
TankHead::TankHead(IGameObject * parent)
	:IGameObject(parent, "TankHead"), _hModel(-1),
	SWING_SPEED(1.5f)
{
}

//デストラクタ
TankHead::~TankHead()
{
}

//初期化
void TankHead::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Model/TankHead.fbx");
	assert(_hModel >= 0);

	//カメラ
	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 2, -3));
	pCamera->SetTarget(D3DXVECTOR3(0, 1, 0));
}

//更新
void TankHead::Update()
{
	//旋回
	Swing();

	//弾を撃つ
	Shot();
}


//旋回
void TankHead::Swing()
{
	//右旋回
	if (Input::IsKey(DIK_RIGHT))
	{
		_rotate.y += SWING_SPEED;
	}

	//左旋回
	if (Input::IsKey(DIK_LEFT))
	{
		_rotate.y -= SWING_SPEED;
	}
}


//弾を撃つ
void TankHead::Shot()
{
	if (Input::IsKeyDown(DIK_SPACE))
	{
		IGameObject* playScene = FindObject("PlayScene");
		Bullet* pBullet = CreateGameObject<Bullet>(FindObject("PlayScene"));


		D3DXVECTOR3 shotPos = Model::GetBonePosition(_hModel, "ShotPoint");
		D3DXVECTOR3 cannonRoot = Model::GetBonePosition(_hModel, "CannonRoot");
		pBullet->Shot(shotPos, shotPos - cannonRoot);
	}
}


//描画
void TankHead::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}


//開放
void TankHead::Release()
{
}