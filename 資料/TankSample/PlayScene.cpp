#include "PlayScene.h"
#include "Ground.h"
#include "Tank.h"
#include "Enemy.h"
#include "Infomation.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"),
	ENEMY_NUM(5)
{
}

//初期化
void PlayScene::Initialize()
{
	CreateGameObject<Ground>(this);
	CreateGameObject<Tank>(this);
	CreateGameObject<Infomation>(this);

	for (int i = 0; i < ENEMY_NUM; i++)
	{
		CreateGameObject<Enemy>(this);
	}
}

//更新
void PlayScene::Update()
{
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}