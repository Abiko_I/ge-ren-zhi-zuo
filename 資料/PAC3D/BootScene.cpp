#include "bootScene.h"
#include "Engine/global.h"
#include "Stage.h"
#include "Engine/gameObject/Camera.h"

BootScene::BootScene(IGameObject * parent)
	: IGameObject(parent, "BootScene")
{
}

void BootScene::Initialize()
{
	CreateGameObject<Stage>(this);
}

void BootScene::Update()
{
}

void BootScene::Draw()
{
}

void BootScene::Release()
{
}
