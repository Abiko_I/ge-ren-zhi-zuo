#include "Stage.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/CsvReader.h"
#include "Player.h"
#include "Engine/GameObject/Camera.h"

//コンストラクタ
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage")
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//モデルデータ(FLOOR)のロード
	_hModel[BL_FLOOR] = Model::Load("data/floor.fbx");
	assert(_hModel[BL_FLOOR] >= 0);

	//モデルデータ(WALL)のロード
	_hModel[BL_WALL] = Model::Load("data/wall.fbx");
	assert(_hModel[BL_WALL] >= 0);

	CsvReader csv;
	csv.Load("data/Map.csv");

	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			_table[x][z] = csv.GetValue(x, z);		//エクセルからの行列を_tableに入れる
		}
	}

	CreateGameObject<Player>(this);

	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(7, 13, -2));		//カメラの位置
	pCamera->SetTarget(D3DXVECTOR3(7, 0, 7));			//カメラの焦点

}


//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	//表示
	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			D3DXMATRIX mat;
			D3DXMatrixTranslation(&mat, x, 0, z);		//ｙは動かさないから０

			Model::SetMatrix(_table[x][z], mat);
			Model::Draw(_table[x][z]);
			
		}
	}
}

//開放
void Stage::Release()
{
}