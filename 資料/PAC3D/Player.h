#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Stage.h"
#include "P3DEngine.h"

#if _DEBUG
#pragma comment(lib, "P3DEngineD.lib")
#else
#pragma comment(lib, "P3DEngine.lib")
#endif

//プレイヤーを管理するクラス
class Player : public IGameObject
{
	int _hModel;    //モデル番号
	Stage* _pStage;
	P3DEngine* pP3D;		//ポインタ

public:
	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	void Move();

	void CollisionDetection();

	//描画
	void Draw() override;

	//開放
	void Release() override;
};