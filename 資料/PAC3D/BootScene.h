#pragma once

#include "Engine/global.h"
#include "Engine/DirectX/Text.h"

class BootScene : public IGameObject 
{

public:
	BootScene(IGameObject* parent);
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};
