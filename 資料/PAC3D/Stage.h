#pragma once
#include "Engine/GameObject/GameObject.h"

//ステージを管理するクラス
class Stage : public IGameObject
{
	//定数化
	enum
	{
		BL_FLOOR,
		BL_WALL,
		BL_MAX
	};

	int _hModel[BL_MAX];		//複数（FLOOR, WALL）あるため配列に
	int _table[15][15];

public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//その位置が通れるかどうか
	//引数：x、ｚ　調べたい位置
	//戻り値：通れるならtrue、壁ならfalse
	bool IsAdvance(int x, int z)
	{
		return _table[x][z] == BL_FLOOR;	//
	}
};