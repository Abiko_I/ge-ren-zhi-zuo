#include "Player.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player"), _hModel(-1)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Player.fbx");
	assert(_hModel >= 0);

	_position.x = 7;
	_position.z = 7;

	_pStage = (Stage*)FindObject("Stage");		//Stageを探す→型がわからないからStage型に

	P3DInitData data;
	data.pDevice = Direct3D::_pDevice;
	pP3D = new P3DEngine(data);
	pP3D->Load("data/smoke.p3b");
	pP3D->Load("data/explosion.p3b");
}

//更新
void Player::Update()
{
	if (Input::IsKeyDown(DIK_E))
	{
		pP3D->Play("data/explosion.p3b", _position, FALSE);
	}
	//入力
	Move();

	//当たり判定
	CollisionDetection();

}

//パックマンの操作
void Player::Move()
{
	D3DXVECTOR3 prevPos = _position;				//今の位置（１）


	float C_MovingSpeed = 15.0f;		//コントローラー入力用

	//コントローラーの左のトリガーを押し込んだら加速
	if (Input::GetPadTrrigerL())
	{
		C_MovingSpeed = 8.0f;		//加速
	}

	//コントローラーのAボタンを押した時ジャンプ
	if (Input::IsPadButtonDown(XINPUT_GAMEPAD_A))
	{
		if (_position.y <= 0.05)		//地面に接している時
		{
			pP3D->Play("data/explosion.p3b", _position, FALSE);
			_position.y += 1;
		}
	}

	//コントローラーでの操作
	_position.x += Input::GetPadStickL().x / C_MovingSpeed;
	_position.z += Input::GetPadStickL().y / C_MovingSpeed;

	//キーボードでの操作
	float K_MovingSpeed = 0.1;		//キーボード入力用

	if (Input::IsKey(DIK_A))		//アクセルボタン
	{
		K_MovingSpeed = 0.15;
	}
	//Aキーと矢印キーのどれかを同時に押している間、砂煙を出す
	if (Input::IsKey(DIK_A) && ( Input::IsKey(DIK_RIGHT) || Input::IsKey(DIK_LEFT) || Input::IsKey(DIK_UP) || Input::IsKey(DIK_DOWN) ))
	{
		pP3D->Play("data/smoke.p3b", _position, FALSE);
	}
	if (Input::IsKey(DIK_RIGHT))
	{
		_position.x += K_MovingSpeed;
	}
	if (Input::IsKey(DIK_LEFT))
	{
		_position.x -= K_MovingSpeed;
	}
	if (Input::IsKey(DIK_UP))
	{
		_position.z += K_MovingSpeed;
	}		
	if (Input::IsKey(DIK_DOWN))
	{
		_position.z -= K_MovingSpeed;
	}
	//ジャンプ
	if (Input::IsKeyDown(DIK_J))
	{

		if (_position.y <= 0.05)		//地面に接している時
		{
			pP3D->Play("data/explosion.p3b", _position, FALSE);
			_position.y += 1;
		}
	}
	if (_position.y > 0.05f)			//重力
	{
		_position.y -= 0.05f;
	}

	D3DXVECTOR3 move = _position - prevPos;			//今の位置から前の位置（１）を引く
	move.y = 0;										//y軸の移動は無視

	if ((prevPos != _position) && Input::GetPadTrrigerL())
	{
		pP3D->Play("data/smoke.p3b", _position, FALSE);
	}

	//パックマンの向き
	float speed = D3DXVec3Length(&move);			//moveの長さをspeedに代入

	if (speed > 0)									//動いていなかったら
	{
		D3DXVec3Normalize(&move, &move);			//moveを正規化
		D3DXVECTOR3 front = D3DXVECTOR3(0, 0, 1);	//パックマンの初期の向き
		float dot = D3DXVec3Dot(&move, &front);
		float angle = acos(dot);

		D3DXVECTOR3 cross;
		D3DXVec3Cross(&cross, &move, &front);		//外積を求める

		if (cross.y > 0)
		{
			angle *= -1;
		}

		_rotate.y = D3DXToDegree(angle);			//ラジアンを度に変換
	}
}

//当たり判定
void Player::CollisionDetection()
{
	int checkX, checkZ, checkY;

	//左側の当たり判定
	checkX = (int)(_position.x - 0.3f);		//プレイヤーの原点から半径を引く
	checkZ = (int)_position.z;

	if (!_pStage->IsAdvance(checkX, checkZ))	//int型にキャストして四捨五入する
	{
		_position.x = checkX + 1.3f;		//当たっていたら、戻す
	}

	//右側の当たり判定
	checkX = (int)(_position.x + 0.3f);		//プレイヤーの原点に半径を足す
	checkZ = (int)_position.z;

	if (!_pStage->IsAdvance(checkX, checkZ))	//int型にキャストして四捨五入する
	{
		_position.x = checkX - 0.3f;		//当たっていたら、戻す
	}

	//手前の当たり判定
	checkX = (int)_position.x;
	checkZ = (int)(_position.z - 0.3);		//プレイヤーの原点から半径を引く

	if (!_pStage->IsAdvance(checkX, checkZ))	//int型にキャストして四捨五入する
	{
		_position.z = checkZ + 1.3f;		//当たっていたら、戻す
	}

	//奥の当たり判定
	checkX = (int)_position.x;
	checkZ = (int)(_position.z + 0.3);		//プレイヤーの原点に半径を足す

	if (!_pStage->IsAdvance(checkX, checkZ))	//int型にキャストして四捨五入する
	{
		_position.z = checkZ - 0.3f;		//当たっていたら、戻す
	}
}

//描画
void Player::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);

	pP3D->Draw();
}

//開放
void Player::Release()
{
}