#include <iostream>
#include <random>
#include <time.h>
#include <windows.h>
#include <vector>

///////////////////////////  棒倒し法  //////////////////////////////////

unsigned long sleepTime = 1 * 1000;	//一時停止する時間

const unsigned int ROW_CNT = 19;	//垂直の大きさ
const unsigned int COL_CNT = 19;	//平行の大きさ

const unsigned int ROAD = 0;		//道（通過可能のマス）
const unsigned int WALL = 1;		//壁（通過不可能のマス）

const char* mazeParts[] = { "　", "□" };	//道と壁の表示

//方向を表現するための構造体
struct tagDirection
{
	unsigned int row;	//垂直
	unsigned int col;	//平行
};

//ステージ
unsigned int stage[ROW_CNT][COL_CNT];

//行き止まりを削除したステージ
unsigned int stageCopy[ROW_CNT][COL_CNT];

//方向
tagDirection dir[4] = 
{
	{0, -1},	//左
	{0, 1},		//右
	{1, 0},		//下
	{-1, 0}		//上
};

//初期化
void init();

//描画
void draw();

//迷路作成
void create(int num, int row, int col);

//行き止まりを削除する
void BreakDeadEnd();

//コピーしたステージの描画
void copyDraw();

int main()
{
	//乱数初期化
	srand((unsigned int)time(NULL));

	init();

	draw();

	Sleep(sleepTime);

	//外周以外のパーツを見る
	for (unsigned int i = 1; i < ROW_CNT - 1; i++)
	{
		for (unsigned int j = 1; j < COL_CNT - 1; j++)
		{
			//壁なら
			if ((i % 2 == 0) && (j % 2 == 0) && stage[i][j] == WALL)
			{
				int num = 0;

				//ステージ全体から見て３行目の時は
				if (i == 2)
				{
					//外周以外の壁としては１行目なので
					num = 1;
				}
				else
				{
					num = 2;
				}

				//迷路作成
				create(num, i, j);
				//draw();
				//Sleep(sleepTime);
			}

			//最後のマスを調べた時は
			if (((i == ROW_CNT - 2) && (j == COL_CNT - 2)))
			{
				draw();
				system("pause");

				BreakDeadEnd();
			}
			//それ以外は
			else
			{
				system("cls");
			}
		}
	}
	return 0;
}

//初期化
void init()
{
	for (unsigned int i = 0; i < ROW_CNT; i++)
	{
		for (unsigned int j = 0; j < COL_CNT; j++)
		{
			//外周と、一つ飛ばしの位置に壁を置く
			if ( ( i == 0) ||						//一番左の列
				( i == ( ROW_CNT - 1) ) ||			//一番右の行
				( j == 0) ||						//一番上の行
				( j == ( COL_CNT - 1) ) ||			//一番下の行
				( ( i % 2 ) == 0 && (j % 2) == 0)	//外周以外の位置の時は１マス置きに
				)
			{
				stage[i][j] = WALL;
			}
			//それ以外の位置には道を置く
			else
			{
				stage[i][j] = ROAD;
			}
		}
	}
}

//描画
void draw()
{
	for (unsigned int i = 0; i < ROW_CNT; i++)
	{
		for (unsigned int j = 0; j < COL_CNT; j++)
		{
			//パーツを表示
			std::cout << mazeParts[stage[i][j]];
		}
		std::cout << "\n";
	}
}


//棒倒し法で迷路を作成
void create(int num, int currentRow, int currentCol)
{
	unsigned int r;
	unsigned int ranVal;
	int row = 0;
	int col = 0;
	bool isOK = false;

	//渡された数値を見て
	switch (num)
	{
		case 1:
			//１行目を倒す（上下左右）
			ranVal = 4;
			break;

		case 2:
			//２行目以降を倒す（下左右）
			ranVal = 3;
			break;
	}

	//かぶりなく１マス埋まるまで
	while (!isOK)
	{
		//乱数生成
		r = rand() % ranVal;

		//乱数が指す方向に１マス進める
		row = currentRow + dir[r].row;
		col = currentCol + dir[r].col;

		//先が道なら
		if (stage[row][col] == ROAD)
		{
			//そのマスを壁にする
			stage[row][col] = WALL;

			//ループ終了
			isOK = true;
		}
	}
}

//行き止まりを消す
void BreakDeadEnd()
{
	//ステージをコピー
	for (unsigned int i = 0; i < ROW_CNT; i++)
	{
		for (unsigned int j = 0; j < COL_CNT; j++)
		{
			stageCopy[i][j] = stage[i][j];
		}
	}

	system("cls");
	//draw();

	for (unsigned int i = 1; i < ROW_CNT - 1; i++)
	{
		for (unsigned int j = 1; j < COL_CNT - 1; j++)
		{
			//今見ている位置が道なら
			if (stageCopy[i][j] == ROAD)
			{
				//壁（外枠以外）がどの方向にあるのかを示す
				std::vector<int> wallList;

				int wallCnt = 0;

				//今の位置の上下左右を調べる
				for (int cnt = 0; cnt < 4; cnt++)
				{
					int y = (i + dir[cnt].row);
					int x = (j + dir[cnt].col);

					//今調べている位置が壁だったら
					if (stageCopy[y][x] == WALL)
					{
						//数える
						wallCnt++;

						//外枠の壁以外は一覧に格納
						if (!(y == 0) &&				//一番左の列
							!(y == (ROW_CNT - 1)) &&	//一番右の行
							!(x == 0) &&				//一番上の行
							!(x == (COL_CNT - 1))		//一番下の行
							)
						{
							wallList.push_back(cnt);
						}
					
					}
				}

				//壁が３つ以上あるときは行き止まりなので
				if (wallCnt >= 3)
				{
					int ranMax = wallList.size();

					int r = rand() % ranMax;

					int y = (i + dir[wallList.at(r)].row);
					int x = (j + dir[wallList.at(r)].col);

					stageCopy[y][x] = ROAD;
				}

				copyDraw();

				//Sleep(sleepTime);

				int a = 0;

				system("cls");

				//最後のマスを調べた時は
				if (((i == ROW_CNT - 2) && (j == COL_CNT - 2)))
				{
					draw();
					copyDraw();
					system("pause");
				}
			}
		}
	}
}

void copyDraw()
{
	for (unsigned int i = 0; i < ROW_CNT; i++)
	{
		for (unsigned int j = 0; j < COL_CNT; j++)
		{
			//パーツを表示
			std::cout << mazeParts[stageCopy[i][j]];
		}
		std::cout << "\n";
	}
}