#include <iostream>
//#include <random>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <windows.h>
#include <algorithm>
#include <conio.h>

///////////////////////////  壁伸ばし法  //////////////////////////////////

unsigned long sleepTime = 1 * 1000;	//一時停止する時間

const unsigned int ROW_CNT = 19;	//垂直の大きさ
const unsigned int COL_CNT = 19;	//平行の大きさ

const unsigned int ROAD = 0;		//道（通過可能のマス）
const unsigned int WALL = 1;		//壁（通過不可能のマス）

const char* mazeParts[] = { "　", "□" };	//道と壁の表示

//伸ばす数
enum
{
	ONE_MINUS = -1,
	ZERO = 0,
	ONE_PLUS = 1
};

//方向を表現するための構造体
struct tagDirection
{
	int row;	//垂直
	int col;	//平行
	int index;	//配列の何番目かを指す
};

//ステージ
unsigned int stage[ROW_CNT][COL_CNT];

//行き止まりを削除したステージ
unsigned int stageCopy[ROW_CNT][COL_CNT];

//方向
tagDirection dir[4] =
{
	{ZERO, ONE_MINUS, 0},	//左
	{ZERO, ONE_PLUS, 1},	//右
	{ONE_PLUS, ZERO, 2},	//下
	{ONE_MINUS, ZERO, 3}	//上
};

//基点の位置を記録する
struct Position
{
	int row;	//垂直
	int col;	//平行
};

//初期化
void init(std::vector<Position>* originList);

//基点を記録
void recordOrigin(int row, int col, std::vector<Position>* originList);

//描画
void draw();

//迷路作成
bool create(Position pos, std::vector<tagDirection>* dirList, std::vector<Position>* originList, std::vector<Position>* usedOriginList, std::vector<Position>* currentWallList);

//メッセージ表示
void showMessage(int dirY, int dirX);

//上下左右をシャッフルする（bcpadではvectorのシャッフルが出来なかったため）
void shuffle(int* array, std::vector<tagDirection>* dirList);

//基点を決める
void selectOrigin(Position* origin, std::vector<Position>* originList, std::vector<Position>* currentWallList);

//壁になった基点候補を別のvectorに格納する
void moveOrigin(Position* origin, std::vector<Position>* originList, std::vector<Position>* usedOriginList);

//壁を伸ばす
void extendWall(int row, int col, int r, int c, bool* isExtend, std::vector<Position>* currentWallList);

//行き止まりを削除する
void BreakDeadEnd();

//コピーしたステージの描画
void copyDraw();

int main()
{
	//最初の乱数
	int ranF;

	//基点リスト
	std::vector<Position> originList;

	//使用済み基点リスト
	std::vector<Position> usedOriginList;

	//再帰中に伸ばしている壁
	std::vector<Position> currentWallList;

	//方向リスト
	std::vector<tagDirection> dirList;

	//リストに登録
	for (int i = 0; i < 4; i++)
	{
		dirList.push_back(dir[i]);
	}

	//初期化
	Position origin = { 0, 0 };

	//乱数初期化
	srand((unsigned int)time(NULL));

	//ステージを初期化
	init(&originList);

	while (!(originList.size() == 0))
	{
		//基点を決める
		selectOrigin(&origin, &originList, &currentWallList);

		draw();

		//一時停止
		Sleep(sleepTime);

		system("cls");

		//上記で決めた基点を移動
		moveOrigin(&origin, &originList, &usedOriginList);

		//迷路作成
		create(origin, &dirList, &originList, &usedOriginList, &currentWallList);

		//再帰で伸ばしたリストを削除
		currentWallList.clear();
	}

	draw();

	BreakDeadEnd();

	system("pause");

	return 0;
}

//初期化
void init(std::vector<Position>* originList)
{
	for (unsigned int i = 0; i < ROW_CNT; i++)
	{
		for (unsigned int j = 0; j < COL_CNT; j++)
		{
			//外周に壁を置く
			if ((i == 0) ||						//一番左の列
				(i == (ROW_CNT - 1)) ||			//一番右の行
				(j == 0) ||						//一番上の行
				(j == (COL_CNT - 1)) 			//一番下の行
				)
			{
				stage[i][j] = WALL;
			}

			//それ以外の位置には道を置く
			else
			{
				stage[i][j] = ROAD;

				//さらに縦横共に偶数なら
				if (((i % 2) == 0) && (j % 2) == 0)
				{
					//基点候補として記録
					recordOrigin(i, j, originList);
				}
			}
		}
	}
}

//基点を記録
void recordOrigin(int row, int col, std::vector<Position>* originList)
{
	//基点の位置を記録
	Position pos;
	pos.row = row;
	pos.col = col;
	originList->push_back(pos);
}

//描画
void draw()
{
	for (unsigned int i = 0; i < ROW_CNT; i++)
	{
		for (unsigned int j = 0; j < COL_CNT; j++)
		{
			//パーツを表示
			std::cout << mazeParts[stage[i][j]];
		}
		std::cout << "\n";
	}
}

//壁伸ばし法で迷路を作成
bool create(Position pos, std::vector<tagDirection>* dirList, std::vector<Position>* originList, std::vector<Position>* usedOriginList, std::vector<Position>* currentWallList)
{
	int row;
	int r;
	int col;
	int c;

	//シャフルに使う配列
	int shuffleArray[4] = { 0, 1, 2, 3 };

	//今見ている方向
	int dirIndex;

	//四方向探索したか
	bool isSearch[4] = { false, false, false, false };

	//ランダムシャッフル
	random_shuffle(dirList->begin(), dirList->end());

	//ランダムシャッフル（bcpad用）
	//shuffle(shuffleArray, dirList);

	//for (std::vector<tagDirection>::iterator it = dirList->begin(); it < dirList->end(); it++)
	for (auto it = dirList->begin(); it < dirList->end(); it++)
	{
		//伸ばしたかどうか
		bool isExtend = false;

		//その方向が、方向の配列の何番目かという情報を取得
		dirIndex = it->index;

		//今見ている方向をまだ探索していない時だけ
		if (!isSearch[dirIndex])
		{
			//先に進む分だけを取り出す
			r = (it->row);
			c = (it->col);

			//乱数が指す方向に１マス進める
			row = pos.row + r;
			col = pos.col + c;

			//次の位置を記録
			Position nextPos;
			nextPos.row = row + r;
			nextPos.col = col + c;

			//どこを探索しているのか
			//showMessage(it->row, it->col);
			//std::cout << "探索\n";

			//先が壁なら
			if (stage[nextPos.row][nextPos.col] == WALL)
			{
				bool isCurrentWall = false;

				//外周の壁なら
				if ((nextPos.row == 0) ||						//一番左の列
					(nextPos.row == (ROW_CNT - 1)) ||		//一番右の行
					(nextPos.col == 0) ||						//一番上の行
					(nextPos.col == (COL_CNT - 1)) 			//一番下の行
					)
				{
					isCurrentWall = false;
				}
				else
				{
					for (std::vector<Position>::iterator it = currentWallList->begin(); it < currentWallList->end(); it++)
					{
						//今と同じ位置を持つのがあるなら
						if ((it->row == nextPos.row) && (it->col == nextPos.col))
						{
							//今伸ばしている壁である
							isCurrentWall = true;
						}
					}
				}

				if (!(isCurrentWall))
				{
					//壁を伸ばす
					extendWall(row, col, r, c, &isExtend, currentWallList);

					//std::cout << "既存の壁に当たりました。\n";

					draw();

					Sleep(sleepTime);
					
					system("cls");

					//伸ばすのは一旦終了
					return true;
				}
			}
			else
			{
				//壁を伸ばす
				extendWall(row, col, r, c, &isExtend, currentWallList);
			}

			for (std::vector<Position>::iterator it = originList->begin(); it < originList->end(); it++)
			{
				//今の位置と同じ位置の基点候補があるなら
				if ((it->row == nextPos.row) && (it->col == nextPos.col))
				{
					//その位置を持つ基点を移動
					moveOrigin(&nextPos, originList, usedOriginList);

					break;
				}
			}

			//伸ばしたなら
			if (isExtend)
			{
				draw();

				Sleep(sleepTime);
				//getch();
				system("cls");

				//再帰
				if (create(nextPos, dirList, originList, usedOriginList, currentWallList))
				{
					return true;
				}
			}

			//探索した
			isSearch[dirIndex] = true;
		}
	}

	return false;
}

//どこを探索しているのか
void showMessage(int dirY, int dirX)
{
	if (dirY == ZERO)
	{
		if (dirX == ONE_MINUS)
		{
			std::cout << "左を";
		}
		else if (dirX == ONE_PLUS)
		{
			std::cout << "右を";
		}
	}
	else if (dirX == ZERO)
	{
		if (dirY == ONE_MINUS)
		{
			std::cout << "上を";
		}
		else if (dirY == ONE_PLUS)
		{
			std::cout << "下を";
		}
	}
}

//ランダムに混ぜる
void shuffle(int* array, std::vector<tagDirection>* dirList)
{
	for (int i = 0; i < 4; i++)
	{
		//乱数を生成
		int ranIndex = rand() % 4;

		//一旦控えさせる
		int space = array[i];

		//上書き
		array[i] = array[ranIndex];

		//こちらも上書き
		array[ranIndex] = space;
	}

	//空にする
	dirList->clear();

	for (int i = 0; i < 4; i++)
	{
		//ランダムの順に追加
		dirList->push_back(dir[array[i]]);
	}

	//ランダムになっているかを表示して確認
	for (std::vector<tagDirection>::iterator it = dirList->begin(); it < dirList->end(); it++)
	{
		std::cout << it->index << ", ";
	}

	std::cout << "\n";
}

//基点を決める
void selectOrigin(Position* origin, std::vector<Position>* originList, std::vector<Position>* currentWallList)
{
	//基点候補から最初の基点を決める

	int oriIndex = rand() % (originList->size());

	int row, col;

	Position currentPos;

	//乱数を生成してイテレータに足す
	std::vector<Position>::iterator it = originList->begin();
	it += oriIndex;

	//基点にする
	origin->row = it->row;
	origin->col = it->col;
	stage[origin->row][origin->col] = WALL;

	//std::cout << "基点：" << origin->row << ", " << origin->col << "\n";

	currentPos.row = it->row;
	currentPos.col = it->col;

	//今伸ばし始めた
	currentWallList->push_back(currentPos);
}

//使用済み基点の移動
void moveOrigin(Position* origin, std::vector<Position>* originList, std::vector<Position>* usedOriginList)
{
	int row, col;

	//今の基点を入れる
	row = origin->row;
	col = origin->col;

	for (std::vector<Position>::iterator it = originList->begin(); it < originList->end(); it++)
	{
		//縦横が同じなら
		if ((it->row == row) && (it->col == col))
		{
			//使用済みリストに移動
			usedOriginList->push_back(*it);

			//削除
			originList->erase(it);

			return;
		}
	}
}

//壁を伸ばす
void extendWall(int row, int col, int r, int c, bool* isExtend, std::vector<Position>* currentWallList)
{
	Position currentPos;

	//そのマスまで壁にする
	stage[row][col] = WALL;
	stage[row + r][col + c] = WALL;

	currentPos.row = row + r;
	currentPos.col = col + c;

	//今の再帰中に伸ばした
	currentWallList->push_back(currentPos);

	//伸ばした
	*isExtend = true;
}

//行き止まりを消す
void BreakDeadEnd()
{
	//ステージをコピー
	for (unsigned int i = 0; i < ROW_CNT; i++)
	{
		for (unsigned int j = 0; j < COL_CNT; j++)
		{
			stageCopy[i][j] = stage[i][j];
		}
	}

	system("cls");
	draw();

	for (unsigned int i = 1; i < ROW_CNT - 1; i++)
	{
		for (unsigned int j = 1; j < COL_CNT - 1; j++)
		{
			//今見ている位置が道なら
			if (stageCopy[i][j] == ROAD)
			{
				//壁（外枠以外）がどの方向にあるのかを示す
				std::vector<int> wallList;

				int wallCnt = 0;

				//今の位置の上下左右を調べる
				for (int cnt = 0; cnt < 4; cnt++)
				{
					int y = (i + dir[cnt].row);
					int x = (j + dir[cnt].col);

					//今調べている位置が壁だったら
					if (stageCopy[y][x] == WALL)
					{
						//数える
						wallCnt++;

						//外枠の壁以外は一覧に格納
						if (!(y == 0) &&				//一番左の列
							!(y == (ROW_CNT - 1)) &&	//一番右の行
							!(x == 0) &&				//一番上の行
							!(x == (COL_CNT - 1))		//一番下の行
							)
						{
							wallList.push_back(cnt);
						}

					}
				}

				//壁が３つ以上あるときは行き止まりなので
				if (wallCnt >= 3)
				{
					int ranMax = wallList.size();

					int r = rand() % ranMax;

					int y = (i + dir[wallList.at(r)].row);
					int x = (j + dir[wallList.at(r)].col);

					stageCopy[y][x] = ROAD;
				}

				copyDraw();

				//Sleep(sleepTime);

				int a = 0;

				//最後のマスを調べた時は
				if (((i == ROW_CNT - 2) && (j == COL_CNT - 2)))
				{
					system("pause");
				}
				//それ以外は
				else
				{
					system("cls");
					draw();
				}
			}
		}
	}
}

void copyDraw()
{
	for (unsigned int i = 0; i < ROW_CNT; i++)
	{
		for (unsigned int j = 0; j < COL_CNT; j++)
		{
			//パーツを表示
			std::cout << mazeParts[stageCopy[i][j]];
		}
		std::cout << "\n";
	}
}