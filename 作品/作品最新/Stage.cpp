#include <time.h>
#include "Stage.h"
#include "Floor.h"
#include "Wall.h"
#include "Crystal.h"
#include "CrystalUI.h"
#include "Engine/Model.h"
#include "Engine/CsvReader.h"
#include "Engine/Camera.h"
#include "Engine/Direct3D.h"

//コンストラクタ
Stage::Stage(IGameObject * parent) : IGameObject(parent, "Stage"),
	crystalCount_(0), pEffect_(nullptr)
{
	playerStartPos_[0] = -1;
	playerStartPos_[1] = -1;

	wallModelList_.clear();
	posList_.clear();
}

//デストラクタ
Stage::~Stage()
{
	//ポインタだから
	SAFE_RELEASE(pEffect_);
}

//初期化
void Stage::Initialize()
{
	//HLSLファイルからエフェクト（シェーダ）を生成
	//エラーが出た結果を入れる
	LPD3DXBUFFER err = 0;

	//エラーが出たとき
	//１：デバイス、２：ファイル、３：よくわからんからNULL、４：よくわからんからNULL、
	//５：？、６：よくわからんからNULL、７：入れたい変数のアドレス、８：よくわからんからNULL
	if (FAILED(
		D3DXCreateEffectFromFile(Direct3D::pDevice, "CommonShader.hlsl",
			NULL, NULL, D3DXSHADER_DEBUG, NULL, &pEffect_, &err)))
	{
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダーエラー", MB_OK);
	}

	assert(pEffect_ != nullptr);

	//乱数を現在時刻で初期化
	srand(time(NULL));

	//csv形式でステージを読み込む
	CsvReader csv;

	//乱数生成
	int ranNum = rand() % 9;

	//乱数によってステージを変える
	switch (ranNum)
	{
		case 0:
		case 3:
		case 6:
			csv.Load("Data/Stages/StageA.csv");
			break;

		case 1:
		case 4:
		case 7:
			csv.Load("Data/Stages/StageB.csv");
			break;

		//乱数が2、5、8のとき
		default:
			csv.Load("Data/Stages/StageC.csv");
			break;
	}

	//csv.Load("Data/Stages/Stage_Sample.csv");

	for (float z = 0.0f; z < HEIGHT; z += 1.0f)
	{
		for (float x = 0.0f; x < WIDTH; x += 1.0f)
		{
			float xPos = x;
			float zPos = (HEIGHT - 1) - z;
			float yPos = 0.0f;

			//csvデータから指定した場所のデータを取り出す
			stage_[(int)zPos][(int)x] = csv.GetValue(x, z);

			//ステージを構成する部品を生成
			CreateParts(stage_[(int)zPos][(int)x], x, z, xPos, yPos, zPos);
		}
	}
}

//更新
void Stage::Update()
{
	//現在のクリスタルの数を渡す
	pCrystalUI_->SetCrystalNum(crystalCount_);
}

//描画
void Stage::Draw()
{
	
}

//開放
void Stage::Release()
{
}

//引数に応じて敵の初期位置を渡す
float Stage::GetEnemyStart(int num, int val)
{
	//渡された番号の位置にある要素を取得
	PosXZ pos = posList_.at(num);

	//第二引数を見て
	switch (val)
	{
		//0の時はxを返す
		case 0:
			return pos.z_;
			break;

		//1の時はzを返す
		case 1:
			return pos.x_;
			break;
	}
}

//ステージを構成する部品を生成する
void Stage::CreateParts(int modelType, int x, int z, float vecX, float vecY, float vecZ)
{
	//各位置のモデルのタイプを見る
	switch (modelType)
	{
		//壁の時
		case TYPE_WALL:
			//壁を生成
			CreateWall((int)x, (int)z, vecX, vecY + 0.5f, vecZ);
			break;

		//プレイヤーの初期値のとき
		case TYPE_PLAYER:
			//位置を記録
			playerStartPos_[0] = vecZ;
			playerStartPos_[1] = x;
			break;

		//上記以外のとき
		default:
			partsPos_[(int)x][(int)z] = D3DXVECTOR3(vecX, vecY + 0.4, vecZ);

			//クリスタルを生成
			Crystal* pCrystal = CreateGameObject<Crystal>(this);
			pCrystal->SetPosition(partsPos_[(int)x][(int)z]);

			crystalCount_++;
			break;
	}

	//敵の初期位置が記されている時
	if (stage_[(int)vecZ][(int)x] == TYPE_ENEMY)
	{
		//位置を記録
		PosXZ pos = { vecZ, x };
		posList_.push_back(pos);
	}

	//床を生成
	Floor* pFloor = CreateGameObject<Floor>(this);
	pFloor->SetPosition(D3DXVECTOR3(vecX, vecY - 0.1f, vecZ));
}

//壁を生成する
void Stage::CreateWall(int x, int z, float vecX, float vecY, float vecZ)
{
	//部品ごとの位置を記録する
	partsPos_[x][z] = D3DXVECTOR3(vecX, vecY, vecZ);

	//壁を生成
	Wall* pWall = CreateGameObject<Wall>(this);
	pWall->SetPosition(partsPos_[x][z]);
}
