#pragma once
#include <fbxsdk.h>
#include "Global.h"

#pragma comment(lib,"libfbxsdk-mt.lib")

//レイキャスト用の構造体
struct RayCastData
{
	D3DXVECTOR3 start;	//レイ発射位置
	D3DXVECTOR3 dir;	//レイの向きのベクトル
	float dist;			//衝突点までの距離
	bool hit;			//レイが当たったかどうか
	D3DXVECTOR3 normal;	//法線

	//初期化
	RayCastData() { dist = 99999.0; }
};

class Fbx
{
	void CheckNode(FbxNode* pNode);
	void CheckMesh(FbxMesh* pMesh);

	int vertexCount_;				//頂点の数を入れる
	int polygonCount_;				//ポリゴン数を入れる
	int indexCount_;				//インデックス数を入れる
	int materialCount_;				//マテリアルの個数
	int* polygonCountOfMaterial_;	//マテリアルごとのポリゴン数を入れる配列

	LPD3DXEFFECT pEffect_;			//効果つけるためのもの

public:
	//頂点データ構造体
	struct Vertex
	{
		D3DXVECTOR3 pos;		//位置情報
		D3DXVECTOR3 normal;		//法線情報
		D3DXVECTOR2 uv;			//uv座標
	};

	FbxManager*  pManager_;		//FBX機能を管理する
	FbxImporter* pImporter_;	//ファイルを開くインポーター
	FbxScene*    pScene_;		//開いたファイルを管理するシーン

	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;			//頂点バッファ
	LPDIRECT3DINDEXBUFFER9* pIndexBuffer_;			//ポインタのポインタ
	LPDIRECT3DTEXTURE9* pTexture_;					//テクスチャを入れる変数
	D3DMATERIAL9* pMaterial_;						//マテリアル

	LPDIRECT3DTEXTURE9*	pNormalMap_;	//ノーマルマッピング用のテクスチャ

	Fbx(LPD3DXEFFECT pEffect);
	~Fbx();

	int vertex_ = 24;		//頂点数
	int polygon_ = 12;		//ポリゴン数


	virtual void Load(const char *fileName);
	void Draw(const D3DXMATRIX &matrix);

	//レイキャスト（レイを飛ばして当たり判定をする）
	//引数：data（必要なものをまとめたデータ）
	//戻り値：なし
	void RayCast(RayCastData* data);

	void SetPEffect(LPD3DXEFFECT pEffect) { pEffect_ = pEffect; }
};