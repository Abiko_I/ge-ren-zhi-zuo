#include "Image.h"


namespace Image
{
	//ロード済みの画像データ一覧
	std::vector<ImageData*>	dataList;

	//画像をロード
	int Load(std::string fileName)
	{
		ImageData* pData = new ImageData;
		pData->fileName = fileName;

		//開いたファイル一覧から同じファイル名のものが無いか探す
		bool isExist = false;

		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			if (dataList[i]->fileName == fileName)
			{
				//i番目のアドレスを渡す
				pData->pSprite = dataList[i]->pSprite;

				//データを見つけたことを知らせる
				isExist = true;
				break;
			}
		}

		//同じデータがなかったら
		if (isExist == false)
		{
			//新たにロード
			pData->pSprite = new Sprite;
			pData->pSprite->Load(fileName.c_str());
		}

		//dataListの後ろのほうに入れる
		dataList.push_back(pData);

		//dataListの要素数を返す
		return dataList.size() - 1;
	}
	void Draw(int handle)
	{
		//dataListのハンドル番目のマトリックスを渡す
		dataList[handle]->pSprite->Draw(dataList[handle]->matrix);
	}
	void SetMatrix(int handle, D3DXMATRIX & matrix)
	{
		//dataListのハンドル番目のマトリックスに、受け取ったマトリックスを入れる
		dataList[handle]->matrix = matrix;
	}


	void Release(int handle)
	{
		bool isExist = false;

		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//自分のファイル名以外で、同じものがあるかどうか調べる
			if (i != handle && dataList[i] != nullptr && dataList[i]->pSprite == dataList[handle]->pSprite)
			{
				//データを見つけたことを知らせる
				isExist = true;
				break;
			}
		}

		//同じモデルを他でも使っていなければモデル解放
		if (isExist == false)
		{
			//dataListのハンドル番目のpFbxをデリート
			SAFE_DELETE(dataList[handle]->pSprite);
		}
		//dataListそのものをデリート
		SAFE_DELETE(dataList[handle]);
	}

	void AllRelease()
	{
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//まだ削除していなければ
			if (dataList[i] != nullptr)
			{
				//リリースを呼ぶ　i （ハンドル番号）を渡す
				Release(i);
			}
		}

		//全て削除
		dataList.clear();
	}
};