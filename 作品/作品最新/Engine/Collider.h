#pragma once
#include "Direct3D.h"

class IGameObject;
class SphereCollider;

//当たり判定のタイプ
enum ColliderType
{
	COLLIDER_BOX,	//箱型コライダー
	COLLIDER_CIRCLE	//球体コライダー
};

//当たり判定を管理する
class Collider
{
private:
	friend class SphereCollider;
	friend class BoxCollider;

protected:
	//プレイヤーから見たコライダーの中心
	D3DXVECTOR3 center_;

	//誰の当たり判定なのかを明示する
	IGameObject* owner_;

	//テスト表示用の枠
	LPD3DXMESH pMesh_;

	//当たり判定のタイプ
	ColliderType type_;

public:
	Collider();
	virtual ~Collider();

	//継承先でオーバーライドする
	//引数：相手の当たり判定
	//戻り値：接触していれば真、接触していなければ偽
	virtual bool IsHit(Collider* target) = 0;

	//球体と球体の当たり判定
	//引数１：当たり判定したい球体１、２：当たり判定したい球体２
	//戻り値：二つの球体が当たっている場合真、当たっていない場合偽
	bool IsHitCircleVsCircle(SphereCollider* sphereA, SphereCollider* sphereB);

	//箱型と球体の当たり判定
	//引数１：当たり判定をしたい箱、２：当たり判定をしたい球体
	//戻り値：箱と球体が当たっている場合真、当たっていない場合偽
	bool IsHitCircleVsBox(BoxCollider* box, SphereCollider* sphere);

	//コリジョンを描画する
	//引数：位置
	//戻り値：なし
	void Draw(D3DXVECTOR3 position);

	//オブジェクトにコライダーをセットする
	//引数：コライダーを付ける対象となるオブジェクト
	//戻り値：なし
	void SetGameObject(IGameObject* object) { owner_ = object; }

	//現在のコライダーの中心を渡す
	//引数：なし
	//戻り値：中心位置
	D3DXVECTOR3 GetCenter();

	//自分を適用しているオブジェクトを教える
	//引数：なし
	//戻り値：自分を適用指定しているオブジェクト
	IGameObject* GetOwner() { return owner_; }
};

