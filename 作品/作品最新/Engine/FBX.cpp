#include "Fbx.h"
#include "Direct3D.h"



Fbx::Fbx(LPD3DXEFFECT pEffect) : pVertexBuffer_(nullptr), pIndexBuffer_(nullptr),
pTexture_(nullptr), pMaterial_(nullptr), pNormalMap_(nullptr), pManager_(nullptr),
pImporter_(nullptr), pScene_(nullptr), vertexCount_(0), polygonCount_(0),
indexCount_(0), polygonCountOfMaterial_(nullptr), pEffect_(pEffect)
{
}


Fbx::~Fbx()
{
	SAFE_DELETE_ARRAY(polygonCountOfMaterial_);
	pScene_->Destroy();
	pManager_->Destroy();
	for (int i = 0; i < materialCount_; i++)
	{
		//それぞれ一つずつi番目を解放
		SAFE_RELEASE(pNormalMap_[i]);
		SAFE_RELEASE(pTexture_[i]);
		SAFE_RELEASE(pIndexBuffer_[i]);
	}
	SAFE_DELETE_ARRAY(pNormalMap_);
	SAFE_DELETE_ARRAY(pMaterial_);
	SAFE_DELETE_ARRAY(pTexture_);			//大本を解放
	SAFE_DELETE_ARRAY(pIndexBuffer_);		//大本を解放
	SAFE_RELEASE(pVertexBuffer_);
}

void Fbx::Load(const char *fileName)
{
	pManager_ = FbxManager::Create();

	//エラー処理
	assert(pManager_ != nullptr);

	pImporter_ = FbxImporter::Create(pManager_, "");

	//エラー処理
	assert(pImporter_ != nullptr);

	pScene_ = FbxScene::Create(pManager_, "");

	//エラー処理
	assert(pScene_ != nullptr);

	pImporter_->Initialize(fileName);					//持ってくる
	pImporter_->Import(pScene_);					//かごに入れる
	pImporter_->Destroy();							//お役御免なので解放

	char defaultCurrentDir[MAX_PATH];

	//現在のカレントディレクトリを調べる
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	char dir[MAX_PATH];
	_splitpath_s(fileName, nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);

	//これ以降は　dir　の下を探してね　ということ　（Linuxのcdコマンドと同じ）
	SetCurrentDirectory(dir);

	FbxNode* rootNode = pScene_->GetRootNode();		//FBXファイルを読み込んだシーンからルートノードを取り出す

	int childCount = rootNode->GetChildCount();		//ルートノードの子供の数を把握する

	for (int i = 0; childCount > i; i++)			//子供の数だけループ
	{
		//ノードの内容を長男から順番に一つずつ関数に渡してチェック
		CheckNode(rootNode->GetChild(i));			//rootNodeのi番目の子供を渡す
	}

	//上でカレントディレクトリを移動したから、元の場所に戻す
	SetCurrentDirectory(defaultCurrentDir);
}

//メッシュノードだった時
void Fbx::CheckMesh(FbxMesh* pMesh)
{
	FbxVector4* pVertexPos = pMesh->GetControlPoints();
	vertexCount_ = pMesh->GetControlPointsCount();			//頂点の数と同じサイズのVertex構造体型の配列を作成
	Vertex* vertexList = new Vertex[vertexCount_];

	polygonCount_ = pMesh->GetPolygonCount();
	indexCount_ = pMesh->GetPolygonVertexCount();

	for (int i = 0; vertexCount_ > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];
	}

	//ポリゴン数だけループ
	for (int i = 0; i < polygonCount_; i++)
	{
		//最初の頂点の番号を調べる
		int startIndex = pMesh->GetPolygonVertexIndex(i);

		//３頂点分ループ
		for (int j = 0; j < 3; j++)
		{
			int index = pMesh->GetPolygonVertices()[startIndex + j];

			//法線取得
			FbxVector4 Normal;

			//第１引数：ポリゴンの番号　第２引数：頂点の番号　第３引数：法線を入れる
			pMesh->GetPolygonVertexNormal(i, j, Normal);

			//頂点情報に入れる
			vertexList[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);

			//頂点情報をゲット
			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));
		}
	}

	//バーテックスバッファ作成
	Direct3D::pDevice->CreateVertexBuffer(sizeof(Vertex) *vertexCount_, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED,
		&pVertexBuffer_, 0);

	//エラー処理
	assert(pVertexBuffer_ != nullptr);

	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);					//バーテックスバッファをロック（捕まえる）
	memcpy(vCopy, vertexList, sizeof(Vertex) *vertexCount_);		//メモリをコピー
	pVertexBuffer_->Unlock();										//バーテックスバッファを解放

	SAFE_DELETE_ARRAY (vertexList);									//完璧なデリート

	//インデックスの処理
	pIndexBuffer_ = new IDirect3DIndexBuffer9*[materialCount_];

	//エラー処理
	assert(pIndexBuffer_ != nullptr);

	//インデックス作成
	for (int i = 0; i < materialCount_; i++)
	{
		int* indexList = new int[indexCount_];
		polygonCountOfMaterial_ = new int[materialCount_];

		//エラー処理
		assert(polygonCountOfMaterial_ != NULL);

		int count = 0;

		for (int polygon = 0; polygon < polygonCount_; polygon++)
		{
			//今見ているポリゴンが何番目のマテリアルのものか確認
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);

			//materialID　が ｉ 番目であればそのポリゴンの3頂点をインデックス情報（pIndexList）に追加する
			if (materialID == i)
			{
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}
		polygonCountOfMaterial_[i] = count / 3;

		//インデックスバッファ作成
		Direct3D::pDevice->CreateIndexBuffer(sizeof(int) * indexCount_, 0,
			D3DFMT_INDEX32, D3DPOOL_MANAGED, &pIndexBuffer_[i], 0);

		//エラー処理
		assert(pIndexBuffer_ != nullptr);

		DWORD *iCopy;
		pIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);			//インデックスバッファをロック）
		memcpy(iCopy, indexList, sizeof(int) * indexCount_);	//メモリをコピー
		pIndexBuffer_[i]->Unlock();								//インデックスバッファを解放

		SAFE_DELETE_ARRAY(indexList);							//完璧にデリート
	}
}

//ノードの内容をチェック
void Fbx::CheckNode(FbxNode* pNode)
{
	//メッシュノードだった時
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//マテリアルの数を調べる
		materialCount_ = pNode->GetMaterialCount();
		pMaterial_ = new D3DMATERIAL9[materialCount_];

		//エラー処理
		assert(pMaterial_ != nullptr);

		pTexture_ = new LPDIRECT3DTEXTURE9[materialCount_];

		//エラー処理
		assert(pTexture_ != nullptr);

		//初期化
		ZeroMemory(pTexture_, sizeof(LPDIRECT3DTEXTURE9) * materialCount_);
		
		//マテリアルの数分のテクスチャを入れる配列作成
		pNormalMap_ = new LPDIRECT3DTEXTURE9[materialCount_];

		for (int i = 0; i < materialCount_; i++)
		{
			//i番目のマテリアルの情報をFbxSurfaceLambert型のポインタに入れる
			//FbxSurfaceLambert* lambert = (FbxSurfaceLambert*)pNode->GetMaterial(i);
			FbxSurfacePhong* surface = (FbxSurfacePhong*)pNode->GetMaterial(i);

			FbxDouble3 diffuse = surface->Diffuse;		//ポリゴン色を取得
			FbxDouble3 ambient = surface->Ambient;		//環境光を取得

			ZeroMemory(&pMaterial_[i], sizeof(D3DMATERIAL9));

			//ポリゴン色
			pMaterial_[i].Diffuse.r = (float)diffuse[0];
			pMaterial_[i].Diffuse.g = (float)diffuse[1];
			pMaterial_[i].Diffuse.b = (float)diffuse[2];
			pMaterial_[i].Diffuse.a = 1.0f;					//透明度　１→透明/０→不透明

			//環境光
			pMaterial_[i].Ambient.r = (float)ambient[0];
			pMaterial_[i].Ambient.g = (float)ambient[1];
			pMaterial_[i].Ambient.b = (float)ambient[2];
			pMaterial_[i].Ambient.a = 1.0f;					//透明度　１→透明/０→不透明

			if (surface->GetClassId().Is(FbxSurfacePhong::ClassId))
			{
				FbxDouble3 specular = surface->Specular;
				pMaterial_[i].Specular.r = (float)specular[0];
				pMaterial_[i].Specular.g = (float)specular[1];
				pMaterial_[i].Specular.b = (float)specular[2];
				pMaterial_[i].Specular.a = 1.0f;

				pMaterial_[i].Power = (float)surface->Shininess;
			}

			//普通のテクスチャロードのブロック
			{
				//設定したテクスチャの情報を取得（通常のdiffuseマッピング）
				FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);

				//上で使われたファイルの情報を取得
				FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

				//テクスチャを使っていない場合
				if (textureFile == NULL)
				{
					pTexture_[i] = NULL;
				}
				//テクスチャを使っている場合は読み込む
				else
				{
					const char* textureFileName = textureFile->GetFileName();

					char name[_MAX_FNAME];			//ファイル名を入れる
					char ext[_MAX_EXT];				//拡張子を入れる
					_splitpath_s(textureFileName, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);

					//nameとextをnameに入れる
					wsprintf(name, "%s%s", name, ext);

					//テクスチャ作成
					D3DXCreateTextureFromFileEx(Direct3D::pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN,
						D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, 0, 0, 0, &pTexture_[i]);

					assert(pTexture_[i] != nullptr);
				}
			}

			//ノーマルマッピング用テクスチャロードのブロック
			{
				//設定したテクスチャの情報を取得（凹凸を表現するbumpマッピング）
				FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sBump);
				FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

				//ノーマルマップを使ってない場合
				if (textureFile == NULL)
				{
					pNormalMap_[i] = NULL;
				}

				//ノーマルマップを使ってる場合
				else
				{
					const char* textureFileName = textureFile->GetFileName();

					char name[_MAX_FNAME];			//ファイル名を入れる
					char ext[_MAX_EXT];				//拡張子を入れる
					_splitpath_s(textureFileName, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);

					//nameとextをnameに入れる
					wsprintf(name, "%s%s", name, ext);

					//テクスチャ作成
					D3DXCreateTextureFromFileEx(Direct3D::pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN,
						D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, 0, 0, 0, &pNormalMap_[i]);

					assert(pNormalMap_[i] != nullptr);
				}
			}
		}

		CheckMesh(pNode->GetMesh());
	}

	else
	{
		//メッシュ以外のデータだった時も、その子供がメッシュノードである可能性があるので、
		//自分の子供も調べていく
		int childCount = pNode->GetChildCount();
		for (int i = 0; childCount > i; i++)
		{
			//再帰呼び出し
			CheckNode(pNode->GetChild(i));
		}
	}
}

void Fbx::Draw(const D3DXMATRIX &matrix)
{
	//ワールド行列として &matrix を使って　という意味
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);

	//Direct3D::pDevice->SetTexture(0, pTexture_);								//使いたいテクスチャを指定
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));	//SetStreamSource第一引数は０枚目という意味
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);	//フラグ管理
					  //      ↑位置			↑法線			↑uv情報

	for (int i = 0; i < materialCount_; i++)
	{
		Direct3D::pDevice->SetTexture(0, pTexture_[i]);			//テクスチャ選択
		Direct3D::pDevice->SetIndices(pIndexBuffer_[i]);
		Direct3D::pDevice->SetMaterial(&pMaterial_[i]);			//使用するマテリアルを指定
		Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertexCount_, 0, polygonCountOfMaterial_[i]);

		//pEffect_があるとき（効果をつけたいとき）
		if (pEffect_)
		{
			//物体の色
			pEffect_->SetVector("DIFFUSE_COLOR",
				&D3DXVECTOR4(
					pMaterial_[i].Diffuse.r,
					pMaterial_[i].Diffuse.b,
					pMaterial_[i].Diffuse.g,
					pMaterial_[i].Diffuse.a
				));

			//環境光
			pEffect_->SetVector("AMBIENT_COLOR",
				&D3DXVECTOR4(
					pMaterial_[i].Ambient.r,
					pMaterial_[i].Ambient.b,
					pMaterial_[i].Ambient.g,
					pMaterial_[i].Ambient.a
				));

			//鏡面反射光
			pEffect_->SetVector("SPECULAR_COLOR",
				&D3DXVECTOR4(
					pMaterial_[i].Specular.r,
					pMaterial_[i].Specular.b,
					pMaterial_[i].Specular.g,
					pMaterial_[i].Specular.a
				));

			//テクスチャを貼っているかどうか
			pEffect_->SetBool("IS_PASTED", pTexture_[i] != nullptr);

			pEffect_->SetTexture("TEXTURE", pTexture_[i]);

			//ハイライトの大きさを渡す
			pEffect_->SetFloat("SPECULAR_POWER", pMaterial_[i].Power);

			//ノーマルマップを使用しているかどうかが渡される
			pEffect_->SetTexture("NORMAL_MAP", pNormalMap_[i]);
		}
		else
		{
			int a = 0;
		}
	}
}

//レイキャスト
void Fbx::RayCast(RayCastData * data)
{
	//とりあえず当たっていないことにする
	data->hit = false;

	//頂点バッファをロックする
	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);

	//マテリアル毎
	for (DWORD i = 0; i < materialCount_; i++)
	{
		//インデックスバッファをロックする
		DWORD *iCopy;
		pIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);

		//マテリアルのポリゴン毎
		for (DWORD j = 0; j < polygonCountOfMaterial_[i]; j++)
		{
			//３頂点分
			D3DXVECTOR3 ver[3];
			ver[0] = vCopy[iCopy[j * 3 + 0]].pos;
			ver[1] = vCopy[iCopy[j * 3 + 1]].pos;
			ver[2] = vCopy[iCopy[j * 3 + 2]].pos;

			bool hit;
			float dist;

			//レイが当たっているかどうかを調べる
			hit = D3DXIntersectTri(&ver[0], &ver[1], &ver[2],
				&data->start, &data->dir, NULL, NULL, &dist);

			//当たっていて且つ、距離がデータの距離より短かったら
			if ( hit &&  dist < data->dist )
			{
				//当たっている
				data->hit = true;

				//距離を更新
				data->dist = dist;
			}
		}

		//インデックスバッファ使用終了
		pIndexBuffer_[i]->Unlock();
	}

	//頂点バッファ使用終了
	pVertexBuffer_->Unlock();
}