#pragma once
#include "Global.h"

class Sprite
{	
	LPD3DXSPRITE pSprite_;				//スプライト　→　ポリゴンの2D版のようなもの
	LPDIRECT3DTEXTURE9 pTexture_;		//テクスチャ　→　ポリゴンやスプライトに貼るシールのようなもの

public:
	Sprite();
	~Sprite();

	void Load(const char *file);		//スプライトやテクスチャの作成、引数：file、戻り値：なし
	void Draw(D3DXMATRIX& matrix);		//ゲーム画面の描画、引数：D3DXMATRIX型のmatrix、戻り値：なし
};

