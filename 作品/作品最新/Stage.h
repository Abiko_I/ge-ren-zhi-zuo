#pragma once
#include <vector>
#include "Engine/IGameObject.h"

class CrystalUI;

//Stageを管理するクラス
class Stage : public IGameObject
{
private:
	//ステージとして表示する種類
	enum
	{
		TYPE_WALL = 0,	//壁
		TYPE_FLOOR,		//床
		TYPE_PLAYER,	//プレイヤーの初期位置
		TYPE_ENEMY,		//敵の初期位置
		TYPE_MAX
	};

	//xとzの位置をまとめる
	struct PosXZ
	{
		float x_;	//x位置
		float z_;	//z位置
	};

	//ステージ
	int stage_[HEIGHT][WIDTH];

	//プレイヤーの初期位置（z, x）
	float playerStartPos_[2];

	//ステージ上のクリスタルの数
	int crystalCount_;

	//ステージの部品ごとの位置
	D3DXVECTOR3 partsPos_[HEIGHT][WIDTH];

	//壁のモデル番号を格納する
	std::vector<int> wallModelList_;

	//敵の初期位置
	std::vector<PosXZ> posList_;

	//クリスタルのUIを管理するクラスのポインタ
	CrystalUI* pCrystalUI_;

	//HLSLから作成されたシェーダを入れる
	LPD3DXEFFECT pEffect_;

public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ステージを構成する部品を生成する
	//引数：モデルのタイプ、記録するx位置、記録するz位置、
	//		ベクトルのx成分、ベクトルのy成分、ベクトルのz成分
	//戻り値：なし
	void CreateParts(int modelType, int x, int z, float vecX, float vecY, float vecZ);

	//壁を生成する
	//引数：記録するx位置、記録するz位置、
	//		ベクトルのx成分、ベクトルのy成分、ベクトルのz成分
	//戻り値：なし
	void CreateWall(int x, int z, float vecX, float vecY, float vecZ);

	//クリスタルの数を減らす
	//引数：なし
	//戻り値：なし
	void DecreaseCrystal() { crystalCount_--; }

	//モデル番号を格納する
	//引数：モデル番号
	//戻り値：なし
	void StoreModelHandle(int handle) { wallModelList_.push_back(handle); }

	//クリスタルのUIクラスのポインタをもらう
	//引数：クリスタルUIクラスのポインタ
	//戻り値：なし
	void SetCrystalUI(CrystalUI* pCrystalUI) { pCrystalUI_ = pCrystalUI; }

	//モデルリストを渡す
	//引数：なし
	//戻り値：モデルリスト
	std::vector<int>* GetModelList() { return &wallModelList_; }

	//引数で渡された場所のステージ情報を渡す
	//引数：ステージのx位置とz位置
	//戻り値：指定された場所のステージ情報
	int GetStage(int x, int z) { return stage_[x][z]; }

	//クリスタルの数を教える
	//引数：なし
	//戻り値：クリスタルの数
	int GetCrystalCnt() { return crystalCount_; }

	//プレイヤーの初期位置を渡す
	//引数：なし
	//戻り値：プレイヤーの初期位置
	float* GetStartPos() { return playerStartPos_; }

	//引数に応じて敵の初期位置を渡す
	//引数：敵が生成された時の番号、0か1
	//戻り値：引数が0の時はxの位置、引数が1の時はzの位置
	float GetEnemyStart(int num, int val);

	//敵の数を教える
	//引数：なし
	//戻り値：敵の数
	int GetEnemyCount() { return posList_.size(); }

	//モデルに付ける効果を渡す
	//引数：なし
	//戻り値：呼び出し元が適用する効果
	LPD3DXEFFECT* GetPEffect() { return &pEffect_; }
};
