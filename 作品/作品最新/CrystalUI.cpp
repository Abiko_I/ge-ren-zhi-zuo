#include "CrystalUI.h"
#include "Engine/Image.h"

//コンストラクタ
CrystalUI::CrystalUI(IGameObject * parent)
	:IGameObject(parent, "CrystalUI"), STANDARD_POSX_CURRENT_(100),
	ADD_POSX_CURRENT_(30), STANDARD_POSY_CURRENT_(0), ADD_POSY_CURRENT_(10),
	STANDARD_POSX_MAX_(220), ADD_POSX_MAX_(22), STANDARD_POSY_MAX_(0), ADD_POSY_MAX_(25),
	crystalMax_(0), crystalNum_(0)
{
}

//デストラクタ
CrystalUI::~CrystalUI()
{
}

//初期化
void CrystalUI::Initialize()
{
	//共通の画像名を格納
	std::string pict[PICT_NUM_MAX]
	{
		"Num0",
		"Num1",
		"Num2",
		"Num3",
		"Num4",
		"Num5",
		"Num6",
		"Num7",
		"Num8",
		"Num9",
	};

	//現在の残りクリスタルの数を示す画像データのロード
	for (int i = 0; i < PICT_NUM_MAX; i++)
	{
		hCrystalCurrent_[i] = Image::Load("data/Picts/Numbers/" + pict[i] + "Current.png");
		assert(hCrystalCurrent_[i] >= 0);
	}

	//クリスタルの最大数画像データのロード
	for (int i = 0; i < PICT_NUM_MAX; i++)
	{
		hCrystalMax_[i] = Image::Load("data/Picts/Numbers/" + pict[i] + "Max.png");
		assert(hCrystalMax_[i] >= 0);
	}

	//枠の画像をロード
	hFrame_ = Image::Load("data/Picts/Numbers/CrystalNumFrame.png");
	assert(hFrame_ >= 0);
}

//更新
void CrystalUI::Update()
{
}

//描画
void CrystalUI::Draw()
{
	////////////////////　枠の表示　////////////////
	//位置調整する画像の行列
	D3DXMATRIX frameMat;

	//描画位置を調整
	D3DXMatrixTranslation(&frameMat, 0, 0, 0);

	//画像を描画
	Image::SetMatrix(hFrame_, frameMat);
	Image::Draw(hFrame_);

	////////////////////////////////////////////////

	//現在の残りクリスタルの数を描画する
	NumberDraw(crystalNum_, STANDARD_POSX_CURRENT_, ADD_POSX_CURRENT_,
		STANDARD_POSY_CURRENT_, ADD_POSY_CURRENT_, hCrystalCurrent_);

	//クリスタルの最大数を描画する
	NumberDraw(crystalMax_, STANDARD_POSX_MAX_, ADD_POSX_MAX_,
		STANDARD_POSY_MAX_, ADD_POSY_MAX_, hCrystalMax_);
}

//開放
void CrystalUI::Release()
{
}

//数字を描画する
void CrystalUI::NumberDraw(int num, float stdX, float addX, float stdY, float addY, int* hPict)
{
	//1、2、3桁の数値を格納する
	int digit[] = { 0, 0, 0 };

	//描画するかどうか
	bool isDraw[] = { true, true, true };

	//位置調整する画像の行列
	D3DXMATRIX mat;

	if (num >= 100)
	{
		//10の位を求める
		int surplus = (num % 100);

		//100の位のみにする
		num -= surplus;

		//3桁目を計算
		digit[0] = num / 100;

		num = surplus;
	}
	if (num >= 10)
	{
		//1の位を求める
		int surplus = (num % 10);

		//10の位のみにする
		num -= surplus;

		//2桁目を計算
		digit[1] = num / 10;

		num = surplus;
	}

	//1桁目を代入
	digit[2] = num;

	//3桁目が0なら
	if (digit[0] == 0)
	{
		//描画しない
		isDraw[0] = false;
	}
	//2桁目が描画しないことになっていて 且つ、
	//3桁目も描画しないことになっているなら
	if (digit[1] == 0 && !(isDraw[0]))
	{
		//2桁目も描画しない
		isDraw[1] = false;
	}

	for (int i = 0; i < 3; i++)
	{
		//描画位置を調整
		D3DXMatrixTranslation(&mat, stdX + (addX * i), stdY + addY, 0);

		//描画するなら
		if (isDraw[i] == true)
		{
			//画像を描画
			Image::SetMatrix(hPict[digit[i]], mat);
			Image::Draw(hPict[digit[i]]);
		}
	}
}
