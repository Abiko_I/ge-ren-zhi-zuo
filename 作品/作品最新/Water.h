#pragma once
#include "Engine/IGameObject.h"

//プレーンを管理するクラス
class Water : public IGameObject
{
	int hModel_;					//モデル番号

	//HLSLから作成されたシェーダを入れる
	LPD3DXEFFECT pEffect_;

public:
	//コンストラクタ
	Water(IGameObject* parent);

	//デストラクタ
	~Water();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};