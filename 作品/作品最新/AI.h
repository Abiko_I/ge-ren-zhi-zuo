#pragma once
#include <vector>
#include "Engine/IGameObject.h"

//AI（基底となるAI）を管理するクラス
class AI : public IGameObject
{
protected:
	//キャラクターの向きを指す
	enum DIRECTION
	{
		FORWARD,	//前を向いている
		RIGHT,		//右を向いている
		LEFT,		//左を向いている
		BACKWARD,	//後ろを向いている
		NONE		//方向無し
	};

	//敵の状態
	enum STATE
	{
		CHASE,		//プレイヤーを発見したから追跡するとき
		PATROL,		//プレイヤーをまだ発見していないから巡回するとき
		SEARCH,		//一時的にプレイヤーを見失ったが、探すとき
	};

	//キャラクターの方向配列の添え字
	enum
	{
		F_B,	//上下
		R_L	//左右
	};

	//ステージの種類
	enum
	{
		TYPE_WALL = 0,	//壁
		TYPE_FLOOR,		//床
		TYPE_PLAYER,	//プレイヤーの初期位置
		TYPE_ENEMY,		//敵の初期位置
		TYPE_MAX
	};

	//ステージの部品の情報
	struct PosInfo
	{
		D3DXVECTOR3 pos;	//その部品の位置
		DIRECTION dir;		//自分から見てどの方向にあるのか
	};

	//キャラクターの位置（引数で渡す時に使う）
	struct CharaPos
	{
		int z_;
		int x_;
	};

	//分岐がある位置の
	struct BranchPosInfo
	{
		int z;			//分岐がある位置のx位置
		int x;			//分岐がある位置のz位置
		int dir;		//分岐している方向
		//float rotate;	//上記の位置を向くために必要な回転度数
	};

	//敵の位置
	D3DXVECTOR3 parentPos_;

	//探索するかどうか
	bool isSearch_;

	//分岐がある位置のリスト
	std::vector<BranchPosInfo> branchPosList_;

public:
	//コンストラクタ
	AI(IGameObject * parent, std::string name);

	//デストラクタ
	~AI();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//敵の位置をもらう
	//引数：敵の位置
	//戻り値：なし
	void SetEnemyPos(D3DXVECTOR3 pos) { parentPos_ = pos; }
};

