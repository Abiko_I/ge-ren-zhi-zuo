#include "CameraTarget.h"
#include "Engine/Model.h"
#include "Engine/Camera.h"
#include "Engine/SphereCollider.h"
#include "Stage.h"
#include "Player.h"

CameraTarget::CameraTarget(IGameObject * parent)
	:IGameObject(parent, "CameraTarget"), hModel_(-1), hitHModel_(-1),
	MOVE_SPEED_(0.2f), ROTATE_SPEED_(6.0f), move_(0, 0, 0), selfDir_(0, 0, 1.0f),
	currentParentDir_(0, 0, 1.0f), newParentDir_(0, 0, 1.0f), isRotate_(false),
	sign_(0)
{
}


CameraTarget::~CameraTarget()
{
}

//初期化
void CameraTarget::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Data/Models/Player.fbx");
	assert(hModel_ >= 0);

	rotate_.y = 0.0f;

	pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0.0f, 3.0f, -1.0f));		//カメラの位置（上斜め後ろ）
	pCamera->SetTarget(position_);								//カメラの焦点
}

//更新
void CameraTarget::Update()
{
	//基準とするオブジェクトの位置に合わせる
	position_ = pStandardObj_->GetPosition();

	D3DXVECTOR3 dir = D3DXVECTOR3(0, 0, 1.0f);

	//行列でベクトルを変形
	TransformVector(D3DXToRadian(rotate_.y), &selfDir_, &dir);

	//カメラ上
	if (Input::IsKey(DIK_9))
	{
		//カメラの位置（真上から）
		pCamera->SetPosition(D3DXVECTOR3(0.0f, 10.0f, -0.01f));
	}
	//カメラ上斜め後ろ
	else if (Input::IsKey(DIK_SPACE))
	{
		//カメラの位置（上斜め後ろ）
		pCamera->SetPosition(D3DXVECTOR3(0.0f, 3.0f, -1.0f));
	}
	//カメラ前
	else if (Input::IsKey(DIK_0))
	{
		//カメラの位置（前）
		pCamera->SetPosition(D3DXVECTOR3(0.0f, 0.0f, -0.01f));
	}

	//親（プレイヤー）の向きを取得
	newParentDir_ = ((Player*)pStandardObj_)->GetCurrentDir();


	//新たに取得した方向と、既存の方向が違うなら
	if (newParentDir_ != currentParentDir_)
	{
		//方向を更新する
		currentParentDir_ = newParentDir_;

		//回転すると判断
		isRotate_ = true;

		//基準となるベクトル
		dir = D3DXVECTOR3(0, 0, 1);

		//基準の方向ベクトルと自分の方向ベクトルの内積から角度を求める
		float angle = CalcAngle(&dir, &selfDir_);

		//自分の方向ベクトルのx成分が0より大きいなら
		if (selfDir_.x > 0)
		{
			//基準オブジェクトの方向ベクトルに掛け合わせる角度を-にする
			angle *= -1;
		}

		//変形する対象のベクトルをコピー
		D3DXVECTOR3 changeDir = currentParentDir_;

		//行列でベクトルを変形
		TransformVector(angle, &changeDir, &changeDir);

		//引数に指定したベクトルのx成分の符号を取得
		sign_ = JudgeSign(changeDir.x);
	}

	//今回転するべきならば
	if (isRotate_)
	{
		//回転する
		Rotate(sign_);
	}
}

//描画
void CameraTarget::Draw()
{
	//自機と同じ位置に配置する（描画はしない）
	Model::SetMatrix(hModel_, worldMatrix_);
}

//開放
void CameraTarget::Release()
{
}

//回転する
void CameraTarget::Rotate(int sign)
{
	//基準オブジェクトの方向をコピーする
	D3DXVECTOR3 dir = currentParentDir_;

	//内積から角度を求める
	float angle = CalcAngle(&dir, &selfDir_);

	//ラジアンから度に変換
	angle = D3DXToDegree(angle);

	//度の値が一定より大きいなら
	if (angle > 0.1f)
	{
		//度が回転分より大きい時は
		if (angle > ROTATE_SPEED_)
		{
			//回転分だけ回転する
			rotate_.y += (ROTATE_SPEED_ * sign);
		}
		//小さい時は
		else
		{
			//回転分ではなく、度の値分だけ回転する
			rotate_.y += (angle * sign);
		}
	}
	//一定より小さい時は
	else
	{
		//回転しない
		isRotate_ = false;
	}

	//回転度を調整する
	RotationAdjustment();
}

//行列を使用してベクトルを変形する
void CameraTarget::TransformVector(float angle, D3DXVECTOR3* changeDir, D3DXVECTOR3* applyDir)
{
	//引数の角度で行列を回転
	D3DXMATRIX m;
	D3DXMatrixRotationY(&m, angle);

	//行列でベクトルを変形
	D3DXVec3TransformCoord(changeDir, applyDir, &m);
}

//内積を求め、角度を返す
float CameraTarget::CalcAngle(D3DXVECTOR3* vecA, D3DXVECTOR3* vecB)
{
	//正規化
	D3DXVec3Normalize(vecA, vecA);
	D3DXVec3Normalize(vecB, vecB);

	//基準と自分のベクトルの内積を求める
	float dot = D3DXVec3Dot(vecB, vecA);

	//角度を求める
	float angle = acos(dot);

	return angle;
}

//引数の値の大きさによって+か-なのかを判断する
int CameraTarget::JudgeSign(float vecX)
{
	int sign = 0;

	//引数のxが0より小さければ
	if (vecX < 0)
	{
		//-回転させる
		return sign = -1;
	}

	//+回転させる
	return sign = 1;
}

//回転度合いの数値を調整する
void CameraTarget::RotationAdjustment()
{
	//四捨五入して、さらにその絶対値が360度になったら最初に戻す
	if (fabs(round(rotate_.y)) == 360)
	{
		rotate_.y = 0.0f;
	}
}
