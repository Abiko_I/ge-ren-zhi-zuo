#include "CharacterFootL.h"
#include "Character.h"
#include "Engine/Model.h"

//コンストラクタ
CharacterFootL::CharacterFootL(IGameObject * parent)
	:CharacterFoot(parent, "CharacterFootL")
{
}

//デストラクタ
CharacterFootL::~CharacterFootL()
{
}

//初期化
void CharacterFootL::Initialize()
{
	sign_ = -1;

	//移動速度
	moveSpeed_ = 0.0f;
}

//更新
void CharacterFootL::Update()
{
	//動くべきかを判断する
	if (((Character*)pParent_)->IsMoveFeet())
	{
		//足を動かして歩く
		Move();
	}
	else
	{
		//足を止める
		Stop();
	}
}

//描画
void CharacterFootL::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void CharacterFootL::Release()
{
}