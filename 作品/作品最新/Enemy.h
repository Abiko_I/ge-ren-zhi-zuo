#pragma once
#include <list>
#include <vector>
#include "Engine/IGameObject.h"
#include "Character.h"

class Stage;
class Player;
class CharacterAI;
class NavigationAI;

struct BranchPosInfo;

//敵を管理するクラス
class Enemy : public Character
{
public:
	//敵の状態
	enum STATE
	{
		CHASE,		//プレイヤーを発見したから追跡するとき
		PATROL,		//プレイヤーをまだ発見していないから巡回するとき
		SEARCH,		//一時的にプレイヤーを見失ったが、探すとき
	};

private:
	//ステージの種類
	enum
	{
		TYPE_WALL = 0,	//壁
		TYPE_FLOOR,		//床
		TYPE_PLAYER,	//プレイヤーの初期位置
		TYPE_ENEMY,		//敵の初期位置
		TYPE_MAX
	};


	const D3DXVECTOR3 START_POS_;	//敵の初期位置
	const float RADIUS_;			//敵の半径
	const float NORMAL_SPEED_;		//通常の移動速度
	const float ACCELE_SPEED_;		//加速時の移動速度
	const float NORMAL_SPEED_FOOT_;	//通常の足を動かす速度
	const float ACCELE_SPEED_FOOT_;	//加速時の足を動かす速度
	const int WAIT_TIME_;			//見渡すときの待ち時間

	//ステージの情報
	struct StageInfo
	{
		int type_;		//ステージ上に生成されたオブジェクトの種類
		int cost_;		//その位置のコスト
	};

	//ステージの部品の情報
	struct PosInfo
	{
		D3DXVECTOR3 pos;	//その部品の位置
		DIRECTION dir;		//自分から見てどの方向にあるのか
	};

	//キャラクターの位置（引数で渡す時に使う）
	struct CharaPos
	{
		int z_;
		int x_;
	};

	//探索用（ステージ上での方向）
	int searchPos_[4][2] = {
		{-1, 0},	//左
		{0, -1},	//下
		{0, 1},		//上
		{1, 0}		//左
	};

	//位置、歩数、推定値、実歩数をまとめたもの
	struct Info
	{
		int z_;			//縦
		int x_;			//横
		int dist_;		//歩数
		int est_;		//推定値
		int realDist_;	//推定歩数
		int prevZ_;		//自分の一つ前のy
		int prevX_;		//自分の一つ前のx
		DIRECTION dir_;	//方向
	};

	//モデル番号
	//int hModel_[3];
	int hModel_;

	//敵の移動速度
	float moveSpeed_;

	//探索関数を呼ぶかどうか
	bool isCallSearch_;

	//敵の状態
	STATE state_;

	//キャラクターAIのポインタ
	CharacterAI* pCharaAI_;

	//ナビゲーションAIのポインタ
	NavigationAI* pNaviAI_;

	//今動くべきかどうか
	bool isMove_;

	//現在の方向
	int currentDir_;

	//次の方向
	int nextDir_;

	//指示を聞くかどうか
	bool isAccept_;

	//行動の間隔
	int waitCount_;

	//仕事
	int task_;

	//HLSLから作成されたシェーダを入れる
	LPD3DXEFFECT* pEffect_;

public:
	//コンストラクタ
	Enemy(IGameObject* parent);

	//デストラクタ
	~Enemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった時の処理
	//引数：当たった相手
	//戻り値：なし
	void OnCollision(IGameObject* pTarget) override;

	//足を動かすべきかどうかを判断する
	//引数：なし
	//戻り値：移動中ならtrue、回転中ならfalse
	bool IsMoveFeet() override;

	//巡回状態のときの更新
	//引数：なし
	//戻り値：なし
	void PatrolUpdate();

	//追跡状態のときの更新
	//引数：なし
	//戻り値：なし
	void ChaseUpdate();

	//探索状態のときの更新
	//引数：なし
	//戻り値：なし
	void SearchUpdate();

	//近くを見渡す
	//引数：なし
	//戻り値：なし
	void Survey();

	//目標地点に向かうための経路を調べる
	//引数：なし
	//戻り値：なし
	void RouteSearch();

	//ステージのポインタをもらう
	//引数：ステージのポインタ、自分が生成されたときの番号
	//戻り値：なし
	void SetStage(Stage* pStage, int num);

	//プレイヤーのポインタをもらう
	//引数：プレイヤーのオブジェクトのポインタ
	//戻り値：なし
	void SetPlayer(Player* pPlayer);

	//向くべき方向をもらう
	//引数：次に移動する方向、第一引数の方向を向くために必要な回転度数
	//戻り値：なし
	void SetDirection(int nextDir, float degree);

	//指定された方向を向いて進む
	//引数：なし
	//戻り値：なし
	void ChangeSpeed();

	//引数の状況から、状態を遷移する
	//引数：キャラクターAIが持つ状況
	//戻り値：なし
	void ChangeState(int situation);

	//初期位置を教えてもらい、決定する
	//引数：xとzの位置
	//戻り値：なし
	void SetStartPos(float x, float z) { position_ = D3DXVECTOR3(x, RADIUS_, z); }

	//探索関数を呼ぶかを教えてもらう
	//引数：呼ぶかどうかの真偽
	//戻り値：なし
	void SetCallFlag(bool flag) { isCallSearch_ = flag; }

	//状態を変える
	//引数：変更後の状態
	//戻り値：なし
	void SetState(STATE state){ state_ = state; }

	//引数で指定された位置の部品タイプをナビゲーションAIから教えてもらい、呼び出し元に返す
	//引数：zの位置、xの位置
	//戻り値：指定された位置の部品タイプ
	int GetStage(int z, int x);

	//自分の状態を教える
	//引数：なし
	//戻り値：自分の状態
	STATE GetState() { return state_; }

	//ナビゲーションAIからプレイヤーのモデル番号を教えてもらい、呼び出し元に返す
	//引数：なし
	//戻り値：プレイヤーのモデル番号
	int GetPlayerModel();

	//ナビゲーションAIから壁のモデル番号リストを教えてもらい、呼び出し元に返す
	//引数：なし
	//戻り値：壁のモデル番号リスト
	std::vector<int>* GetWallModel();

	//命令を受け付けるかどうかを教える
	//引数：なし
	//戻り値：受け付けるならtrue、受け付けないならfalse
	bool isAccept() { return isAccept_; }
	
	//ナビゲーションAIの分岐を数える関数を呼ぶ
	//引数：基準となる位置z、基準となる位置x、世界から見たときの親（敵）の向き、
	//		分岐の個数
	//戻り値：分岐の数
	int CallCountBranch(int posZ, int posX, int dir, int branchCnt);

	//ナビゲーションAIの、プレイヤーが通った道のコストを下げる関数を呼ぶ
	//引数：なし
	//戻り値：なし
	void CallCostDown();

	//ナビゲーションAIの、分岐のある道のリストのポインタを返す関数を呼ぶ
	//引数：なし
	//戻り値：分岐のある道のリストのポインタ
	void CallGetBranchList();

	//今の向きを渡す
	//引数：なし
	//戻り値：今の向き
	int GetDirection() { return currentDir_; }
};