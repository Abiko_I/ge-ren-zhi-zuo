#pragma once
#include "CharacterFoot.h"
#include "Engine/IGameObject.h"

//キャラクターの左足を管理するクラス
class CharacterFootL : public CharacterFoot
{
private:

public:
	//コンストラクタ
	CharacterFootL(IGameObject* parent);

	//デストラクタ
	~CharacterFootL();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};