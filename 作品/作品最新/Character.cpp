#include "Character.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"
#include "Stage.h"
#include "Wall.h"

Character::Character(IGameObject * parent, std::string name) : IGameObject(parent, name),
	addMove_(0, 0, 1), addRotate_(0, 0, 0)
{
}


Character::~Character()
{
}

//初期化
void Character::Initialize()
{
}

//更新
void Character::Update()
{
}

//描画
void Character::Draw()
{
}

//開放
void Character::Release()
{
}

//移動
void Character::SetMoveVector(float x, float z)
{
	addMove_ += D3DXVECTOR3(x, 0, z);
}

//めり込んだ時の位置調節
void Character::PositionAdjustment(IGameObject * pTarget, float radius)
{
	//相手の位置を取得
	D3DXVECTOR3 targetPos = ((Wall*)pTarget)->GetCollider()->GetCenter();

	//プレイヤーの位置調整用
	D3DXVECTOR3 CharacterPos = D3DXVECTOR3(position_.x, 0.5f, position_.z);

	//当たった相手から自分に向かうベクトルを求める
	D3DXVECTOR3 returnPos = CharacterPos - targetPos;

	//コライダーを取得
	Collider* col = ((Wall*)pTarget)->GetCollider();

	//求めたベクトルの、x、zの長い方向を見る
	if (fabs(returnPos.x) > fabs(returnPos.z))
	{
		//xの方向を取得
		D3DXVECTOR3 dir;

		if (returnPos.x >= 0)
		{
			//xの方向を取得（+）
			dir = ((BoxCollider*)col)->GetDirection(0);
		}
		else
		{
			//xの方向を取得（-）
			dir = -((BoxCollider*)col)->GetDirection(0);
		}

		//横幅の半分の長さを求める
		float len = ((BoxCollider*)col)->GetLength(0);

		//xのめり込んだ分を戻す
		position_.x = dir.x * (len + radius) + targetPos.x;
	}
	else
	{
		//xの方向を取得
		D3DXVECTOR3 dir;

		if (returnPos.z >= 0)
		{
			dir = ((BoxCollider*)col)->GetDirection(2);
		}
		else
		{
			//zの方向を取得
			dir = -((BoxCollider*)col)->GetDirection(2);
		}

		//横幅の半分の長さを求める
		float len = ((BoxCollider*)col)->GetLength(2);

		//xのめり込んだ分を戻す
		position_.z = dir.z * (len + radius) + targetPos.z;
	}
}

//引数のx成分とz成分から、方向を求めて返す
int Character::JudgeDirection(float posX, float posZ)
{
	if (fabs(posX) > fabs(posZ))
	{
		if (posX > 0.0f)
		{
			//世界から見て右
			return RIGHT;
		}
		else
		{
			//世界から見て左
			return LEFT;
		}
	}
	else
	{
		if (posZ > 0.0f)
		{
			//世界から見て前
			return FORWARD;
		}
		else
		{
			//世界から見て後ろ
			return BACKWARD;
		}
	}
}
