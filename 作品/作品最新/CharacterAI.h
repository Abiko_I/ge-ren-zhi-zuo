#pragma once
#include "AI.h"

struct RayCastData;

//キャラクターAIを管理するクラス
class CharacterAI : public AI
{
public:
	//現在の状況を示す
	enum
	{
		FOUND_PLAYER,		//プレイヤーを見つけた
		LOST_SIGHT_PLAYER,	//プレイヤーを見失った
		NOT_FOUND_PLAYER	//プレイヤーが見つからない、見つかっていない（初期状態）
	};

	enum
	{
		BEHAVIOR_WALK,
		BEHAVIOR_ROTATE
	};

private:

	//記録したレイが何時なのかを示す
	enum
	{
		TIME_LAST,		//前回
		TIME_CURRENT	//現在
	};

	//レイの情報を記録する構造体
	struct RayRecord
	{
		float x;			//レイのxの成分
		float z;			//レイのzの成分
		bool isHitPlayer;	//このレイがプレイヤーと当たったかどうか
	};

	//敵の移動速度
	float enemySpeed_;

	//プレイヤーのモデル番号
	int hPlayerModel_;

	//目標地点
	CharaPos targetPos_;

	//自分が通れる場所を記録する
	std::list<PosInfo> pos_;

	//目標地点を指す
	std::list<PosInfo>::iterator it_;

	//次の目標地点を指す
	std::list<PosInfo>::iterator nextIt_;

	//レイの情報を記録する
	RayRecord record_[2];

	//現在の状況
	int situation_;

	//動作
	int behavior_;

	//分岐地点を見るときのイテレーター
	std::vector<BranchPosInfo>::iterator branchIt_;

	//方向の配列
	float direction_[4][4];

	//目標地点が欲しいかどうか
	bool request_;

	//小数点での目標地点
	float targetPosFloat_[2];

public:
	//コンストラクタ
	CharacterAI(IGameObject* parent);

	//デストラクタ
	~CharacterAI();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//プレイヤーを見つけているときの更新
	//引数：なし
	//戻り値：なし
	void FoundUpdate();

	//プレイヤーが視界からいなくなったが、探すときの更新
	//引数：なし
	//戻り値：なし
	void LostSightUpdate();

	//状況を変える
	//引数：現在の状況
	//戻り値：なし
	void ChangeSituation(int newSituation);

	//敵の移動速度をもらう
	//引数：敵の移動速度
	//戻り値：なし
	void SetSpeed(float speed) { enemySpeed_ = speed; }

	//目標地点を設定する
	//引数：目標地点のz位置、目標地点のx位置
	//戻り値：なし
	void SetTarget(int z, int x);

	//分岐地点が記録されている一覧をもらう
	//引数：分岐地点が記録されている一覧のポインタ
	//戻り値：なし
	void SetBranchPosList(std::vector<BranchPosInfo>* list);

	//要請をリセット（解除）する
	//引数：なし
	//戻り値：なし
	void ResetRequest() { request_ = false; }

	//位置リストのポインタを渡す
	//引数：なし
	//戻り値：位置リストのポインタ
	std::list<PosInfo>* GetPosList() { return &pos_; }

	//目標地点を返す
	//引数：なし
	//戻り値：目標地点
	CharaPos GetTarget() { return targetPos_; }

	//発見したかどうかの状況を渡す
	//引数：なし
	//戻り値：発見した時は真、発見していない時は偽
	int GetSituation() { return situation_; }

	//動作を渡す
	//引数：なし
	//戻り値：動作
	int GetBehavior() { return behavior_; }

	//見渡すための角度を渡す
	//引数：なし
	//戻り値：なし
	void GetRotateForSurvey(int parentDir);

	//今、目標地点が欲しいかどうかを教える
	//引数：なし
	//戻り値：目標を要請しているときはtrue、要請していないときはfalse
	bool GetRequest() { return request_; }

	//向きを変える
	//引数：なし
	//戻り値：なし
	void ChangeDirection();

	//目標地点に到着したかどうかを判断し、目標地点に移動するか、更新する
	//引数：自分の位置（zかx）と目標地点の位置（zかx）が格納された配列、前後の方向、左右の方向、
	//		左右の移動量、前後の移動量
	//戻り値：なし
	void JudgeNextMove(float * pos, int dir, float moveX, float moveZ);

	//イテレーターを初期化する
	//引数：なし
	//戻り値：なし
	void InitIterator();

	//目標地点を決める
	//引数：床の位置リストのポインタ
	//戻り値：なし
	void DecideTarget(std::vector<CharaPos>* floorList);

	//プレイヤーが視覚に入っているか確認する
	//引数：なし
	//戻り値：なし
	void VisualConfirmation();

	//引数で渡された番号のモデルにレイを撃つ
	//引数：対象のモデル番号、発射位置から対象までの距離を格納する変数のポインタ、レイの情報を格納する構造体のポインタ
	//戻り値：なし
	void RayCast(int handle, float* distTarget, RayRecord* record);

	//記録した２つのレイの内積を求め、角度を見て向きが違うかどうかを判断する
	//引数：なし
	//戻り値：方向が違うならtrue、同じならfalse
	bool IsDirDifferent();

	//渡された目標地点のx、z位置から、親（敵）がいるマスと同じ位置かどうかを判断する
	//引数：位置z、位置x
	//戻り値：引数で渡された位置が親（敵）の現在位置と同じマスならtrue、
	//		  違うマスならfalse
	bool IsTargetSamePos(float z, float x);

	//親（敵）の位置と目標地点が同じときの動作
	//引数：なし
	//戻り値：なし
	void TargetSamePosMovement();
};