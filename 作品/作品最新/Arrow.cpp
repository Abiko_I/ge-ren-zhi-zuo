#include "Arrow.h"
#include "Player.h"
#include "Enemy.h"
#include "Engine/Model.h"

//コンストラクタ
Arrow::Arrow(IGameObject * parent)
	:IGameObject(parent, "Arrow"), isDraw_(false)
{
}

//デストラクタ
Arrow::~Arrow()
{
}

//初期化
void Arrow::Initialize()
{
	//モデルデータのロード
	hModel_[Enemy::CHASE] = Model::Load("Data/Models/Arrow/Arrow_Chase.fbx");
	assert(hModel_[Enemy::CHASE] >= 0);
	hModel_[Enemy::PATROL] = Model::Load("Data/Models/Arrow/Arrow_Patrol.fbx");
	assert(hModel_[Enemy::PATROL] >= 0);
	hModel_[Enemy::SEARCH] = Model::Load("Data/Models/Arrow/Arrow_Search.fbx");
	assert(hModel_[Enemy::SEARCH] >= 0);

	rotate_ = D3DXVECTOR3(0, 0, 0);
}

//更新
void Arrow::Update()
{
	//自分の位置を、中心とするオブジェクトの位置に合わせる
	position_ = pCenterObj_->GetPosition();

	//ベクトルを求める
	D3DXVECTOR3 vec = CalcVector();

	//上記のベクトルの長さを求める
	float length = D3DXVec3Length(&vec);

	//一定より短いとき
	if (length >= 4.0)
	{
		//描画する
		isDraw_ = true;

		//向きを設定
		ChangeDirection(isReverse(vec), vec);
	}
	else
	{
		//描画しない
		isDraw_ = false;
	}
}

//描画
void Arrow::Draw()
{
	int model = 0;

	//自分が見る敵の状態を見る
	switch (((Enemy*)pTargetObj_)->GetState())
	{
		//追跡なら
		case Enemy::CHASE:
			//敵が追跡状態であることを表す色のモデル（赤）
			model = hModel_[Enemy::CHASE];
			break;

		//巡回なら
		case Enemy::PATROL:
			//敵が巡回状態であることを表す色のモデル（青）
			model = hModel_[Enemy::PATROL];
			break;

		//探索なら
		case Enemy::SEARCH:
			//敵が探索状態であることを表す色のモデル（黄）
			model = hModel_[Enemy::SEARCH];
			break;
	}

	if (isDraw_)
	{
		Model::SetMatrix(model, worldMatrix_);
		Model::Draw(model);
	}
}

//開放
void Arrow::Release()
{
}

//ベクトルを求める
D3DXVECTOR3 Arrow::CalcVector()
{
	//自分の位置からターゲットの位置へのベクトルを求める
	D3DXVECTOR3 vec = position_ - pTargetObj_->GetPosition();

	return vec;
}

//引数のベクトルのx成分から、反転させるかどうかを決める
bool Arrow::isReverse(D3DXVECTOR3 vec)
{
	if (vec.x > 0)
	{
		//逆にする
		return true;
	}

	//そのまま使う
	return false;
}

//向きを変える
void Arrow::ChangeDirection(bool isReverse, D3DXVECTOR3 vec)
{
	//奥を向いているベクトルを基準とする
	D3DXVECTOR3 pos = D3DXVECTOR3(0, 0, -1);

	//それぞれのベクトルを正規化
	D3DXVec3Normalize(&vec, &vec);
	D3DXVec3Normalize(&pos, &pos);

	//内積を求める
	float dot = D3DXVec3Dot(&vec, &pos);

	//角度を求める
	float angle = acos(dot);

	//逆にするように指示が出ているなら
	if (isReverse)
	{
		//角度を反転させてラジアンから度に修正
		angle = D3DXToDegree(angle * -1);
	}
	else
	{
		//そのままの角度をラジアンから度に修正
		angle = D3DXToDegree(angle);
	}

	//回転する
	rotate_.y = angle;
}
