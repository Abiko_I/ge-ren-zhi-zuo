#pragma once
#include "Engine/IGameObject.h"

class CharacterFootR;
class CharacterFootL;

//キャラクターを管理するクラス
class Character : public IGameObject
{
protected:
	//ステージを構成する部品の内部数値
	enum
	{
		TYPE_WALL = 0,	//壁
		TYPE_FLOOR,		//床
		TYPE_PLAYER,	//プレイヤーの初期位置
		TYPE_ENEMY,		//敵の初期位置
		TYPE_FLOOR_B,
		TYPE_MAX
	};

	//キャラクターの向きを指す
	enum DIRECTION
	{
		FORWARD,	//前を向いている
		RIGHT,		//右を向いている
		LEFT,		//左を向いている
		BACKWARD,	//後ろを向いている
		NONE		//方向無し
	};

	//キャラクターの方向配列の添え字
	enum
	{
		F_B,	//上下
		R_L	//左右
	};

	//体色
	struct Color
	{
		float red;		//RGBAのRの値
		float green;	//RGBAのGの値
		float blue;		//RGBAのBの値
		float alpha;	//RGBAのAの値
	};

	//どれくらい移動するのか
	D3DXVECTOR3 addMove_;

	//どれくらい移動するのか
	D3DXVECTOR3 addRotate_;

	//キャラクターの方向
	DIRECTION direction_[2];

	//キャラクターの足のモデルの名前
	std::string footModelPath_[2];

	//左右の足のオブジェクトのポインタ
	CharacterFootR* pFootR;
	CharacterFootL* pFootL;

	//状態毎の体色を格納する
	Color bodyColor_[3];

	//足を動かすかどうかのフラグ
	bool isMoveFeet_;

public:
	//コンストラクタ
	Character(IGameObject* parent, std::string name);

	//デストラクタ
	~Character();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//どれくらい移動するか
	//引数：移動する値
	//戻り値：なし
	void SetMoveVector(float x, float z);

	//何かに当たった時にめり込んだ部分を戻す
	//引数：当たった相手、自分の半径
	//戻り値：なし
	void PositionAdjustment(IGameObject * pTarget, float radian);

	//引数のx成分とz成分から、方向を求めて返す
	//引数：x成分とz成分
	//戻り値：方向
	int JudgeDirection(float posX, float posZ);

	//足を動かすべきかどうかを判断する
	//引数：なし
	//戻り値：移動中ならtrue、移動していないならfalse
	bool GetIsMoveFeet() { return isMoveFeet_; }
};