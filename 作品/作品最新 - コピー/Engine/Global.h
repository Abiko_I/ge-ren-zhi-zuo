#pragma once
#include <d3dx9.h>
#include <assert.h>
#include "Input.h"
#include "IGameobject.h"
#include "SceneManager.h"

//解放処理のマクロ（デリート）
#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}

//解放処理のマクロ（デリート、配列版）
#define SAFE_DELETE_ARRAY(p) if(p != nullptr){ delete[] p; p = nullptr;}

//解放処理のマクロ（リリースを呼んで削除させる）
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}

struct Global
{
	int screenWidth;		//スクリーン幅
	int screenHeight;		//スクリーン高さ
};
extern Global g;		//グローバル型のg、スクリーンの幅と高さを入れる
