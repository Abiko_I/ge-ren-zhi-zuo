#pragma once
#include "Global.h"

namespace Direct3D	//この中の関数はDirect3Dのものである
{
	extern LPDIRECT3D9	pD3d;			//Direct3Dオブジェクト
	extern LPDIRECT3DDEVICE9 pDevice;	//Direct3Dデバイスオブジェクト

	extern bool isDrawCollision_;		//コリジョンを表示するか
	extern bool isLighting_;			//ライティングするか

	void Initialize(HWND hWnd);		//初期化、引数：hWnd(ウィンドウハンドル)、戻り値：なし
	void BeginDraw();				//描画開始、引数：なし、戻り値：なし
	void EndDraw();					//描画終了、引数：なし、戻り値：なし
	void Release();					//開放、引数：なし、戻り値：なし
}