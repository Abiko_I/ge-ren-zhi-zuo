#pragma once
#include "IGameObject.h"

//シーンのIDを列挙
enum SCENE_ID
{
	SCENE_ID_TITLE = 0,	//タイトルシーン
	SCENE_ID_PLAY,		//プレイシーン
	SCENE_ID_CLEAR,		//クリアシーン
	SCENE_ID_GAMEOVER	//ゲームオーバーシーン
};

//各シーンを管理するクラス
class SceneManager : public IGameObject
{
	//今のシーン・静的メンバ変数
	static SCENE_ID currentSceneID_;
	//次のシーン・静的メンバ変数
	static SCENE_ID nextSceneID_;
	//現在のシーンのアドレスを保存　setCurrentScene();を作れ
	static IGameObject* pCurrentScene_;

public:

	//コンストラクタ
	SceneManager(IGameObject* parent);

	//デストラクタ
	~SceneManager();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//シーン切り替え・静的メンバ関数なので静的変数を扱える
	static void ChangeScene(SCENE_ID next);

	//現在のシーンを渡す
	static IGameObject* GetCurrentScene()
	{
		return pCurrentScene_;
	}
};