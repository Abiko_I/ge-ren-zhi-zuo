#pragma once
#include "Character.h"

class Stage;

//プレイヤーを管理するクラス
class Player : public Character
{
private:
	enum STATE
	{
		FINE,	//元気で、走ることが出来る
		TIRED,	//疲れていて、走ることが出来ない
		RUN		//走っている
	};

	const float RADIUS_;			//自機の半径
	const float NORMAL_SPEED_;		//通常の移動速度
	const float ACCELE_SPEED_;		//加速時の移動速度
	const float NORMAL_SPEED_FOOT_;	//通常の足を動かす速度
	const float ACCELE_SPEED_FOOT_;	//加速時の足を動かす速度
	const float STAMINA_MAX_;		//スタミナの最大値

	//モデル番号
	int hModel_;

	//自機の移動速度
	float moveSpeed_;

	//クリスタルの数
	int crystalCnt_;

	//x、zの移動量
	float moveX_;
	float moveZ_;

	//現在の方向
	int currentDir_;

	//次の方向
	int nextDir_;

	//ステージ
	int stage_[HEIGHT][WIDTH];

	//加速する
	bool toAccelerate_;

	//スタミナ
	int stamina_;

	//状態
	STATE state_;

	//移動ベクトルの回転の度合い
	float rotateValue_;

	//次の移動ベクトル（予約されているベクトル）
	D3DXVECTOR3 nextAddMove_;

	//入力されたかどうか
	bool isInput_;

	//前の位置
	D3DXVECTOR3 prevPos_;

	//HLSLから作成されたシェーダを入れる
	LPD3DXEFFECT pEffect_;

public:
	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった時の処理
	//引数：当たった相手
	//戻り値：なし
	void OnCollision(IGameObject* pTarget) override;

	//入力に対する行動を起こす
	//引数：なし
	//戻り値：なし
	void Input();

	//方向を更新する
	//引数：
	//戻り値：なし
	void DirectionUpdate();

	//速度を変える
	//引数：なし
	//戻り値：なし
	void ChangeSpeed();

	//スタミナの管理をする
	//引数：なし
	//戻り値：なし
	void StaminaManagement();

	//次の方向に曲がれるか確認
	//引数：なし
	//戻り値：なし
	void CheckAround();

	//内積を求め、角度を返す
	//引数：ベクトル２つ
	//戻り値：角度（ラジアン）
	float CalcAngle(D3DXVECTOR3* dirA, D3DXVECTOR3* dirB);

	//移動ベクトルを回転させる
	//引数：回転させる度合い、変形させる対象のベクトル
	//戻り値：なし
	void DirectionRotate(float angle, D3DXVECTOR3* vec);

	//移動する
	//引数：なし
	//戻り値：なし
	void AddMove();

	//引数の値の大きさによって+か-なのかを判断する
	//引数：ベクトルのx成分
	//戻り値：引数の値が0より大きいなら1、小さいなら-1
	int JudgeSign(float vecX);

	//次の方向を向く
	//引数：位置A、位置B
	//戻り値：なし
	void Execution(float posA, float posB, D3DXVECTOR3* copyVec);

	//初期位置を教えてもらう（zとxのみ）
	//引数：zとxの位置が格納された配列のポインタ
	//戻り値：なし
	void SetStartPos(float* pos);

	//ステージをコピーする
	//引数：ステージのポインタ
	//戻り値：なし
	void SetStage(Stage* pStage);

	//今の方向に対応するベクトルを返す
	//引数：なし
	//戻り値：方向に対応したベクトル
	D3DXVECTOR3 GetCurrentDir();

	//渡された引数の方向に対応するベクトルを返す
	//引数：今、次のどちらかの方向
	//戻り値：方向に対応したベクトル
	D3DXVECTOR3 GetCurrentDir(int dir);

	//クリスタルの数をもらう
	//引数：クリスタルの数
	//戻り値：なし
	void SetCrystalCnt(int cnt) { crystalCnt_ = cnt; }

	//モデル番号を返す
	//引数：なし
	//戻り値：モデル番号
	int GetModelHandle();
};