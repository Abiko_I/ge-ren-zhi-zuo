#include "Player.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"
#include "Stage.h"
#include "Wall.h"
#include "CharacterFootR.h"
#include "CharacterFootL.h"
#include "Engine/Audio.h"

Player::Player(IGameObject * parent) : Character(parent, "Player"),
	RADIUS_(0.35f), NORMAL_SPEED_(0.05), ACCELE_SPEED_(0.06), NORMAL_SPEED_FOOT_(2.0f),
	ACCELE_SPEED_FOOT_(3.0f), STAMINA_MAX_(300), hModel_(-1), moveSpeed_(0.05f),
	crystalCnt_(0), moveX_(0.0f), moveZ_(0.0f), currentDir_(FORWARD), nextDir_(NONE),
	toAccelerate_(false), state_(FINE), rotateValue_(0.0f), nextAddMove_(0, 0, 1),
	isInput_(false), prevPos_(0.0f, 0.0f, 0.0f), pEffect_(nullptr)
{
	//状態毎の体色を格納
	bodyColor_[FINE] =	{ 0.2f, 0.9f, 1.0f, 1.0f };	//水色
	bodyColor_[RUN] =	{ 1.0f, 0.6f, 0.0f, 1.0f };	//橙色
	bodyColor_[TIRED] = { 0.6f, 0.6f, 0.6f, 1.0f };	//灰色
}


Player::~Player()
{
	//ポインタだから
	SAFE_RELEASE(pEffect_);
}

//初期化
void Player::Initialize()
{
	//HLSLファイルからエフェクト（シェーダ）を生成
	//エラーが出た結果を入れる
	LPD3DXBUFFER err = 0;

	//エラーが出たとき
	if (FAILED(
		D3DXCreateEffectFromFile(Direct3D::pDevice, "ColorChangeShader.hlsl",
			NULL, NULL, D3DXSHADER_DEBUG, NULL, &pEffect_, &err)))
	{
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダーエラー", MB_OK);
	}

	assert(pEffect_ != nullptr);

	//モデルデータのロード
	hModel_ = Model::Load("Data/Models/Player/Player_Body.fbx", pEffect_);
	assert(hModel_ >= 0);

	//最初は前を向かせる
	direction_[0] = FORWARD;
	direction_[1] = NONE;
	rotate_.y = 0.0f;
	position_ = D3DXVECTOR3(1.0f, 2.0f, 1.0f);

	//初期は通常速度
	moveSpeed_ = NORMAL_SPEED_;

	//スタミナは最大
	stamina_ = STAMINA_MAX_;

	//球体コライダーをつける
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), RADIUS_);
	AddCollider(collision);

	//足のモデルのパスを格納
	footModelPath_[0] = "Player/Player_Foot_R";
	footModelPath_[1] = "Player/Player_Foot_L";

	//右足を生成し、パスを渡してモデルを読み込ませる
	pFootR = CreateGameObject<CharacterFootR>(this);
	pFootR->LoadModel(footModelPath_[0]);

	//速度を決める
	pFootR->ChangeSpeed(NORMAL_SPEED_FOOT_);

	//左足を生成し、パスを渡してモデルを読み込ませる
	pFootL = CreateGameObject<CharacterFootL>(this);
	pFootL->LoadModel(footModelPath_[1]);

	//速度を決める
	pFootL->ChangeSpeed(NORMAL_SPEED_FOOT_);
}

//更新
void Player::Update()
{
	//移動ベクトルの初期化
	addMove_ = D3DXVECTOR3(0, 0, moveSpeed_);

	//スタミナが空になったら
	if (stamina_ <= 0)
	{
		//疲労状態に遷移
		state_ = TIRED;
		stamina_ = 0;

		//走れなくなる
		toAccelerate_ = false;

		//速度変更
		ChangeSpeed();
	}
	//スタミナが全回復したら
	else if (stamina_ >= STAMINA_MAX_)
	{
		//元気な状態に遷移
		state_ = FINE;
		stamina_ = STAMINA_MAX_;

		//速度変更
		ChangeSpeed();
	}

	//現在の向きを示すベクトルになる
	DirectionRotate(rotate_.y, &addMove_);

	//今の方向を決める
	currentDir_ = JudgeDirection(addMove_.x, addMove_.z);

	//入力処理
	Input();

	//スタミナの管理をする
	StaminaManagement();

	//次の行動が指定されている時
	if (nextDir_ != NONE)
	{
		//次の方向に曲がれるか確認
		CheckAround();
	}

	//四捨五入して、さらにその絶対値が360度になったら最初に戻す
	if (fabs(round(rotate_.y)) == 360)
	{
		rotate_.y = 0.0f;
	}

	//入力があったら動き始める
	if (isInput_)
	{
		//移動
		AddMove();
	}

	//位置を更新する
	pFootL->SetParentPos(position_);
	pFootR->SetParentPos(position_);

	//今動いているなら
	if (prevPos_ != position_)
	{
		//今の位置を前回の位置として保存
		prevPos_ = position_;
		isMoveFeet_ = true;
	}
	else
	{
		//足を止める
		isMoveFeet_ = false;
	}
}

//描画
void Player::Draw()
{
	//状態によって体色を指定する
	pEffect_->SetVector("SPECIFIED_DIFFUSE", &D3DXVECTOR4(
		bodyColor_[state_].red,
		bodyColor_[state_].green,
		bodyColor_[state_].blue,
		bodyColor_[state_].alpha
	));

	Model::SetMatrix(hModel_, worldMatrix_);

	//ビュー行列
	D3DXMATRIX view;
	//第一引数で渡したものを第二引数で渡したアドレス元に入れてくれる
	Direct3D::pDevice->GetTransform(D3DTS_VIEW, &view);

	//プロジェクション行列
	D3DXMATRIX proj;
	//第一引数で渡したものを第二引数で渡したアドレス元に入れてくれる
	Direct3D::pDevice->GetTransform(D3DTS_PROJECTION, &proj);

	//W、V、Pの順番でかける
	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	//かけた行列を渡す
	pEffect_->SetMatrix("WVP", &matWVP);

	D3DXMATRIX mat = worldMatrix_;

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);

	//拡縮行列を逆行列にする
	D3DXMatrixInverse(&scale, nullptr, &scale);

	//行列掛け合わせ
	mat = scale * rotateZ * rotateX * rotateY;

	//渡す
	pEffect_->SetMatrix("RS", &mat);

	//ライトの情報を扱う変数
	D3DLIGHT9 lightState;

	//ライトの情報を取得
	Direct3D::pDevice->GetLight(0, &lightState);

	//三次元ベクトルを四次元ベクトルにキャストして渡す
	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	//カメラの位置を渡す
	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0, 5, -10));

	//ワールド行列を渡す
	pEffect_->SetMatrix("W", &worldMatrix_);

	//ここから描画開始
	pEffect_->Begin(NULL, 0);

	//普通に表示
	pEffect_->BeginPass(0);

	//厚みのない部分を描画するために両面描画
	Direct3D::pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	Model::Draw(hModel_);

	//表のみ描画（初期に戻す）
	Direct3D::pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	pEffect_->EndPass();

	//シェーダを使った描画終わり
	//以降はDirectXのデフォルト描画に戻る
	pEffect_->End();
}

//開放
void Player::Release()
{
}

//何かに当たった時の処理
void Player::OnCollision(IGameObject * pTarget)
{
	//壁と当たった時
	if (pTarget->GetName() == "Wall")
	{
		//めり込みを直す
		PositionAdjustment(pTarget, RADIUS_);

		//足を止める
		//isMoveFeet_ = false;
	}

	//クリスタルと当たった時
	if (pTarget->GetName() == "Crystal")
	{
		//効果音を再生する
 		Audio::Play("GetCrystal");

		pTarget->KillMe();

		crystalCnt_--;

		//ステージ上のクリスタルを全て取得したら
		if (crystalCnt_ == 0)
		{
			//クリアシーンへ遷移
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_CLEAR);
		}
	}
}

//入力に対する行動を起こす
void Player::Input()
{
	//前進
	if (Input::IsKeyDown(DIK_W) || Input::IsKeyDown(DIK_UP))
	{
		rotateValue_ = 0.0f;

		//コピー
		nextAddMove_ = addMove_;

		//次の向きを示すベクトルになる
		DirectionRotate(rotateValue_, &nextAddMove_);

		//次の方向を決める
		nextDir_ = JudgeDirection(nextAddMove_.x, nextAddMove_.z);
	}
	//後退
	else if (Input::IsKeyDown(DIK_S) || Input::IsKeyDown(DIK_DOWN))
	{
		rotateValue_ = 180.0f;

		//コピー
		nextAddMove_ = addMove_;

		//次の向きを示すベクトルになる
		DirectionRotate(rotateValue_, &nextAddMove_);

		//次の方向を決める
		nextDir_ = JudgeDirection(nextAddMove_.x, nextAddMove_.z);
	}
	//右移動
	else if (Input::IsKeyDown(DIK_D) || Input::IsKeyDown(DIK_RIGHT))
	{
		rotateValue_ = 90.0f;

		//コピー
		nextAddMove_ = addMove_;

		//次の向きを示すベクトルになる
		DirectionRotate(rotateValue_, &nextAddMove_);

		//次の方向を決める
		nextDir_ = JudgeDirection(nextAddMove_.x, nextAddMove_.z);
	}
	//左移動
	else if (Input::IsKeyDown(DIK_A) || Input::IsKeyDown(DIK_LEFT))
	{
		rotateValue_ = -90.0f;

		//コピー
		nextAddMove_ = addMove_;

		//次の向きを示すベクトルになる
		DirectionRotate(rotateValue_, &nextAddMove_);

		//次の方向を決める
		nextDir_ = JudgeDirection(nextAddMove_.x, nextAddMove_.z);
	}

	//加速
	if (Input::IsKeyDown(DIK_SEMICOLON))
	{
		//元気な時は
		if (state_ == FINE)
		{
			//走る状態に遷移
			state_ = RUN;

			//加速する
			toAccelerate_ = true;

			//速度変更
			ChangeSpeed();
		}
	}

	//自殺コマンド
	if (Input::IsKeyDown(DIK_RETURN))
	{
		KillMe();

		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
	}
}


//方向を更新する
void Player::DirectionUpdate()
{
	//最新にする
	currentDir_ = nextDir_;
	nextDir_ = NONE;
}

//速度を変える
void Player::ChangeSpeed()
{
	//足を動かす速度
	float footSpeed = 0.0f;

	//加速するなら
	if (toAccelerate_)
	{
		//速度を上げる
		moveSpeed_ = ACCELE_SPEED_;
		footSpeed = ACCELE_SPEED_FOOT_;
	}
	else
	{
		//速度を元に戻す
		moveSpeed_ = NORMAL_SPEED_;
		footSpeed = NORMAL_SPEED_FOOT_;
	}

	//足の速度も変える
	pFootL->ChangeSpeed(footSpeed);
	pFootR->ChangeSpeed(footSpeed);
}

//スタミナの管理をする
void Player::StaminaManagement()
{
	switch (state_)
	{
		//疲労状態の時
		case TIRED:
			//スタミナを回復する
			stamina_++;
			break;

		//走っている時
		case RUN:
			//徐々にスタミナを減らす
			stamina_--;
			break;

		default: 
			return;
	}
}

//次の方向に曲がれるか確認
void Player::CheckAround()
{
	//プレイヤーの位置と、プレイヤーが立っている床の位置を記録
	float posA = 0;
	float posB = 0;
	
	//隣接するステージパーツのタイプ
	int stageType = TYPE_WALL;

	//自分の立っている位置と、床の位置を記録する
	if (fabs(addMove_.x) > fabs(addMove_.z))
	{
		//移動ベクトルのx成分が0より大きかったら
		if (addMove_.x > 0)
		{
			posA = position_.x;
			posB = round(position_.x);
		}
		else
		{
			posA = round(position_.x);
			posB = position_.x;
		}
	}
	else
	{
		//移動ベクトルのz成分が0より大きかったら
		if (addMove_.z > 0)
		{
			posA = position_.z;
			posB = round(position_.z);
		}
		else
		{
			posA = round(position_.z);
			posB = position_.z;
		}
	}

	//確認するべきは前後か左右か
	if (fabs(nextAddMove_.x) > fabs(nextAddMove_.z))
	{
		//左右どちらか
		if (nextAddMove_.x > 0)
		{
			//自分の右
			stageType = stage_[(int)round(position_.x) + 1][(int)round(position_.z)];
		}
		else
		{
			//自分の左
			stageType = stage_[(int)round(position_.x) - 1][(int)round(position_.z)];
		}
	}
	else
	{
		//前後どちらか
		if (nextAddMove_.z > 0)
		{
			//自分の前
			stageType = stage_[(int)round(position_.x)][(int)round(position_.z) + 1];
		}
		else
		{
			//自分の後ろ
			stageType = stage_[(int)round(position_.x)][(int)round(position_.z) - 1];
		}
	}

	//隣接するステージのタイプが壁以外なら
	if (stageType != TYPE_WALL)
	{
		//進行可能な方向への入力があった（最初のみ）
		isInput_ = true;

		//次の方向に変更する
		Execution(posA, posB, &nextAddMove_);
	}
}

//内積を求め、角度を返す
float Player::CalcAngle(D3DXVECTOR3* dirA, D3DXVECTOR3* dirB)
{
	//正規化
	D3DXVec3Normalize(dirA, dirA);
	D3DXVec3Normalize(dirB, dirB);

	//今と次のベクトルの内積を求める
	float dot = D3DXVec3Dot(dirA, dirB);

	//角度を求める
	float angle = acos(dot);

	return angle;
}

//移動ベクトルを回転させる
void Player::DirectionRotate(float angle, D3DXVECTOR3* vec)
{
	//引数の角度で行列を回転
	D3DXMATRIX m;
	D3DXMatrixRotationY(&m, D3DXToRadian(angle));

	//行列でベクトルを変形
	D3DXVec3TransformCoord(vec, vec, &m);
}

void Player::AddMove()
{
	position_ += addMove_;
}

//引数の値の大きさによって+か-なのかを判断する
int Player::JudgeSign(float vecX)
{
	int sign = 0;

	//引数のxが0より小さければ
	if (vecX < 0.0f)
	{
		//-回転させる
		return sign = -1;
	}

	//+回転させる
	return sign = 1;
}

//次の方向を向く
void Player::Execution(float posA, float posB, D3DXVECTOR3* copyVec)
{
	//引数二つの距離の絶対値を測る
	float dist = fabs(posA - posB);

	//一定距離以内なら
	if (dist <= 0.2)
	{
		//回転させる
		rotate_.y += rotateValue_;

		//移動させる
		addMove_ = *copyVec;

		//次の方向に変更
		DirectionUpdate();
	}
}

//初期位置を教えてもらう
void Player::SetStartPos(float * pos)
{
	position_.x = pos[1];
	position_.y = RADIUS_;
	position_.z = pos[0];

	prevPos_ = position_;
}

//ステージをコピーする
void Player::SetStage(Stage* pStage)
{
	for (float i = 0; i < HEIGHT; i += 1.0)
	{
		for (float j = 0; j < WIDTH; j += 1.0)
		{
			//ステージの情報をもらう
			stage_[(int)j][(int)i] = pStage->GetStage((int)i, (int)j);
		}
	}
}


//今の方向に対応するベクトルを返す
D3DXVECTOR3 Player::GetCurrentDir()
{
	//移動ベクトルが方向ベクトルでもあるので、移動ベクトルを返す
	return addMove_;
}

//渡された引数の方向に対応するベクトルを返す
D3DXVECTOR3 Player::GetCurrentDir(int dir)
{
	//現在の方向を見る
	switch (dir)
	{
		case FORWARD:
			return D3DXVECTOR3(0, 0, 1);

		case BACKWARD:
			return D3DXVECTOR3(0, 0, -1);

		case RIGHT:
			return D3DXVECTOR3(1, 0, 0);

		case LEFT:
			return D3DXVECTOR3(-1, 0, 0);

			//方向がなし（NONE）の時は前を向いている
		default:
			return D3DXVECTOR3(0, 0, 1);
	}
}

//モデル番号を返す
int Player::GetModelHandle()
{
	return hModel_;
}
