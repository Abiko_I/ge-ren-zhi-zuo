#pragma once
#include "Engine/Global.h"

//タイトルシーンを管理するクラス
class TitleScene : public IGameObject
{
private:
	enum
	{
		TITLE,	//タイトル画像
		LOAD	//ロード画像
	};

	int hPict_[2];    //画像番号

	//二枚のうちどちらの画像を表示するか
	int pictNum_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};