#pragma once
#include "Engine/IGameObject.h"

class Camera;
class Player;

//プレイヤーを管理するクラス
class CameraTarget : public IGameObject
{
private:
	const float MOVE_SPEED_;		//自機の移動速度
	const float ROTATE_SPEED_;	//自機の旋回速度

	//ステージとの当たり判定に使用する
	enum
	{
		TYPE_WALL = 0,	//壁
		TYPE_FLOOR,	//床
		TYPE_MAX
	};

	//自分と基準オブジェクトの方向ベクトルを
	//引いたときのベクトルのx、zの符号を示す
	enum
	{
		PLUS,	//+
		MINUS,	//-
	};

	//どれくらい移動するのか
	D3DXVECTOR3 move_;

	Camera* pCamera;

	//自分が向いている方向
	D3DXVECTOR3 selfDir_;

	//基準とするオブジェクトの方向
	D3DXVECTOR3 currentParentDir_;
	D3DXVECTOR3 newParentDir_;

	//自分の基準となるオブジェクト
	IGameObject* pStandardObj_;

	//回転するかどうか
	bool isRotate_;

	//符号
	int sign_;

public:
	//コンストラクタ
	CameraTarget(IGameObject* parent);

	//デストラクタ
	~CameraTarget();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//回転する
	//引数：回転速度にかける値
	//戻り値：なし
	void Rotate(int sign);

	//行列を使用してベクトルを変形する
	//引数：変形に適用する角度、変形した後に格納するベクトル、変形するベクトル
	//戻り値：なし
	void TransformVector(float angle, D3DXVECTOR3* changeDir, D3DXVECTOR3* applyDir);

	//内積を求め、角度を返す
	//引数：ベクトル２つ
	//戻り値：角度（ラジアン）
	float CalcAngle(D3DXVECTOR3* vecA, D3DXVECTOR3* vecB);

	//引数の値の大きさによって+か-なのかを判断する
	//引数：ベクトルのx成分
	//戻り値：引数の値が0より大きいなら1、小さいなら-1
	int JudgeSign(float vecX);

	//回転度合いの数値を調整する
	//引数：なし
	//戻り値：なし
	void RotationAdjustment();

	//基準とするオブジェクトをもらう
	//引数：自分が基準とするオブジェクト
	//戻り値：なし
	void SetStandard(IGameObject* pObj) { pStandardObj_ = pObj; };
};