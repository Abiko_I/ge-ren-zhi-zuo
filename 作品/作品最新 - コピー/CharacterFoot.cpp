#include "CharacterFoot.h"
#include "Engine/Model.h"

//コンストラクタ
CharacterFoot::CharacterFoot(IGameObject * parent, std::string name)
	:IGameObject(parent, name), MOVE_FRONT_LIMIT_(45.0f), MOVE_BACK_LIMIT_(-45.0f),
	hModel_(-1), parentPos_(0.0f, 0.0f, 0.0f), prevParentPos_(0.0f, 0.0f, 0.0f)
{
}

//デストラクタ
CharacterFoot::~CharacterFoot()
{
}

//初期化
void CharacterFoot::Initialize()
{
}

//更新
void CharacterFoot::Update()
{
}

//描画
void CharacterFoot::Draw()
{
}

//開放
void CharacterFoot::Release()
{
}

//モデルを読み込む
void CharacterFoot::LoadModel(std::string modelPath)
{
	//足のモデルをロード
	hModel_ = Model::Load("Data/Models/" + modelPath + ".fbx");
	assert(hModel_ >= 0);
}

//足を動かす
void CharacterFoot::Move()
{
	if (rotate_.x > MOVE_FRONT_LIMIT_)
	{
		//足を限界まで後ろに出す
		sign_ = -1;
	}
	else if (rotate_.x < MOVE_BACK_LIMIT_)
	{
		//足を限界まで前に出す
		sign_ = 1;
	}

	//足を動かす（親クラスを中心として回転する）
	rotate_.x += (moveSpeed_ * (sign_));
}

//足を止める
void CharacterFoot::Stop()
{
	//初期に戻して止まる
	rotate_.x = 0.0f;
}
