#include <time.h>
#include "CharacterAI.h"
#include "NavigationAI.h"
#include "Enemy.h"
#include "Engine/Model.h"

//コンストラクタ
CharacterAI::CharacterAI(IGameObject * parent)
	:AI(parent, "CharacterAI"), enemySpeed_(0.0f), hPlayerModel_(-1),
	situation_(SITUATION_NOT_FOUND), behavior_(BEHAVIOR_WALK),
	request_(true)
{
	for (int i = 0; i < 2; i++)
	{
		record_[i] = {0.0f, 0.0f, false};
		targetPosFloat_[i] = 0.0f;
	}
}

//デストラクタ
CharacterAI::~CharacterAI()
{
}

//初期化
void CharacterAI::Initialize()
{
	//最初は探索する
	isSearch_ = true;

	//乱数を現在時刻で初期化
	srand(time(NULL));

	//格納する値
	float degree[4][4] =
	{
		{  0.0f,  90.0f, -90.0f, 180.0f},	//前　を向いているときに前、右、左、後ろを向くために必要な角度
		{-90.0f,   0.0f, 180.0f,  90.0f},	//右　を向いているときに前、右、左、後ろを向くために必要な角度
		{ 90.0f, 180.0f,   0.0f, -90.0f},	//左　を向いているときに前、右、左、後ろを向くために必要な角度
		{180.0f, -90.0f,  90.0f,   0.0f}	//後ろを向いているときに前、右、左、後ろを向くために必要な角度
	};

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			direction_[i][j] = degree[i][j];
		}
	}
}

//更新
void CharacterAI::Update()
{
	//プレイヤーを発見していないとき
	if (situation_ != SITUATION_FOUND)
	{
		//次の目標地点がリストの最後　または
		//プレイヤーを見失っているとき
		if ((nextIt_ == pos_.end()) || situation_ == SITUATION_LOST_SIGHT)
		{
			//探索するべきなら（きりが良い時）
			if (isSearch_)
			{
				//探索するよう伝える
				((Enemy*)pParent_)->SetCallFlag(true);
			}
			else
			{
				//探索しないよう伝える
				((Enemy*)pParent_)->SetCallFlag(false);
			}
		}
	}

	//移動先の数が0以下になったら
	if (situation_ != SITUATION_LOST_SIGHT && pos_.size() <= 0)
	{
		//目標地点が欲しいので要請する
		request_ = true;
	}

	//状況を見る
	switch (situation_)
	{
		//プレイヤーを発見しているなら
		case SITUATION_FOUND:
			FoundUpdate();
			break;

		//プレイヤーを見失っているなら
		case SITUATION_LOST_SIGHT:
			LostSightUpdate();
			break;
	}

	//前回の記録として格納する
	record_[TIME_LAST] = record_[TIME_CURRENT];

}

//描画
void CharacterAI::Draw()
{
}

//開放
void CharacterAI::Release()
{
}

//プレイヤーを見つけているときの更新
void CharacterAI::FoundUpdate()
{
	//前回も現在も視界にプレイヤーがいないとき
	if (!record_[TIME_LAST].isHitPlayer && !record_[TIME_CURRENT].isHitPlayer)
	{
		//現在と前回で方向が違うかどうかを調べ、違うなら
		if (IsDirDifferent())
		{
			//親（敵）の位置を取得
			D3DXVECTOR3 parentPos = ((Enemy*)pParent_)->GetPosition();

			//現在の方向を求める
			int dir = ((Enemy*)pParent_)->JudgeDirection(record_[TIME_CURRENT].x, record_[TIME_CURRENT].z);

			//今見ている方向にある分岐の数を数える
			int branchCnt = ((Enemy*)pParent_)->CallCountBranch(
				(int)round(parentPos.z), (int)round(parentPos.x), dir, 0);

			//分岐がある（道が二つある）なら
			if (branchCnt >= 2)
			{
				//状況を「プレイヤーを見失った」に変更
				ChangeSituation(SITUATION_LOST_SIGHT);
			}
		}
	}

	//前回視界にプレイヤーがいなかったが、現在は視界にプレイヤーがいるとき
	if (!record_[TIME_LAST].isHitPlayer && record_[TIME_CURRENT].isHitPlayer)
	{
		//プレイヤーの位置を取得
		request_ = true;
	}
	//前回視界にプレイヤーがいたが、現在は視界にプレイヤーがいないとき
	else if (record_[TIME_LAST].isHitPlayer && !record_[TIME_CURRENT].isHitPlayer)
	{
		//プレイヤーの位置を取得
		request_ = true;
	}

	if (request_)
	{
		if (isSearch_)
		{
			//探索するよう伝える
			((Enemy*)pParent_)->SetCallFlag(true);
		}
		else
		{
			//探索しないよう伝える
			((Enemy*)pParent_)->SetCallFlag(false);
		}
	}
}

void CharacterAI::LostSightUpdate()
{
	//次の動作を受け付けているなら
	if (((Enemy*)pParent_)->isAccept())
	{
		//分岐地点の更新
		branchIt_++;
	}

	//イテレーターが分岐点リストの最後を指していないとき
	if (branchIt_ != branchPosList_.end())
	{
		//分岐イテレーターがさしている位置と同じ位置に親（敵）がいるなら
		if ((int)round(parentPos_.z) == branchIt_->z && (int)round(parentPos_.x) == branchIt_->x)
		{
			//見渡す
			behavior_ = BEHAVIOR_ROTATE;
		}
		else
		{
			//進んでもらう
			behavior_ = BEHAVIOR_WALK;
		}
	}
	else
	{
		//状況を「プレイヤーを見つけていない、見つからない」に変更
		ChangeSituation(SITUATION_NOT_FOUND);
	}
}

//状況を変える
void CharacterAI::ChangeSituation(int newSituation)
{
	//引数で渡された新たな状況が、いまの状況と異なるときだけ
	if (situation_ != newSituation)
	{
		//状況を変え、親（敵）の状態も変える
		situation_ = newSituation;
		((Enemy*)pParent_)->ChangeState(situation_);

		//目標地点を渡すように要請を出す
		request_ = true;
	}
}

//目標地点を設定する
void CharacterAI::SetTarget(int z, int x)
{
	//設定
	targetPos_.z_ = z;
	targetPos_.x_ = x;
}

//分岐地点が記録されている一覧をもらう
void CharacterAI::SetBranchPosList(std::vector<BranchPosInfo>* list)
{
	//一旦空にする
	branchPosList_.clear();

	for (auto it = (*list).end() - 1; it != (*list).begin(); it--)
	{
		//地点情報をコピーする
		branchPosList_.push_back(*it);

		//今のイテレーターの１つ前が一覧の最初なら
		if ((it - 1) == (*list).begin())
		{
			//目標地点に設定
			SetTarget((it - 1)->z, (it - 1)->x);
		}
	}

	//一覧の最初の地点を示すようにする
	branchIt_ = branchPosList_.begin();
}

//見渡すための角度を渡す
void CharacterAI::GetRotateForSurvey(int parentDir)
{
	//親（敵）が次に向くべき方向を指定する
	((Enemy*)pParent_)->SetDirection(branchIt_->dir, direction_[parentDir][branchIt_->dir]);
}

//向きを変える
void CharacterAI::ChangeDirection()
{
	//敵と目標地点のxかzの位置を格納する配列
	float pos[2] = { 0, 0 };

	parentPos_ = pParent_->GetPosition();

	//目標地点の方向を見て次の行動を決める
	switch (it_->dir)
	{
		case FORWARD:
			pos[0] = parentPos_.z;
			pos[1] = (*it_).pos.z;
			JudgeNextMove(pos, FORWARD, 0, enemySpeed_);
			break;

		case BACKWARD:
			pos[0] = (*it_).pos.z;
			pos[1] = parentPos_.z;
			JudgeNextMove(pos, BACKWARD, 0, enemySpeed_ * -1);
			break;

		case RIGHT:
			pos[0] = parentPos_.x;
			pos[1] = (*it_).pos.x;
			JudgeNextMove(pos, RIGHT, enemySpeed_, 0);
			break;

		case LEFT:
			pos[0] = (*it_).pos.x;
			pos[1] = parentPos_.x;
			JudgeNextMove(pos, LEFT, enemySpeed_ * -1, 0);
			break;
	}
}

//目標地点に移動するか、目標地点を更新するか
void CharacterAI::JudgeNextMove(float * pos, int dir, float moveX, float moveZ)
{
	float firstVal = pos[0];
	float secondVal = pos[1];

	//今いる場所が目標地点と重なるか、超えたら
	if (firstVal > secondVal)
	{
		std::list<PosInfo>::iterator it = it_;

		//目標地点を更新
		it_++;

		//位置リストの数が２つ以上の時
		if (pos_.size() >= 2)
		{
			//次の位置も更新
			nextIt_++;
		}

		//通過済みの位置は削除
		pos_.erase(it);

		//探索する
		isSearch_ = true;
	}
	//まだ目標地点にたどりついていないなら
	else
	{
		//向きを教える
		((Enemy*)pParent_)->SetDirection(dir, direction_[((Enemy*)pParent_)->GetDirection()][dir]);

		//移動値を設定
		((Enemy*)pParent_)->SetMoveVector(moveX, moveZ);

		//まだ探索しない
		isSearch_ = false;
	}
}

//イテレーターを初期化する
void CharacterAI::InitIterator()
{
	//探索で得た情報の一番最初の場所を取得
	it_ = pos_.begin();
	nextIt_ = pos_.begin();
	nextIt_++;
}

//目標地点を決める
void CharacterAI::DecideTarget(std::vector<CharaPos>* floorList)
{
	//床リストをコピーする
	std::vector<CharaPos> copy;

	parentPos_ = pParent_->GetPosition();

	for (auto it = floorList->begin(); it < floorList->end(); it++)
	{
		//現在位置から今見ている床のzとxの差の絶対値を求める
		int zAbs = abs(int(round(parentPos_.z)) - it->z_);
		int xAbs = abs(int(round(parentPos_.x)) - it->x_);

		//z、xの位置が現在の位置から２マス以上離れている場所を候補にする
		if (zAbs > 2 || xAbs > 2)
		{
			copy.push_back(*it);
		}
	}

	//乱数を生成する
	int ranNum = rand() % copy.size();

	//乱数が指す要素を取り出す
	auto ranIt = copy.at(ranNum);

	//位置を記録
	SetTarget(ranIt.z_, ranIt.x_);
}

//プレイヤーが視覚に入っているか確認する
void CharacterAI::VisualConfirmation()
{
	//プレイヤーまでの距離
	float distPlayer = 9999.0f;

	//レイの情報（プレイヤーに対して、壁に対して）
	RayRecord rayInfoToPlayer = {0.0f, 0.0f, false};
	RayRecord rayInfoToWall = {0.0f, 0.0f, false};

	//壁までの距離
	float distWall = 9999.0f;

	//一番近い壁までの距離
	float minDistWall = 9999.0f;

	//プレイヤーのモデル番号を取得
	hPlayerModel_ = ((Enemy*)pParent_)->GetPlayerModel();

	//壁のモデル番号を格納する
	std::vector<int>* wallModelList_ = ((Enemy*)pParent_)->GetWallModel();

	//プレイヤーに対してレイを撃つ
	RayCast(hPlayerModel_, &distPlayer, &rayInfoToPlayer);

	//全ての壁を調べる
	for (auto it = wallModelList_->begin(); it != wallModelList_->end(); it++)
	{
		//壁に対してレイを撃つ
		RayCast((*it), &distWall, &rayInfoToWall);

		//現在の最短距離より短いなら
		if (distWall < minDistWall)
		{
			//更新
			minDistWall = distWall;

			//現在一番近いレイの情報を記録
			record_[TIME_CURRENT] = rayInfoToWall;
			record_[TIME_CURRENT].isHitPlayer = false;
		}
	}

	//プレイヤーが最も近いなら
	if (distPlayer < minDistWall)
	{
		//状況を「プレイヤーを見つけた」に変更
		ChangeSituation(SITUATION_FOUND);

		//プレイヤーに当たったレイの情報を記録
		record_[TIME_CURRENT] = rayInfoToPlayer;
		record_[TIME_CURRENT].isHitPlayer = true;

		((Enemy*)pParent_)->CallCostDown();
	}
}

//引数で渡された番号のモデルにレイを撃つ
void CharacterAI::RayCast(int handle, float * distTarget, RayRecord* record)
{
	//レイキャストのデータを作成
	RayCastData data;

	//レイの発射位置は敵の位置
	data.start = pParent_->GetPosition();

	//敵の向いている方向にレイも合わせる
	D3DXMATRIX mat;
	D3DXVECTOR3 enemyRotate = pParent_->GetRotate();
	D3DXMatrixRotationY(&mat, D3DXToRadian(enemyRotate.y));

	//レイも回転させる（初期はz方向）
	D3DXVECTOR3 ray = D3DXVECTOR3(0, 0, 1);
	D3DXVec3TransformCoord(&ray, &ray, &mat);
	data.dir = ray;

	//レイを発射する
	Model::RayCast(handle, &data);

	//レイが当たったら
	if (data.hit)
	{
		//現在の対象との距離を測る
		*distTarget = data.dist;

		//レイの情報を記録
		record->x = ray.x;
		record->z = ray.z;
	}
}

//記録した２つのレイの内積を求め、角度を見て向きが違うかどうかを判断する
bool CharacterAI::IsDirDifferent()
{
	D3DXVECTOR3 vecA = D3DXVECTOR3(record_[TIME_CURRENT].x, 0.0f, record_[TIME_CURRENT].z);
	D3DXVECTOR3 vecB = D3DXVECTOR3(record_[TIME_LAST].x, 0.0f, record_[TIME_LAST].z);

	float dot = D3DXVec3Dot(&vecA, &vecB);

	float angle = D3DXToDegree(acos(dot));

	//２つのベクトルの角度が20度以下のとき
	//（同じ向きでこれ以上の開きがないと考える）
	if (fabs(angle) <= 20)
	{
		//方向が同じ
		return false;
	}
	
	//方向が違う
	return true;
}

//渡された目標地点のx、z位置から、親（敵）がいるマスと同じ位置かどうかを判断する
bool CharacterAI::IsTargetSamePos(float z, float x)
{
	//親（敵）の位置と引数の目標地点のx、z成分を四捨五入して両方の成分が同じなら
	if (round(parentPos_.z) == round(z) && round(parentPos_.x) == round(x))
	{
		targetPosFloat_[0] = z;
		targetPosFloat_[1] = x;

		//同じマス上にいる
		return true;
	}

	//違うマス上にいる
	return false;
}

//親（敵）の位置と目標地点が同じときの動作
void CharacterAI::TargetSamePosMovement()
{
	//目標地点と親（敵）の位置の差を求める
	float diffZ = targetPosFloat_[0] - parentPos_.z;
	float diffX = targetPosFloat_[1] - parentPos_.x;

	//親（敵）から見てどの方向に目標地点があるのかを求める
	int dir = ((Enemy*)pParent_)->JudgeDirection(diffZ, diffX);

	//向きを教える
	((Enemy*)pParent_)->SetDirection(dir, direction_[((Enemy*)pParent_)->GetDirection()][dir]);

	//目標地点の方向を見て移動値を設定する
	switch (dir)
	{
		case FORWARD:
			((Enemy*)pParent_)->SetMoveVector(0, enemySpeed_);
			break;

		case BACKWARD:
			((Enemy*)pParent_)->SetMoveVector(0, enemySpeed_ * -1);
			break;

		case RIGHT:
			((Enemy*)pParent_)->SetMoveVector(enemySpeed_, 0);
			break;

		case LEFT:
			((Enemy*)pParent_)->SetMoveVector(enemySpeed_ * -1, 0);
			break;
	}
}
