#include "NavigationAI.h"
#include "Stage.h"
#include "Enemy.h"
#include "Player.h"

//コンストラクタ
NavigationAI::NavigationAI(IGameObject * parent)
	:AI(parent, "NavigationAI"), FLOOR_COST(98), PLAYER_PASSED_COST(1), enemyPos_(0.0f, 0.0f, 0.0f)
{
}

//デストラクタ
NavigationAI::~NavigationAI()
{
}

//初期化
void NavigationAI::Initialize()
{
	//自分が持つステージの初期化
	for (int i = 0; i < HEIGHT; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			stage_[i][j].x_ = i;
			stage_[i][j].z_ = j;
			stage_[i][j].cost_ = 99;
			stage_[i][j].type_ = -1;
		}
	}

	//自分の向いている方向と、世界から見たときの自分が向いている方向
	//から、自分がいる位置から前後左右の位置を特定するための値
	int value[4][6] =
	{
		{1,  0,  1,  1,  1, -1},	//世界から見て前を向いている
		{0,  1, -1,  1,  1,  1},	//			　右を向いている
		{0, -1,  1, -1, -1, -1},	//			　左を向いている
		{-1, 0, -1, -1, -1,  1}		//			　後ろを向いている
	};

	//列
	int row = 0;

	//それぞれの方向の調べる位置を格納（それぞれ前、右斜め前、左斜め前のz成分、x成分の順）
	for (int i = 0; i < 4; i++)
	{
		for (int j = 1; j < 7; j++)
		{
			//jが奇数のとき
			if ((j % 2) != 0)
			{
				//zに代入
				depDir_[i][row].addZ = value[i][j - 1];
			}
			else
			{
				//xに代入
				depDir_[i][row].addX = value[i][j - 1];
				row++;
			}

			//depDir_の行分以上になったら
			if (row >= 3)
			{
				//行を初期位置に戻す
				row = 0;
			}
		}
	}
}

//更新
void NavigationAI::Update()
{
	//----------------プレイヤーの通った位置のコストを下げる----------------
	D3DXVECTOR3 playerPos = pPlayer_->GetPosition();
	int playerZ = (int)round(playerPos.z);
	int playerX = (int)round(playerPos.x);

	//親（敵）が「追跡」状態なら
	if (((Enemy*)pParent_)->GetState() == Enemy::CHASE)
	{
		//プレイヤーのいる位置のコストを変更
		stage_[playerZ][playerX].cost_ = PLAYER_PASSED_COST;

		//一覧に追加
		PushAddress(&stage_[playerZ][playerX], &prevPlayerPos_);
	}
	//「巡回」、「探索」状態なら
	else
	{
		for (auto it = prevPlayerPos_.begin(); it != prevPlayerPos_.end();)
		{
			//コストを戻す
			(*it)->cost_ = FLOOR_COST;

			//今イテレーターが指している位置を一覧から削除
			it = prevPlayerPos_.erase(it);
		}
	}

	//------------親（敵）が通った、プレイヤーまでの直線の位置のコストを戻していく---------
	enemyPos_ = pParent_->GetPosition();

	auto it = costChangeList_.begin();
	while (it != costChangeList_.end())
	{
		//一覧の中に現在の親（敵）と同じ位置のものがあれば
		if ((*it)->z_ == (int)round(enemyPos_.x) && (*it)->x_ == (int)round(enemyPos_.z))
		{
			//コストを戻す
			(*it)->cost_ = FLOOR_COST;

			//一覧から削除
			it = costChangeList_.erase(it);
		}
		else
		{
			it++;
		}
	}
}

//描画
void NavigationAI::Draw()
{
}

//開放
void NavigationAI::Release()
{
}

//ステージ情報をもらい、コピーする
void NavigationAI::SetStage(Stage * pStage, int num)
{
	pStage_ = pStage;

	for (float i = 0; i < HEIGHT; i += 1.0)
	{
		for (float j = 0; j < WIDTH; j += 1.0)
		{
			//ステージの情報をもらう
			stage_[(int)i][(int)j].type_ = pStage_->GetStage((int)i, (int)j);

			if(stage_[(int)i][(int)j].type_ != TYPE_WALL)
			{
				//位置を保存
				CharaPos pos = {i, j};
				floorList_.push_back(pos);

				//コストをつける
				stage_[(int)i][(int)j].cost_ = FLOOR_COST;
			}
		}
	}

	//xとzの初期位置を教えてもらう
	float pos[2] = { 0, 0 };
	pos[0] = pStage_->GetEnemyStart(num, 0);
	pos[1] = pStage_->GetEnemyStart(num, 1);

	//敵本体に教える
	((Enemy*)pParent_)->SetStartPos(pos[0], pos[1]);

}

//プレイヤーを探す関数達を呼び出す
void NavigationAI::SearchTargetPos(CharaPos targetPos)
{
	enemyPos_ = pParent_->GetPosition();

	//探索の初期位置の情報
	Info pos = { (int)round(enemyPos_.z), (int)round(enemyPos_.x), 0, 0, 0, 0, 0, NONE };

	//リストを空にする
	openList_.clear();
	closeList_.clear();
	pos_->clear();

	//終点に着いていないのなら
	if (!(pos.z_ == targetPos.z_ && pos.x_ == targetPos.x_))
	{
		//初期値を調査中リストに追加
		openList_.push_back(pos);
	}

	//目標地点までの最短距離を求める
	Search(pos, targetPos, &openList_, &closeList_);
}

//プレイヤーのモデル番号を渡す
int NavigationAI::GetPlayerModel()
{
	return pPlayer_->GetModelHandle();
}

//壁のモデル番号リストを渡す
std::vector<int>* NavigationAI::GetWallModel()
{
	return pStage_->GetModelList();
}

//プレイヤーの位置を渡す
D3DXVECTOR3 NavigationAI::GetPlayerPos()
{
	return pPlayer_->GetPosition();
}

//A*アルゴリズム法でプレイヤーの位置までの最短経路を調べる
void NavigationAI::Search(Info currentPos, CharaPos playerPos, std::vector<Info>* openList, std::vector<Info>* closeList)
{
	//上下左右の位置を記録する
	Info nextPos[4];

	for (int i = 0; i < 4; i++)
	{
		//有り得ない値で初期化
		nextPos[i].z_ = -1;
		nextPos[i].x_ = -1;
		nextPos[i].dist_ = 9999;
		nextPos[i].est_ = 9999;
		nextPos[i].realDist_ = 9999;
		nextPos[i].prevZ_ = -1;
		nextPos[i].prevX_ = -1;
		nextPos[i].dir_ = NONE;
	}

	//再帰処理をする時に引数として渡す
	Info retPos = { 0, 0, 0, 0, 9999, 0, 0, NONE };

	//上下左右を探索
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			//上下
			if (j == 0)
			{
				nextPos[i].z_ = currentPos.z_ + searchPos_[i][j];
			}
			//左右
			else
			{
				nextPos[i].x_ = currentPos.x_ + searchPos_[i][j];
			}
		}

		//カウント数を見て方向を決定
		switch (i)
		{
			case 0:
				nextPos[i].dir_ = BACKWARD;
				break;

			case 1:
				nextPos[i].dir_ = LEFT;
				break;

			case 2:
				nextPos[i].dir_ = RIGHT;
				break;

			case 3:
				nextPos[i].dir_ = FORWARD;
				break;
		}

		//調査中リストにあるかどうか
		bool isExistOpen = false;

		//調査済みリストにあるかどうか
		bool isExistClose = false;

		//始点かどうか
		bool isStartPos = false;

		//計算中リストに、今見ている位置と同じ情報があるか調べる
		isExistOpen = IsExist(openList, nextPos[i]);

		//探索済みリストに、今見ている位置と同じ情報があるか調べる
		isExistClose = IsExist(closeList, nextPos[i]);

		//始点であるなら
		if (nextPos[i].z_ == (int)round(position_.z) && nextPos[i].x_ == (int)round(position_.x))
		{
			isStartPos = true;
		}

		//壁でなく、調査中リストになく、調査済みリストになく、
		//始点でない位置の時は
		if (stage_[nextPos[i].z_][nextPos[i].x_].type_ != TYPE_WALL &&
			!isExistOpen && !isExistClose && !isStartPos)
		{
			//実歩数を計算
			nextPos[i].dist_ = currentPos.dist_ + stage_[nextPos[i].z_][nextPos[i].x_].cost_;

			//目的地までの推定値を計算
			nextPos[i].est_ = abs(nextPos[i].z_ - playerPos.z_) + abs(nextPos[i].x_ - playerPos.x_);

			//目的地までの推定歩数を計算
			nextPos[i].realDist_ = nextPos[i].dist_ + nextPos[i].est_;

			//前の位置を保存
			nextPos[i].prevZ_ = currentPos.z_;
			nextPos[i].prevX_ = currentPos.x_;

			//調査中リストに追加
			openList->push_back(nextPos[i]);
		}
	}

	//初回
	if (closeList->size() <= 0)
	{
		//上下左右を探索したので、調査終了
		closeList->push_back(currentPos);
	}

	for (auto it = closeList->begin(); it != closeList->end(); it++)
	{
		//調査済みリストに、現在の位置と同じものがなければ
		if (!((it->z_ == currentPos.z_) && (it->x_ == currentPos.x_)))
		{
			//上下左右を探索したので、調査終了
			closeList->push_back(currentPos);
			break;
		}
	}

	//計算中リストから同じ位置を探して
	for (auto it = openList->begin(); it < openList->end(); it++)
	{
		if ((it->z_ == currentPos.z_) && (it->x_ == currentPos.x_))
		{
			//調査中リストから削除
			openList->erase(it);
			break;
		}
	}

	//計算中リストの最小推定実歩数のものを選択
	for (auto it = openList->begin(); it < openList->end(); it++)
	{
		//現在見ている情報の推定実歩数が
		//引数として使おうとしている情報の中の推定歩数より小さいなら
		if (retPos.realDist_ > it->realDist_)
		{
			//引数として使う情報を更新
			retPos = *it;
		}
	}

	//終点に着いたのなら
	if ((retPos.z_ == playerPos.z_) && (retPos.x_ == playerPos.x_))
	{
		//終点から逆探索して通る道に印をつける
		Mark(retPos, closeList);

		//関数を終了
		return;
	}
	//まだ終点ではないのなら
	else
	{
		//再帰呼び出し
		Search(retPos, playerPos, openList, closeList);
	}
}

//次の位置がリストに存在するか確認する
bool NavigationAI::IsExist(std::vector<Info>* list, Info nextPos)
{
	//引数のリストに、今見ている位置と同じ情報があるなら
	for (auto it = list->begin(); it < list->end(); it++)
	{
		if ((it->z_ == nextPos.z_) && (it->x_ == nextPos.x_))
		{
			//引数のリストにある
			return true;
		}
	}

	//引数のリストにない
	return false;
}

//通る道を記録させ、調査済みリストから通るべき位置を探す
void NavigationAI::Mark(Info currentPos, std::vector<Info>* closeList)
{
	//その位置に印をつける
	AddMark(currentPos, enemyPos_.y);

	//再帰させるか
	bool isCallFunc = false;

	//前の位置が始点ではない時
	if (!((currentPos.prevZ_ == (int)round(enemyPos_.z)) && (currentPos.prevX_ == (int)round(enemyPos_.x))))
	{
		//再帰させる
		isCallFunc = true;
	}

	//再帰させるなら
	if (isCallFunc)
	{
		//計算済みリストから、今の位置の前の位置と同じ位置を探して
		for (auto it = closeList->begin(); it < closeList->end(); it++)
		{
			if ((it->z_ == currentPos.prevZ_) && (it->x_ == currentPos.prevX_))
			{
				//再帰で渡す引数を、今の位置の前の位置に更新
				currentPos = *it;
			}
		}

		//再帰呼び出し
		Mark(currentPos, closeList);
	}
}

//通る位置を記録する
void NavigationAI::AddMark(Info pos, float y)
{
	//要素を格納
	PosInfo info;
	info.dir = pos.dir_;
	info.pos = D3DXVECTOR3(pos.x_, y, pos.z_);

	//先頭に追加
	pos_->push_front(info);
}

//分岐地点一覧の中身を全削除する
void NavigationAI::ClearBranchList()
{
	branchPosList_.clear();
}

//分岐しているかどうかを見る
int NavigationAI::CountBranch(int posZ, int posX, int enemyDir, int branchCnt)
{
	int type[3];

	//壁かどうかの結果
	bool isWall[3] = { false, false, false };

	//先に、引数で渡された位置の前にあるモデルタイプを取得
	type[FORWARD] = ((Enemy*)pParent_)->GetStage(posZ + depDir_[enemyDir][FORWARD].addZ, posX + depDir_[enemyDir][FORWARD].addX);

	//前に壁があるなら
	if (type[FORWARD] == TYPE_WALL)
	{
		//行き止まり
		BranchPosInfo deadEnd = { posZ , posX, FORWARD };
		branchPosList_.push_back(deadEnd);

		//分岐なし
		return 0;
	}
	//前に壁がないなら
	else
	{
		//再起（最初に前だけ調べていく）
		branchCnt = CountBranch(posZ + depDir_[enemyDir][FORWARD].addZ, posX + depDir_[enemyDir][FORWARD].addX, enemyDir, branchCnt);
	}

	for (int i = 1; i < 3; i++)
	{
		//引数で渡された位置に隣接した左、右のモデルタイプを取得
		type[i] = ((Enemy*)pParent_)->GetStage(posZ + depDir_[enemyDir][i].addZ, posX + depDir_[enemyDir][i].addX);

		//モデルタイプが壁じゃないなら
		if (type[i] != TYPE_WALL)
		{
			//分岐だから、数える
			branchCnt++;

			//「分岐がある位置」として保存する
			BranchPosInfo info = { -1, -1, NONE };

			//親（敵）の向きを見て格納する情報を決める
			switch (enemyDir)
			{
				case FORWARD:
				case BACKWARD:
					info = { posZ + depDir_[enemyDir][i].addZ, posX, NONE };
					break;

				case RIGHT:
				case LEFT:
					info = { posZ, posX + depDir_[enemyDir][i].addX, NONE };
					break;
			}

			D3DXMATRIX mat;

			//基準の方向ベクトル
			D3DXVECTOR3 dir = D3DXVECTOR3(0, 0, 1);

			//親（敵）の回転度をもらう
			D3DXVECTOR3 parentRotate = pParent_->GetRotate();

			float rotateY = 0.0f;

			//添え字から方向を判断する
			switch (i)
			{
				case RIGHT:
					rotateY = 90.0f;

					break;

				case LEFT:
					rotateY = -90.0f;
					break;
			}

			//方向ベクトルを、分岐している方向に合わせる
			D3DXMatrixRotationY(&mat, D3DXToRadian(parentRotate.y + rotateY));
			D3DXVec3TransformCoord(&dir, &dir, &mat);

			//方向ベクトルから、方向を判断し、格納する
			info.dir = ((Enemy*)pParent_)->JudgeDirection(dir.x, dir.z);

			//情報を保存
			branchPosList_.push_back(info);
		}
	}

	//分岐の数を返す
	return branchCnt;
}

//敵がプレイヤーを発見したとき、プレイヤーが通った道のコストを下げる
void NavigationAI::CostDown()
{
	float x = 0.0f, z = 0.0f;
	float xMin = 0.0f, xMax = 0.0f, zMin = 0.0f, zMax = 0.0f;

	D3DXVECTOR3 playerPos = pPlayer_->GetPosition();

	//プレイヤーと敵のxの位置が同じなら
	if (fabs(round(playerPos.x)) == fabs(round(enemyPos_.x)))
	{
		x = round(enemyPos_.x);

		//世界から見て、プレイヤーが敵より前にいるとき
		if (playerPos.z > enemyPos_.z)
		{
			zMax = round(playerPos.z);
			zMin = round(enemyPos_.z + 1.0f);
		}
		//世界から見て、プレイヤーが敵より後ろにいるとき
		else
		{
			zMax = round(enemyPos_.z - 1.0f);
			zMin = round(playerPos.z);
		}

		//x固定
		for (float i = 0.0f; zMin + i < zMax + 1; i += 1.0f)
		{
			int xPos = (int)x;
			int zPos = (int)(zMin + i);

			//位置のアドレスを追加
			PushAddress(&stage_[zPos][xPos], &costChangeList_);
		}
	}
	else
	{
		z = round(playerPos.z);

		//世界から見て、プレイヤーが敵より右にいるとき
		if (playerPos.x > enemyPos_.x)
		{
			xMax = round(playerPos.x);
			xMin = round(enemyPos_.x + 1.0f);
		}
		//世界から見て、プレイヤーが敵より左にいるとき
		else
		{
			xMax = round(enemyPos_.x - 1.0f);
			xMin = round(playerPos.x);
		}

		//x固定
		for (float i = 0.0f; xMin + i < xMax + 1; i += 1.0f)
		{
			int xPos = (int)(xMin + i);
			int zPos = (int)z;

			//位置のアドレスを追加
			PushAddress(&stage_[zPos][xPos], &costChangeList_);
		}
	}

	for (auto it = costChangeList_.begin(); it != costChangeList_.end(); it++)
	{
		//コスト変更
		(*it)->cost_ = PLAYER_PASSED_COST;
	}
}

//引数で渡されたステージ位置のアドレスを追加する
void NavigationAI::PushAddress(StageInfo* address, std::vector<StageInfo*>* posList)
{
	for (auto it = (*posList).begin(); it != (*posList).end(); it++)
	{
		//一覧の中に同じ位置を示すイテレーターがあるときは
		if ((*it)->x_ == address->x_ && (*it)->z_ == address->z_)
		{
			//追加せず終了
			return;
		}
	}

	//追加する
	(*posList).push_back(address);
}