#pragma once
#include "Engine/IGameObject.h"

//床を管理するクラス
class Floor : public IGameObject
{
private:

	//モデル番号
	int hModel_;

public:
	//コンストラクタ
	Floor(IGameObject* parent);

	//デストラクタ
	~Floor();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};