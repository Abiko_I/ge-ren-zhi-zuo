//グローバル変数（アプリ側から渡される）
float4x4 WVP;			//ワールド・ビュー・プロジェクションを掛けた変数
float4x4 RS;			//回転、拡縮行列の逆行列
float4x4 W;				//ワールド行列
float4 LIGHT_DIR;		//ライト
float4 DIFFUSE_COLOR;	//色
float4 CAMERA_POS;		//視線（カメラの位置）
texture TEXTURE;		//テクスチャ
float4 AMBIENT_COLOR;	//環境光
float4 SPECULAR_COLOR;	//鏡面色
float SPECULAR_POWER;	//鏡面の強さ
float SCROLL;			//増加する値を受け取る
bool IS_PASTED;			//テクスチャが貼られているかどうか

//サンプラー
//テクスチャを使用する時必須
sampler texSampler = sampler_state
{
	//<>の間に入れられた変数をテクスチャとして使う（今回はグローバル）
	Texture = <TEXTURE>;

	//テクスチャのギザギザしたところを滑らかにする（アンチエイリアス）
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;

	//真ん中に貼って、テクスチャの大きさが足りない時にUなら横端を、Vなら縦端の部分を引き延ばす
	//AddressU = Clamp;
	//AddressV = Clamp;

	//デフォルトと同じ、普通の貼り方
	//AddressU = Wrap;
	//AddressV = Wrap;

	//左右反転して並べる
	//Uは横に、Vは上下反転した状態で
	AddressU = Mirror;
	AddressV = Mirror;
};

//構造体作成
//頂点シェーダの出力でピクセルシェーダの入力
struct VS_OUT
{
	float4 pos		:SV_POSITION;	//位置
	float4 normal	:NORMAL;		//法線

	//視線(セマンティクスは本来いらないが、書かなきゃいけないので、テクスチャコーディネートの略を書いとく)
	float4 eye		:TEXCOORD1;
	float2 uv		:TEXCOORD0;		//UV座標（uとvの二つの情報だからfloat2）
};

//頂点シェーダ
//頂点ごとのMayaのローカル座標をスクリーン座標に変える
//引数：位置、法線、UV座標
//今回は、返す構造体の中身で欲しいものを掲載しているので、戻り値のセマンティクスは無し
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD0)
{
	//WVP（ワールド・ビュー・プロジェクションを掛けた変数)を掛ける
	VS_OUT outData;

	//ベクトルに行列を掛ける（トランスフォームコードと同じ）、頂点シェーダでほぼ必ずやる
	outData.pos = mul(pos, WVP);

	//ベクトルに行列を掛ける
	normal = mul(normal, RS);

	//法線を正規化
	normal = normalize(normal);

	outData.normal = normal;

	//引数のposに、グローバルのワールド行列を掛ける
	//いろいろ変化した結果の行列になる
	float4 worldPos = mul(pos, W);

	//視線ベクトルを求めて、正規化する
	outData.eye = normalize(CAMERA_POS - worldPos);

	//引数で受け取ったuv座標を代入
	outData.uv = uv;

	return outData;
}

//ピクセルシェーダ（ピクセルの数だけ呼ばれる）
//頂点シェーダの戻り値と同じ
float4 PS(VS_OUT inData) : COLOR
{
	//頂点シェーダで求めた、一頂点からの複数の法線の間をとって、一頂点一法線にするが、
	//その頂点の間にある面上の法線たちが短くなるので、正規化する
	inData.normal = normalize(inData.normal);

	//視線ベクトルも正規化
	inData.eye = normalize(inData.eye);

	//自作のライトのベクトルに、グローバル変数を代入（向きのみ）
	float4 lightDir = LIGHT_DIR;

	//法線を正規化
	lightDir = normalize(lightDir);

	//拡散反射光（内積を求める）
	float4 diffuse = dot(inData.normal, -lightDir);

	//透明度が0だと困るから、1にして不透明にする
	diffuse.a = 1;

	//テクスチャが貼られていると伝達されたら
	if (IS_PASTED)
	{
		//指定した位置の色を取得する
		diffuse *= tex2D(texSampler, inData.uv);
	}
	//テクスチャが貼られていないと伝達されたら
	else
	{
		//グローバル変数を掛けて代入
		diffuse *= DIFFUSE_COLOR;
	}


	//環境光
	float4 ambient = AMBIENT_COLOR;

	ambient = diffuse;
	ambient.a = 0.5f;

	//鏡面反射光
	float4 R = reflect(lightDir, inData.normal);		//正反射ベクトル
	float4 specular = pow(dot(R, inData.eye), SPECULAR_POWER) * 2 * SPECULAR_COLOR;

	//頂点シェーダで色を求めているので、そのまま足して出力
	return ambient + diffuse + specular;
}

//VS、PSがどっちがどっちかわからないので、登録する
technique
{
	//pass　→　セット的な意味
	//書いた順に番号が0から振られる（↓は）
	pass
	{
		//VS()をVertexShaderとして登録
		VertexShader = compile vs_3_0 VS();
		//PS()をPixelShaderとして登録
		PixelShader = compile ps_3_0 PS();
	}
}