#include "PlayScene.h"
#include "Stage.h"
#include "Enemy.h"
#include "Player.h"
#include "CameraTarget.h"
#include "Arrow.h"
#include "CrystalUI.h"
#include "Engine/Audio.h"
#include "Water.h"
#include "Engine/Direct3D.h"
#include "Engine/Image.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), pEnemyEffect_(nullptr)
{
	sounds_[NORMAL] = "BGM_Normal";
	sounds_[CHASE] = "BGM_Chase";

	pEnemyList_.clear();
	pArrowList_.clear();
}

PlayScene::~PlayScene()
{
	//ポインタだから
	SAFE_RELEASE(pEnemyEffect_);
}

//初期化
void PlayScene::Initialize()
{
	//HLSLファイルからエフェクト（シェーダ）を生成
	//エラーが出た結果を入れる
	LPD3DXBUFFER err = 0;

	//エラーが出たとき
	if (FAILED(
		D3DXCreateEffectFromFile(Direct3D::pDevice, "ColorChangeShader.hlsl",
			NULL, NULL, D3DXSHADER_DEBUG, NULL, &pEnemyEffect_, &err)))
	{
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダーエラー", MB_OK);
	}

	assert(pEnemyEffect_ != nullptr);

	//プレイヤーを生成
	pPlayer_ = CreateGameObject<Player>(this);

	//ステージを生成
	pStage_ = CreateGameObject<Stage>(this);

	pPlayer_->SetStartPos(pStage_->GetStartPos());
	pPlayer_->SetCrystalCnt(pStage_->GetCrystalCnt());
	pPlayer_->SetStage(pStage_);

	//カメラのターゲットを生成
	pCameraTarget_ = CreateGameObject<CameraTarget>(this);
	pCameraTarget_->SetStandard(pPlayer_);

	for (int i = 0; i < pStage_->GetEnemyCount(); i++)
	{
		//敵を生成
		pEnemyList_.push_back(CreateGameObject<Enemy>(this));

		//矢印を生成
		pArrowList_.push_back(CreateGameObject<Arrow>(this));

		//ステージのポインタを敵に渡す
		pEnemyList_.at(i)->SetStage(pStage_, i);

		//プレイヤーのポインタを敵に渡す
		pEnemyList_.at(i)->SetPlayer(pPlayer_);

		pArrowList_.at(i)->SetCenterObj(pPlayer_);
		pArrowList_.at(i)->SetTargetObj(pEnemyList_.at(i));
	}

	//クリスタルのUIを生成
	pCrystalUI_ = CreateGameObject<CrystalUI>(this);
	pCrystalUI_->SetCrystalMax(pStage_->GetCrystalCnt());

	pStage_->SetCrystalUI(pCrystalUI_);

	//最初は通常のBGM
	currentSound_ = sounds_[NORMAL];

	//次に流すのが追いかけられている時のBGM
	nextSound_ = sounds_[CHASE];

	//BGMを再生
	Audio::Play(currentSound_);

	//水を生成
	pWater_ = CreateGameObject<Water>(this);
	D3DXVECTOR3 pPlayerPos = pPlayer_->GetPosition();
	pPlayerPos.y = -2.0f;
	pWater_->SetPosition(pPlayerPos);
}

//更新
void PlayScene::Update()
{
	//追跡状態の敵の数
	int chaseCnt = 0;

	for (auto it = pEnemyList_.begin(); it != pEnemyList_.end(); it++)
	{
		//敵がプレイヤーを追いかけている時
		if ((*it)->GetState() != Enemy::PATROL)
		{
			//追いかけている時の音を再生
			SwitchSounds(sounds_[CHASE]);

			//現在の音にする
			currentSound_ = sounds_[CHASE];

			//追跡状態の敵を数える
			chaseCnt++;
		}
	}

	//追跡状態の敵が１人もいないとき
	if (chaseCnt == 0)
	{
		//通常時の音を再生
		SwitchSounds(sounds_[NORMAL]);

		//現在の音にする
		currentSound_ = sounds_[NORMAL];
	}
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
	//BGMを止める
	Audio::Stop(currentSound_);
}

//引数で渡されたBGMを流し、現在流れているBGMを停止する
void PlayScene::SwitchSounds(char * cueName)
{
	//渡された音と、現在流している音が違う時
	if (cueName != currentSound_)
	{
		//現在の音を停止し、
		Audio::Stop(currentSound_);

		//渡された音を再生する
		Audio::Play(cueName);
	}
}
