#include "TitleScene.h"
#include "Engine/Image.h"

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene")/*, hPict_(-1)*/, pictNum_(TITLE)
{
	hPict_[0] = -1;
	hPict_[1] = -1;
}

//初期化
void TitleScene::Initialize()
{
	//画像データのロード
	hPict_[TITLE] = Image::Load("Data/Picts/Title.jpg");
	assert(hPict_[TITLE] >= 0);
	hPict_[LOAD] = Image::Load("Data/Picts/Load.jpg");
	assert(hPict_[LOAD] >= 0);
}

//更新
void TitleScene::Update()
{
	//エンターキーが押されていたら
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//ロード画面を表示
		pictNum_ = LOAD;

		//シーンを変える
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void TitleScene::Draw()
{
	Image::SetMatrix(hPict_[pictNum_], worldMatrix_);
	Image::Draw(hPict_[pictNum_]);
}

//開放
void TitleScene::Release()
{
}