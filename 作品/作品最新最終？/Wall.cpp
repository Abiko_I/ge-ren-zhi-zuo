#include "Wall.h"
#include "Stage.h"
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"

//コンストラクタ
Wall::Wall(IGameObject * parent)
    :IGameObject(parent, "Wall"), hModel_(-1), collision_(nullptr)
{
}

//デストラクタ
Wall::~Wall()
{
}

//初期化
void Wall::Initialize()
{
	//モデルのロード
	hModel_ = Model::Load("Data/Models/Wall.fbx");
	assert(hModel_ >= 0);

	//モデル番号を渡して格納してもらう
	((Stage*)pParent_)->StoreModelHandle(hModel_);

	collision_ = new BoxCollider(
		D3DXVECTOR3(position_.x, position_.y, position_.z),
		D3DXVECTOR3(1.0f, 1.0f, 1.0f));

	AddCollider(collision_);
}

//更新
void Wall::Update()
{
}

//描画
void Wall::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Wall::Release()
{
}

//コライダーリストを渡す
Collider* Wall::GetCollider()
{
	return collision_;
}
