#include <time.h>
#include "Stage.h"
#include "Floor.h"
#include "Wall.h"
#include "Crystal.h"
#include "CrystalUI.h"
#include "Engine/Model.h"
#include "Engine/CsvReader.h"
#include "Engine/Camera.h"
#include "Engine/Direct3D.h"

//コンストラクタ
Stage::Stage(IGameObject * parent) : IGameObject(parent, "Stage"),
	ENEMY_MAX_(3), FOUR_DIRECTIONS_(4), THREE_DIRECTIONS_(3), crystalCount_(0), pEffect_(nullptr)
{
	playerStartPos_[0] = -1;
	playerStartPos_[1] = -1;

	wallModelList_.clear();
	posList_.clear();

	//棒倒し法で棒がランダムで倒れる方向を格納
	fallDirection[0] = {  0, -1 };	//左
	fallDirection[1] = {  0,  1 };	//右
	fallDirection[2] = {  1,  0 };	//後ろ
	fallDirection[3] = { -1,  0 };	//前
}

//デストラクタ
Stage::~Stage()
{
	//ポインタだから
	SAFE_RELEASE(pEffect_);
}

//初期化
void Stage::Initialize()
{
	//HLSLファイルからエフェクト（シェーダ）を生成
	//エラーが出た結果を入れる
	LPD3DXBUFFER err = 0;

	//エラーが出たとき
	if (FAILED(
		D3DXCreateEffectFromFile(Direct3D::pDevice, "CommonShader.hlsl",
			NULL, NULL, D3DXSHADER_DEBUG, NULL, &pEffect_, &err)))
	{
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダーエラー", MB_OK);
	}

	assert(pEffect_ != nullptr);

	//乱数を現在時刻で初期化
	srand(time(NULL));

	//ステージの元を生成
	for (int i = 0; i < HEIGHT; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			//外周と、一つ飛ばしの位置に壁を置く
			if ((i == 0) ||						//一番左の列
				(i == (HEIGHT - 1)) ||			//一番右の行
				(j == 0) ||						//一番上の行
				(j == (WIDTH - 1)) ||			//一番下の行
				((i % 2) == 0 && (j % 2) == 0)	//外周以外の位置の時は１マス置きに
				)
			{
				stage_[i][j] = TYPE_WALL;
			}
			//それ以外の位置には床を置く
			else
			{
				stage_[i][j] = TYPE_FLOOR;
			}
		}
	}

	CreateStage();

	for (float z = 0.0f; z < HEIGHT; z += 1.0f)
	{
		for (float x = 0.0f; x < WIDTH; x += 1.0f)
		{
			float xPos = x;
			float zPos = (HEIGHT - 1) - z;
			float yPos = 0.0f;

			//ステージを構成する部品を生成
			CreateParts(stage_[(int)zPos][(int)x], x, z, xPos, yPos, zPos);
		}
	}
}

//更新
void Stage::Update()
{
	//現在のクリスタルの数を渡す
	pCrystalUI_->SetCrystalNum(crystalCount_);
}

//描画
void Stage::Draw()
{
	
}

//開放
void Stage::Release()
{
}

//ステージを生成する
void Stage::CreateStage()
{
	for (int i = 1; i < HEIGHT - 1; i++)
	{
		for(int j = 1; j < WIDTH - 1; j++)
		{
			//今見ている位置が壁を配置した場所なら
			if ((i % 2 == 0) && (j % 2 == 0))
			{
				//乱数の最大値
				int ranBase = 0;

				//ステージ全体から見て３行目の時は
				if (i == 2)
				{
					//外周以外の壁としては１行目なので上下左右を調べる
					ranBase = FOUR_DIRECTIONS_;
				}
				else
				{
					//下左右を調べる
					ranBase = THREE_DIRECTIONS_;
				}

				//迷路作成
				CreateMaze(ranBase, i, j);
			}
		}
	}

	//ステージの中央の位置
	PosXZ stageCenter;

	//プレイヤー、敵の初期位置の候補一覧
	std::vector<PosXZ> candidateListForPlayer;
	std::vector<PosXZ> candidateListForEnemy;

	for (int i = 1; i < HEIGHT - 1; i++)
	{
		for (int j = 1; j < WIDTH - 1; j++)
		{
			//今見ている位置が床なら
			if (stage_[i][j] != TYPE_WALL)
			{
				//行き止まりを削除する
				BreakDeadEnd(i, j);
			}

			//今ステージの中央を見ているなら
			if (i == HEIGHT / 2 && j == WIDTH / 2)
			{
				//その位置を保存する
				stageCenter.z_ = i;
				stageCenter.x_ = j;
			}

			//四つ角に敵を配置する
			if((i == 1 && j == 1) ||
			   (i == 1 && j == WIDTH - 2) ||
			   (i == HEIGHT - 2 && j == 1) ||
			   (i == HEIGHT - 2 && j == WIDTH - 2))
			{
				//敵の初期位置の候補とする
				PosXZ candidatePos = { i, j };
				candidateListForEnemy.push_back(candidatePos);
			}
		}
	}

	for (int i = stageCenter.z_ - 1; i < stageCenter.z_ + 2; i++)
	{
		for (int j = stageCenter.x_ - 1; j < stageCenter.x_ + 2; j++)
		{
			//今見ている位置が床ならば
			if (stage_[i][j] == TYPE_FLOOR)
			{
				//プレイヤーの初期位置の候補とする
				PosXZ candidatePos = { i, j };
				candidateListForPlayer.push_back(candidatePos);
			}
		}
	}

	PosXZ startPos = {0, 0};

	//候補の中から初期位置を決める
	startPos = DecideCharacterStartPos(&candidateListForPlayer);

	stage_[(int)(startPos.z_)][(int)(startPos.x_)] = TYPE_PLAYER;

	//敵の初期位置を決める
	for (int i = 0; i < ENEMY_MAX_; i++)
	{
		//候補の中から初期位置を決める
		startPos = DecideCharacterStartPos(&candidateListForEnemy);

		stage_[(int)(startPos.z_)][(int)(startPos.x_)] = TYPE_ENEMY;

		for (auto it = candidateListForEnemy.begin(); it != candidateListForEnemy.end(); it++)
		{
			//候補から選んだ位置を一覧から削除する
			if (startPos.z_ == it->z_ && startPos.x_ == it->x_)
			{
				candidateListForEnemy.erase(it);
				break;
			}
		}
	}
}

//棒倒し法で迷路を作成
void Stage::CreateMaze(int ranBase, int currentPosZ, int currentPosX)
{
	//乱数
	int ranVal;

	int posZ = 0;
	int posX = 0;
	bool isOK = false;

	//かぶりなく１マス埋まるまで
	while (!isOK)
	{
		//乱数生成
		ranVal = rand() % ranBase;

		//乱数が指す方向に１マス進める
		posZ = currentPosZ + fallDirection[ranVal].z_;
		posX = currentPosX + fallDirection[ranVal].x_;

		//先が床なら
		if (stage_[posZ][posX] != TYPE_WALL)
		{
			//そのマスを壁にする（棒を倒す）
			stage_[posZ][posX] = TYPE_WALL;

			//ループ終了
			isOK = true;
		}
	}
}

//行き止まりを削除する
void Stage::BreakDeadEnd(int posZ, int posX)
{
	//壁（外枠以外）がどの方向にあるのかを示す
	std::vector<int> wallList;

	//四方の壁を数える
	int wallCnt = 0;

	//今の位置の上下左右を調べる
	for (int cnt = 0; cnt < FOUR_DIRECTIONS_; cnt++)
	{
		int z = (posZ + fallDirection[cnt].z_);
		int x = (posX + fallDirection[cnt].x_);

		//今調べている位置が壁だったら
		if (stage_[z][x] == TYPE_WALL)
		{
			//数える
			wallCnt++;

			//外枠の壁以外は一覧に格納
			if (!(z == 0) &&			//一番左の列
				!(z == (HEIGHT - 1)) &&	//一番右の行
				!(x == 0) &&			//一番上の行
				!(x == (WIDTH - 1))		//一番下の行
				)
			{
				wallList.push_back(cnt);
			}
		}
	}

	//上下左右に壁が３つ以上あるときは行き止まりなので
	if (wallCnt >= 3)
	{
		//乱数の最大値を四方にある壁の数にする
		int ranBase = wallList.size();

		int ranVal = rand() % ranBase;

		int z = (posZ + fallDirection[wallList.at(ranVal)].z_);
		int x = (posX + fallDirection[wallList.at(ranVal)].x_);

		stage_[z][x] = TYPE_FLOOR;
	}
}

//キャラクターの初期位置を決める
Stage::PosXZ Stage::DecideCharacterStartPos(std::vector<PosXZ>* candidateList)
{
	//プレイヤーの初期位置の候補から一つ選ぶ
	int ranVal = rand() % candidateList->size();

	//選んだ位置を保存し、戻り値にする
	PosXZ startPos = { candidateList->at(ranVal).z_, candidateList->at(ranVal).x_ };
	return startPos;
}

//引数に応じて敵の初期位置を渡す
float Stage::GetEnemyStart(int num, int val)
{
	//渡された番号の位置にある要素を取得
	PosXZ pos = posList_.at(num);

	//第二引数を見て
	switch (val)
	{
		//0の時はxを返す
		case 0:
			return pos.z_;
			break;

		//1の時はzを返す
		case 1:
			return pos.x_;
			break;
	}
}

//ステージを構成する部品を生成する
void Stage::CreateParts(int modelType, int x, int z, float vecX, float vecY, float vecZ)
{
	//各位置のモデルのタイプを見る
	switch (modelType)
	{
		//壁の時
		case TYPE_WALL:
			//壁を生成
			CreateWall((int)x, (int)z, vecX, vecY + 0.5f, vecZ);
			break;

		//プレイヤーの初期値のとき
		case TYPE_PLAYER:
			//位置を記録
			playerStartPos_[0] = vecZ;
			playerStartPos_[1] = x;
			break;

		//上記以外のとき
		default:
			//クリスタルを生成
			Crystal* pCrystal = CreateGameObject<Crystal>(this);
			pCrystal->SetPosition(D3DXVECTOR3(vecX, vecY + 0.4, vecZ));

			crystalCount_++;
			break;
	}

	//敵の初期位置が記されている時
	if (stage_[(int)vecZ][(int)x] == TYPE_ENEMY)
	{
		//位置を記録
		PosXZ pos = { vecZ, x };
		posList_.push_back(pos);
	}

	//床を生成
	Floor* pFloor = CreateGameObject<Floor>(this);
	pFloor->SetPosition(D3DXVECTOR3(vecX, vecY - 0.1f, vecZ));
}

//壁を生成する
void Stage::CreateWall(int x, int z, float vecX, float vecY, float vecZ)
{
	//壁を生成
	Wall* pWall = CreateGameObject<Wall>(this);
	pWall->SetPosition(D3DXVECTOR3(vecX, vecY, vecZ));
}
