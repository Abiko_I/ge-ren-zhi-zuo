#pragma once
#include "AI.h"
#include <list>

class Stage;
class Player;

//ナビゲーションAIを管理するクラス
class NavigationAI : public AI
{
private:
	const int FLOOR_COST;			//床のコスト
	const int PLAYER_PASSED_COST;	//プレイヤーが通った道のコスト

	//ステージの情報
	struct StageInfo
	{
		int x_;			//位置x
		int z_;			//位置z
		int type_;		//ステージ上に生成されたオブジェクトの種類
		int cost_;		//その位置のコスト
	};

	//探索用（ステージ上での方向）
	int searchPos_[4][2] = {
		{-1, 0},	//左
		{0, -1},	//下
		{0, 1},		//上
		{1, 0}		//左
	};

	//位置、歩数、推定値、実歩数をまとめたもの
	struct Info
	{
		int z_;			//縦
		int x_;			//横
		int dist_;		//歩数
		int est_;		//推定値
		int realDist_;	//推定歩数
		int prevZ_;		//自分の一つ前のy
		int prevX_;		//自分の一つ前のx
		DIRECTION dir_;	//方向
	};

	//位置x、zに足す値
	struct AddValue
	{
		int addZ;	//zに足す量
		int addX;	//xに足す量
	};

	//敵の位置
	D3DXVECTOR3 enemyPos_;

	//ステージのポインタ
	Stage* pStage_;

	//プレイヤーのポインタ
	Player* pPlayer_;

	//ステージを格納
	StageInfo stage_[HEIGHT][WIDTH];

	//床だけの位置のリスト（壁が乗っている位置は該当しない）
	std::vector<CharaPos> floorList_;

	//計算中のマスたちを格納
	std::vector<Info> openList_;

	//計算済みのマスたちを格納
	std::vector<Info> closeList_;

	//自分が通れる場所を記録する
	std::list<PosInfo>* pos_;

	//キャラクターの位置
	CharaPos pPos_;

	//展開する方向
	AddValue depDir_[4][3];

	//コストを変更したいステージの位置のアドレス一覧
	std::vector<StageInfo*> costChangeList_;

	//プレイヤーが居た場所のステージの位置のアドレス一覧
	std::vector<StageInfo*> prevPlayerPos_;

public:
	//コンストラクタ
	NavigationAI(IGameObject* parent);

	//デストラクタ
	~NavigationAI();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//親からステージ情報をもらう
	//引数：ステージのポインタ、敵本体が生成されたときの番号
	//戻り値：なし
	void SetStage(Stage* pStage, int num);

	//プレイヤーのポインタをもらう
	//引数：プレイヤーのオブジェクトのポインタ
	//戻り値：なし
	void SetPlayer(Player* pPlayer) { pPlayer_ = pPlayer; }

	//位置のリストのポインタをもらう
	//引数：位置のリストのポインタ
	//戻り値：なし
	void SetPosList(std::list<PosInfo>* pos) { pos_ = pos; }

	//プレイヤーを探す関数達を呼び出す
	//引数：なし
	//戻り値：なし
	void SearchTargetPos(CharaPos targetPos);

	//敵の位置をもらう
	//引数：敵の位置
	//戻り値：なし
	void SetEnemyPos(D3DXVECTOR3 pos) { enemyPos_ = pos; }

	//引数で指定された位置の部品タイプを教える
	//引数：zの位置、xの位置
	//戻り値：指定された位置の部品タイプ
	int GetStage(int z, int x) { return stage_[z][x].type_; }

	//床の位置リストのポインタを渡す
	//引数：なし
	//戻り値：床の位置リスト
	std::vector<CharaPos>* GetFloorList() { return &floorList_; }

	//分岐のある道のリストのポインタを渡す
	//引数：なし
	//戻り値：分岐のある道のリストのポインタ
	std::vector<BranchPosInfo>* GetBranchPosList() { return &branchPosList_; }

	//プレイヤーのモデル番号を教えてもらい、呼び出し元に返す
	//引数：なし
	//戻り値：プレイヤーのモデル番号
	int GetPlayerModel();

	//壁のモデル番号リストを渡す
	//引数：なし
	//戻り値：壁のモデル番号リスト
	std::vector<int>* GetWallModel();

	//プレイヤーの位置を渡す
	//引数：なし
	//戻り値：プレイヤーの位置
	D3DXVECTOR3 GetPlayerPos();

	//A*アルゴリズム法で目標地点までの最短経路を調べる
	//引数：敵の現在地の情報、探索開始時のプレイヤーの位置、調査中リスト、調査済みリスト
	//戻り値：なし
	void Search(Info currentPos, CharaPos playerPos, std::vector<Info>* openList, std::vector<Info>* closeList);

	//引数のリストの中に、調べたい位置が存在するかを確認する
	//引数：中身を確認したいリストのポインタ、存在を確かめたい位置
	//戻り値：第一引数のリストの中に第二引数の位置が存在すればtrue、
	//		　存在しなければfalse
	bool IsExist(std::vector<Info>* list, Info nextPos);

	//通る道を記録させ、調査済みリストから通るべき位置を探す
	//引数：敵の現在地の情報、調査済みリスト
	//戻り値：なし
	void Mark(Info currentPos, std::vector<Info>* closeList);

	//通る位置を記録する
	//引数：位置や距離、方向が１つになった構造体、yの位置（キャラクターの半径）
	//戻り値：なし
	void AddMark(Info pos, float y);

	//分岐地点一覧の中身を全削除する
	//引数：なし
	//戻り値：なし
	void ClearBranchList();

	//渡された方向に分岐があるか調べ、分岐があれば数える
	//引数：基準となる位置z、基準となる位置x、世界から見たときの親（敵）の向き、
	//		分岐の個数
	//戻り値：分岐の数
	int CountBranch(int posZ, int posX, int enemyDir, int branchCnt);

	//敵がプレイヤーを発見したとき、プレイヤーが通った道のコストを下げる
	//引数：なし
	//戻り値：なし
	void CostDown();

	//引数で渡されたステージ位置のアドレスを追加する
	//引数：追加したいステージ位置のアドレス、位置を格納したい一覧のポインタ
	//戻り値：なし
	void PushAddress(StageInfo* address, std::vector<StageInfo*>* posList);
};