#include "SphereCollider.h"


SphereCollider::SphereCollider(D3DXVECTOR3 center, float radius)
{
	center_ = center;
	radius_ = radius;
	type_ = COLLIDER_CIRCLE;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateSphere(Direct3D::pDevice, radius, 8, 4, &pMesh_, 0);
#endif
}

//当たったかどうか
bool SphereCollider::IsHit(Collider* target)
{
	//相手の当たり判定が球体なら
	if (target->type_ == COLLIDER_CIRCLE)
	{
		//引数の球体と、自分（球体）を渡して、判定する
		return IsHitCircleVsCircle((SphereCollider*)target, this);
	}
	//それ以外（箱型）なら
	else
	{
		//引数の球体と、自分（球体）を渡して、判定する
		return IsHitCircleVsBox((BoxCollider*)target, this);
	}
}