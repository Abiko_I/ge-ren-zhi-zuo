#include "Camera.h"
#include "Direct3D.h"
#include "../Player.h"


//コンストラクタ
Camera::Camera(IGameObject * parent)
	:IGameObject(parent, "Camera"), target_ (0, 0, 0)
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化
void Camera::Initialize()
{
	//projection行列を入れる変数
	D3DXMATRIX proj;
	//変数、視野角、アスペクト比、ニアクリッピング面、ファークリッピング面
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 0.5f, 200.0f);
	//プロジェクション行列として proj を使ってという意味
	Direct3D::pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

//更新
void Camera::Update()
{
	//行列変形
	Transform();

	//view行列
	D3DXVECTOR3 worldPosition, worldTarget;

	//worldPosition　に　position_　と　worldMatrix_　を掛け合わせた行列を入れる
	D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
	//				 ↑コード：コーディネートの略

	//worldTarget　に　target_　と　worldMatrix_　を掛け合わせた行列を入れる
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);

	//view行列を入れる変数
	D3DXMATRIX view;
	//変数、視点、焦点、上方向
	D3DXMatrixLookAtLH(&view, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	
	//ビュー行列として view を使ってという意味
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view);
}

//描画
void Camera::Draw()
{
}

//開放
void Camera::Release()
{
}