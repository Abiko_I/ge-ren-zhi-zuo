#include "RootJob.h"
#include "SceneManager.h"

RootJob::RootJob()
{
}


RootJob::~RootJob()
{
}

//初期化
void RootJob::Initialize()
{
	//シーンマネージャーを生成・親は自分
	CreateGameObject<SceneManager>(this);
}

//更新
void RootJob::Update()
{
}

//描画
void RootJob::Draw()
{
}

//解放
void RootJob::Release()
{
}
