#pragma once
#include <string>
#include "Fbx.h"


namespace Model
{
	struct ModelData
	{
		//string型のfilename
		std::string fileName;

		//ロードしたFbxファイルのアドレスを格納
		Fbx* pFbx;

		//行列
		D3DXMATRIX matrix;

		//コンストラクタ
		ModelData() : fileName(""), pFbx(nullptr)
		{
			//なにもしない行列にして、初期化
			D3DXMatrixIdentity(&matrix);
		}
	};

	int Load(std::string fileName, LPD3DXEFFECT pEffect = nullptr);

	void SetPEffect(int handle, LPD3DXEFFECT pEffect);

	//Draw関数に画像の番号を渡す
	void Draw(int handle);

	void SetMatrix(int handle, D3DXMATRIX & matrix);

	void AllRelease();

	//レイキャスト（レイを飛ばして当たり判定する）
	//引数１：レイが当たる側のモデル番号
	//引数２：必要なものをまとめたデータ
	void RayCast(int handle, RayCastData *data);
}