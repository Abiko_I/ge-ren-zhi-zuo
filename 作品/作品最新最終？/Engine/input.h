#pragma once
#define DIRECTINPUT_VERSION 0x0800

#include <dInput.h>

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")

//グローバルをインクルードしないためリリース
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;} 

namespace Input
{
	void Initialize(HWND hWnd);		//初期化
	void Update();					//更新
	bool IsKey(int keyCode);		//キーが押されているか否か
	bool IsKeyDown(int keyCode);	//キーが押されたままか否か
	bool IsKeyUp(int keyCode);		//キーから指が離れたか否か
	void Release();					//解放
};