#pragma once
#include "IGameObject.h"

class Player;

//カメラを管理するクラス
class Camera : public IGameObject
{
	//焦点を入れる
	D3DXVECTOR3 target_;

public:
	//コンストラクタ
	Camera(IGameObject* parent);

	//デストラクタ
	~Camera();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ターゲットを　target　として受け取り、target_　に代入
	void SetTarget(D3DXVECTOR3 target) { target_ = target; }

	void SetPosition(D3DXVECTOR3 position) { position_ = position; }
};