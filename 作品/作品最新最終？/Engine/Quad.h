#pragma once
#include "Global.h"

class Quad
{
public:
	struct Vertex
	{
		D3DXVECTOR3 pos;		//位置情報
		D3DXVECTOR3 normal;		//法線情報
		D3DXVECTOR2 uv;			//uv座標
	};

	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9 pIndexBuffer_;
	LPDIRECT3DTEXTURE9 pTexture_;				//テクスチャを入れる変数
	D3DMATERIAL9 material_;						//マテリアル

	Quad();
	~Quad();

	int vertex_ = 24;		//頂点数
	int polygon_ = 12;		//ポリゴン数

	virtual void Load(const char *file);
	void VertexAlloc(Vertex *vertexList, int listSize);
	void IndexAlloc(int *indexList, int listSize);
	void LoadTexture(const char *file);
	void Draw(const D3DXMATRIX &matrix);
};