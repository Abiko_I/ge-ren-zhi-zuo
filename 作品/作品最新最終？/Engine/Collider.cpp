#include "Global.h"
#include "Collider.h"
#include "SphereCollider.h"
#include "BoxCollider.h"

//コンストラクタ
Collider::Collider() : owner_(nullptr), pMesh_(nullptr)
{
}

//デストラクタ
Collider::~Collider()
{
	if (pMesh_ != nullptr)
	{
		pMesh_->Release();
	}
}

//球体と球体の当たり判定
bool Collider::IsHitCircleVsCircle(SphereCollider* sphereA, SphereCollider* sphereB)
{
	//球体Aの中心位置を取得
	D3DXVECTOR3 aPos = sphereA->GetCenter();

	//球体Bの中心位置を取得
	D3DXVECTOR3 bPos = sphereB->GetCenter();

	//球体Bの中心から球体Aの中心へのベクトルを求める
	D3DXVECTOR3 v = aPos - bPos;

	//上記で求めたベクトルの長さが、球体Aと球体Bの半径を足した長さより小さかったら
	if (D3DXVec3Length(&v) <= (sphereA->radius_ + sphereB->radius_))
	{
		//当たっている
		return true;
	}

	//当たっていない
	return false;
}

//箱型と球体の当たり判定
bool Collider::IsHitCircleVsBox(BoxCollider* box, SphereCollider* sphere)
{
	//箱型のベクトルのそれぞれの長さを取得
	D3DXVECTOR3 boxCenter = box->GetCenter();
	D3DXVECTOR3 sphereCenter = sphere->GetCenter();

	//ベクトル
	D3DXVECTOR3 vec(0, 0, 0);

	//幅、高さ、奥行分だけ回す
	for (int i = 0; i < 3; i++)
	{
		//幅、高さ、奥行の方向を求める
		D3DXVECTOR3 dir = box->GetDirection(i);

		//幅、高さ、奥行の長さの半分を求める
		float halfLen = box->GetLength(i);

		//球体の中心から箱型の中心までのベクトルを求め、
		//そのベクトルと、箱型の幅、高さ、奥行のそれぞれの方向で内積を求める
		float dotVec = D3DXVec3Dot(&(sphereCenter - boxCenter), &dir);

		//幅、高さ、奥行のサイズの半分で割る
		dotVec /= halfLen;

		//絶対値を求める
		dotVec = fabs(dotVec);

		//求めた内積が幅、高さ、奥行の長さより長かったら
		if (dotVec > 1)
		{
			//大きい部分だけのベクトルを求める
			vec += ((1 - dotVec) * halfLen * dir);
		}
	}

	//箱型と球体の最短距離の長さを求める
	float minLen = D3DXVec3Length(&vec);

	//箱型と球体の最短距離が、球体の半径以下なら
	if (minLen <= sphere->radius_)
	{
		//当たっている
		return true;
	}

	//当たっていない
	return false;
}

//コリジョンを描画する
void Collider::Draw(D3DXVECTOR3 position)
{
	D3DXMATRIX mat;

	//引数で渡された位置にコリジョンの位置を足して行列を変換
	D3DXMatrixTranslation(&mat, position.x + center_.x, position.y + center_.y, position.z + center_.z);

	//位置を設定
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &mat);

	//メッシュサブセットを描画
	pMesh_->DrawSubset(0);
}

D3DXVECTOR3 Collider::GetCenter()
{
	//返す中心位置
	D3DXVECTOR3 recvCenter;

	//自分を適用しているオブジェクトの今の位置をもらう
	D3DXVECTOR3 ownerCenter = owner_->GetPosition();

	//自分と、自分を適用しているオブジェクトの位置を足す
	recvCenter = center_ + ownerCenter;

	return recvCenter;
}
