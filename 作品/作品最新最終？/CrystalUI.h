#pragma once
#include "Engine/IGameObject.h"

//クリスタル関係のUIを管理するクラス
class CrystalUI : public IGameObject
{
private:
	//数字
	enum
	{
		PICT_NUM_MAX = 10,
		PICT_FRAME,
		PICT_MAX,
	};

	//現在のクリスタルの数
	const float STANDARD_POSX_CURRENT_;	//画像のxの基準の位置
	const float ADD_POSX_CURRENT_;		//画像のxの調整値
	const float STANDARD_POSY_CURRENT_;	//画像のyの基準の位置
	const float ADD_POSY_CURRENT_;		//画像のyの調整値

	//最大値のクリスタルの最大値
	const float STANDARD_POSX_MAX_;		//画像のxの基準の位置
	const float ADD_POSX_MAX_;			//画像のxの調整値
	const float STANDARD_POSY_MAX_;		//画像のyの基準の位置
	const float ADD_POSY_MAX_;			//画像のyの調整値

	//クリスタルの数を示す画像
	int hCrystalCurrent_[PICT_NUM_MAX];

	//クリスタルの最大数を示す画像
	int hCrystalMax_[PICT_NUM_MAX];

	//クリスタルの数を表示する枠
	int hFrame_;

	//クリスタルの最大数
	int crystalMax_;

	//クリスタルの数
	int crystalNum_;

public:
	//コンストラクタ
	CrystalUI(IGameObject* parent);

	//デストラクタ
	~CrystalUI();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//数字を描画する
	//引数：
	//戻り値：なし
	void NumberDraw(int num, float stdX, float addX, float stdY, float addY, int* hPict);

	//クリスタルの最大数をもらう
	//引数：クリスタルの最大数
	//戻り値：なし
	void SetCrystalMax(int crystalNum) { crystalMax_ = crystalNum; }

	//現在の残りのクリスタルの数をもらう
	//引数：残りのクリスタルの数
	//戻り値：なし
	void SetCrystalNum(int crystalMax) { crystalNum_ = crystalMax; }
};