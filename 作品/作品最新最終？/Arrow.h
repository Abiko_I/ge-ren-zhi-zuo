#pragma once
#include "Engine/IGameObject.h"

//矢印を管理するクラス
class Arrow : public IGameObject
{
private:
	//キャラクターの向きを指す
	enum DIRECTION
	{
		FORWARD,	//前を向いている
		RIGHT,		//右を向いている
		LEFT,		//左を向いている
		BACKWARD,	//後ろを向いている
		NONE		//方向無し
	};

	//体色
	struct Color
	{
		float red;		//RGBAのRの値
		float green;	//RGBAのGの値
		float blue;		//RGBAのBの値
		float alpha;	//RGBAのAの値
	};

	//モデル番号
	int hModel_;

	//自分の中心となるオブジェクト
	IGameObject* pCenterObj_;

	//方向を示す対象のオブジェクト
	IGameObject* pTargetObj_;

	//描画するかどうか
	bool isDraw_;

	//HLSLから作成されたシェーダを入れる
	LPD3DXEFFECT* pEffect_;

	//矢印の色
	Color arrowColor_;

public:
	//コンストラクタ
	Arrow(IGameObject * parent);

	//デストラクタ
	~Arrow();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//自分の位置からターゲットの位置へのベクトルを求める
	//引数：なし
	//戻り値：自分からターゲットへのベクトル
	D3DXVECTOR3 CalcVector();

	//引数のベクトルのx成分から、反転させるかどうかを決める
	//引数：x成分が+と-どちらの方向を向いているのかを調べたいベクトル
	//戻り値：引数のベクトルのx成分が-ならtrue、+ならfalse
	bool isReverse(D3DXVECTOR3 vec);

	//向きを変える
	//引数：回転度数の符号を反転させるかどうかのフラグ、
	//		自分からターゲットへのベクトル
	//戻り値：なし
	void ChangeDirection(bool isReverse, D3DXVECTOR3 vec);

	//渡された引数のオブジェクトを自分の中心とする
	//引数：中心となるオブジェクト
	//戻り値：なし
	void SetCenterObj(IGameObject* pObj) { pCenterObj_ = pObj; }

	//渡された引数のオブジェクトを自分が向く方向の対象とする
	//引数：方向を示す対象のオブジェクト
	//戻り値：なし
	void SetTargetObj(IGameObject* pObj) { pTargetObj_ = pObj; }
};
