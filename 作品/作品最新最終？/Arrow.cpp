#include "Arrow.h"
#include "Player.h"
#include "Enemy.h"
#include "Engine/Model.h"
#include "PlayScene.h"
#include "Engine/Direct3D.h"

//コンストラクタ
Arrow::Arrow(IGameObject * parent)
	:IGameObject(parent, "Arrow"), hModel_(-1), isDraw_(false)
{
}

//デストラクタ
Arrow::~Arrow()
{
}

//初期化
void Arrow::Initialize()
{
	//親（プレイシーン）から効果をもらう
	pEffect_ = ((PlayScene*)pParent_)->GetEnemyPEffect();

	//モデルデータのロード
	hModel_ = Model::Load("Data/Models/Arrow.fbx", *pEffect_);
	assert(hModel_ >= 0);

	rotate_ = D3DXVECTOR3(0, 0, 0);
}

//更新
void Arrow::Update()
{
	//自分の位置を、中心とするオブジェクトの位置に合わせる
	position_ = pCenterObj_->GetPosition();

	//ベクトルを求める
	D3DXVECTOR3 vec = CalcVector();

	//上記のベクトルの長さを求める
	float length = D3DXVec3Length(&vec);

	//一定より短いとき
	if (length >= 4.0)
	{
		//描画する
		isDraw_ = true;

		//向きを設定
		ChangeDirection(isReverse(vec), vec);
	}
	else
	{
		//描画しない
		isDraw_ = false;
	}
}

//描画
void Arrow::Draw()
{
	if (isDraw_)
	{
		//敵の体色に合わせる
		(*pEffect_)->SetVector("SPECIFIED_DIFFUSE",
			&D3DXVECTOR4(
				arrowColor_.red = (((Enemy*)pTargetObj_)->GetColor()).red,
				arrowColor_.green = (((Enemy*)pTargetObj_)->GetColor()).green,
				arrowColor_.blue = (((Enemy*)pTargetObj_)->GetColor()).blue,
				arrowColor_.alpha = (((Enemy*)pTargetObj_)->GetColor()).alpha
			));

		Model::SetMatrix(hModel_, worldMatrix_);

		//ビュー行列
		D3DXMATRIX view;
		//第一引数で渡したものを第二引数で渡したアドレス元に入れてくれる
		Direct3D::pDevice->GetTransform(D3DTS_VIEW, &view);

		//プロジェクション行列
		D3DXMATRIX proj;
		//第一引数で渡したものを第二引数で渡したアドレス元に入れてくれる
		Direct3D::pDevice->GetTransform(D3DTS_PROJECTION, &proj);

		//W、V、Pの順番でかける
		D3DXMATRIX matWVP = worldMatrix_ * view * proj;

		//かけた行列を渡す
		(*pEffect_)->SetMatrix("WVP", &matWVP);

		D3DXMATRIX mat = worldMatrix_;

		//回転行列
		D3DXMATRIX rotateX, rotateY, rotateZ;
		D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
		D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
		D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

		//拡大縮小
		D3DXMATRIX scale;
		D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);

		//拡縮行列を逆行列にする
		D3DXMatrixInverse(&scale, nullptr, &scale);

		//行列掛け合わせ
		mat = scale * rotateZ * rotateX * rotateY;

		//渡す
		(*pEffect_)->SetMatrix("RS", &mat);

		//ライトの情報を扱う変数
		D3DLIGHT9 lightState;

		//ライトの情報を取得
		Direct3D::pDevice->GetLight(0, &lightState);

		//三次元ベクトルを四次元ベクトルにキャストして渡す
		(*pEffect_)->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

		//カメラの位置を渡す
		(*pEffect_)->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0, 5, -10));

		//ワールド行列を渡す
		(*pEffect_)->SetMatrix("W", &worldMatrix_);

		//ここから描画開始
		(*pEffect_)->Begin(NULL, 0);

		//普通に表示
		(*pEffect_)->BeginPass(0);

		//厚みのない部分を描画するために両面描画
		Direct3D::pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
		Model::Draw(hModel_);

		//表のみ描画（初期に戻す）
		Direct3D::pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
		(*pEffect_)->EndPass();

		//シェーダを使った描画終わり
		//以降はDirectXのデフォルト描画に戻る
		(*pEffect_)->End();
	}
}

//開放
void Arrow::Release()
{
}

//ベクトルを求める
D3DXVECTOR3 Arrow::CalcVector()
{
	//自分の位置からターゲットの位置へのベクトルを求める
	D3DXVECTOR3 vec = position_ - pTargetObj_->GetPosition();

	return vec;
}

//引数のベクトルのx成分から、反転させるかどうかを決める
bool Arrow::isReverse(D3DXVECTOR3 vec)
{
	if (vec.x > 0)
	{
		//逆にする
		return true;
	}

	//そのまま使う
	return false;
}

//向きを変える
void Arrow::ChangeDirection(bool isReverse, D3DXVECTOR3 vec)
{
	//奥を向いているベクトルを基準とする
	D3DXVECTOR3 pos = D3DXVECTOR3(0, 0, -1);

	//それぞれのベクトルを正規化
	D3DXVec3Normalize(&vec, &vec);
	D3DXVec3Normalize(&pos, &pos);

	//内積を求める
	float dot = D3DXVec3Dot(&vec, &pos);

	//角度を求める
	float angle = acos(dot);

	//逆にするように指示が出ているなら
	if (isReverse)
	{
		//角度を反転させてラジアンから度に修正
		angle = D3DXToDegree(angle * -1);
	}
	else
	{
		//そのままの角度をラジアンから度に修正
		angle = D3DXToDegree(angle);
	}

	//回転する
	rotate_.y = angle;
}
