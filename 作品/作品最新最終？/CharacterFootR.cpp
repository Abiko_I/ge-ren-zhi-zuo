#include "CharacterFootR.h"
#include "Character.h"
#include "Engine/Model.h"

//コンストラクタ
CharacterFootR::CharacterFootR(IGameObject * parent)
	:CharacterFoot(parent, "CharacterFootR")
{
}

//デストラクタ
CharacterFootR::~CharacterFootR()
{
}

//初期化
void CharacterFootR::Initialize()
{
	sign_ = 1;

	//移動速度
	moveSpeed_ = 0.0f;
}

//更新
void CharacterFootR::Update()
{
	//動くべきかを判断する
	if (((Character*)pParent_)->GetIsMoveFeet())
	{
		//足を動かして歩く
		Move();
	}
	else
	{
		//足を止める
		Stop();
	}
}

//描画
void CharacterFootR::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void CharacterFootR::Release()
{
}