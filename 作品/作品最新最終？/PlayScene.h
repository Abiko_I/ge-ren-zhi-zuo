#pragma once
#include <vector>
#include "Engine/Global.h"

class Player;
class Stage;
class Enemy;
class Arrow;
class CameraTarget;
class CrystalUI;
class Water;

//プレイシーンを管理するクラス IGameObjectを継承
class PlayScene : public IGameObject
{
private:
	//BGM
	enum
	{
		CHASE,	//敵が追跡状態のときのBGM
		NORMAL,	//敵が巡回、探索状態のときのBGM
		MAX
	};

	//プレイヤーのポインタ
	Player* pPlayer_;

	//ステージのポインタ
	Stage* pStage_;

	//敵のポインタリスト
	std::vector<Enemy*> pEnemyList_;

	//矢印のポインタリスト
	std::vector<Arrow*> pArrowList_;

	//カメラの焦点のポインタ
	CameraTarget* pCameraTarget_;

	//クリスタルのUIを管理するクラスのポインタ
	CrystalUI* pCrystalUI_;

	//水面のポインタ
	Water* pWater_;

	//音
	char* sounds_[MAX];

	//現在流れている音
	char* currentSound_;

	//次に流す音
	char* nextSound_;

	//HLSLから作成されたシェーダを入れる
	LPD3DXEFFECT pEnemyEffect_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);
	~PlayScene();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//引数で渡されたBGMを流し、現在流れているBGMを停止する
	//引数：次に流したい音のポインタ
	//戻り値：なし
	void SwitchSounds(char* cueName);

	//モデルに付ける効果を渡す
	//引数：なし
	//戻り値：呼び出し元が適用する効果
	LPD3DXEFFECT* GetEnemyPEffect() { return &pEnemyEffect_; }
};