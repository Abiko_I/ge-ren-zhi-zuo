#include "Crystal.h"
#include "Stage.h"
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"

//コンストラクタ
Crystal::Crystal(IGameObject * parent)
	:IGameObject(parent, "Crystal"), hModel_(-1), pEffect_(nullptr)
{
}

//デストラクタ
Crystal::~Crystal()
{
	//ステージが持つクリスタルの数を減らす
	((Stage*)pParent_)->DecreaseCrystal();
}

//初期化
void Crystal::Initialize()
{
	////親（Stage）から適用する効果をもらう
	pEffect_ = ((Stage*)pParent_)->GetPEffect();

	////モデルのロード
	hModel_ = Model::Load("Data/Models/Crystal.fbx", *pEffect_);
	assert(hModel_ >= 0);

	collision_ = new BoxCollider(
		D3DXVECTOR3(position_.x, position_.y, position_.z),
		D3DXVECTOR3(0.16f, 0.33f, 0.16f));

	AddCollider(collision_);
}

//更新
void Crystal::Update()
{
	//回転しながら上下に揺れる
	static float cnt = 0.001;
	position_.y = ((sin(cnt)) / 100) + 0.3f;

	rotate_.y += 0.2f;

	//回転度のリセット
	if (rotate_.y >= 360.0f)
	{
		rotate_.y = 0.0f;
	}

	if (cnt >= 10.0)
	{
		cnt = 0.001f;
	}

	cnt += 0.001f;
}

//描画
void Crystal::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);

	//ビュー行列
	D3DXMATRIX view;
	//第一引数で渡したものを第二引数で渡したアドレス元に入れてくれる
	Direct3D::pDevice->GetTransform(D3DTS_VIEW, &view);

	//プロジェクション行列
	D3DXMATRIX proj;
	//第一引数で渡したものを第二引数で渡したアドレス元に入れてくれる
	Direct3D::pDevice->GetTransform(D3DTS_PROJECTION, &proj);

	//W、V、Pの順番でかける
	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	//かけた行列を渡す
	(*pEffect_)->SetMatrix("WVP", &matWVP);

	D3DXMATRIX mat = worldMatrix_;

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);

	//拡縮行列を逆行列にする
	D3DXMatrixInverse(&scale, nullptr, &scale);

	//行列掛け合わせ
	mat = scale * rotateZ * rotateX * rotateY;

	//渡す
	(*pEffect_)->SetMatrix("RS", &mat);

	//ライトの情報を扱う変数
	D3DLIGHT9 lightState;

	//ライトの情報を取得
	Direct3D::pDevice->GetLight(0, &lightState);

	//三次元ベクトルを四次元ベクトルにキャストして渡す
	(*pEffect_)->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	//カメラの位置を渡す
	(*pEffect_)->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0, 5, -10));

	//ワールド行列を渡す
	(*pEffect_)->SetMatrix("W", &worldMatrix_);

	//ここから描画開始
	(*pEffect_)->Begin(NULL, 0);

	//普通に表示
	(*pEffect_)->BeginPass(0);

	//厚みのない部分を描画するために両面描画
	Direct3D::pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	Model::Draw(hModel_);

	//表のみ描画（初期に戻す）
	Direct3D::pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	(*pEffect_)->EndPass();

	//シェーダを使った描画終わり
	//以降はDirectXのデフォルト描画に戻る
	(*pEffect_)->End();
}

//開放
void Crystal::Release()
{
}