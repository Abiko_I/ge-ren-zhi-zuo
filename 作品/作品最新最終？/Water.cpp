#include "Water.h"
#include "Engine/Model.h"
#include "Engine/Direct3D.h"

//コンストラクタ
Water::Water(IGameObject * parent)
	:IGameObject(parent, "Water"), hModel_(-1), pEffect_(nullptr)
{
}

//デストラクタ
Water::~Water()
{
	//ポインタだから
	SAFE_RELEASE(pEffect_);
}

//初期化
void Water::Initialize()
{
	//HLSLファイルからエフェクト（シェーダ）を生成
	//エラーが出た結果を入れる
	LPD3DXBUFFER err = 0;

	//エラーが出たとき
	if (FAILED(
		D3DXCreateEffectFromFile(Direct3D::pDevice, "WaterShader.hlsl",
			NULL, NULL, D3DXSHADER_DEBUG, NULL, &pEffect_, &err)))
	{
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダーエラー", MB_OK);
	}

	assert(pEffect_ != nullptr);

	//モデルデータのロード
	hModel_ = Model::Load("Data/Models/Water.fbx", pEffect_);
	assert(hModel_ >= 0);
}

//更新
void Water::Update()
{
}

//描画
void Water::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);

	//ビュー行列
	D3DXMATRIX view;
	//第一引数で渡したものを第二引数で渡したアドレス元に入れてくれる
	Direct3D::pDevice->GetTransform(D3DTS_VIEW, &view);

	//プロジェクション行列
	D3DXMATRIX proj;
	//第一引数で渡したものを第二引数で渡したアドレス元に入れてくれる
	Direct3D::pDevice->GetTransform(D3DTS_PROJECTION, &proj);

	//W、V、Pの順番でかける
	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	//かけた行列を渡す
	pEffect_->SetMatrix("WVP", &matWVP);

	D3DXMATRIX mat = worldMatrix_;

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);

	//拡縮行列を逆行列にする
	D3DXMatrixInverse(&scale, nullptr, &scale);

	//行列掛け合わせ
	mat = scale * rotateZ * rotateX * rotateY;

	//渡す
	pEffect_->SetMatrix("RS", &mat);

	//ライトの情報を扱う変数
	D3DLIGHT9 lightState;

	//ライトの情報を取得
	Direct3D::pDevice->GetLight(0, &lightState);

	//三次元ベクトルを四次元ベクトルにキャストして渡す
	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	//カメラの位置を渡す
	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0, 5, -10));

	//ワールド行列を渡す
	pEffect_->SetMatrix("W", &worldMatrix_);

	//ここから描画開始
	pEffect_->Begin(NULL, 0);

	//テクスチャの貼る位置を少しずつずらす
	static float scroll = 0.0f;
	scroll += 0.001;
	pEffect_->SetFloat("SCROLL", scroll);

	//普通に表示
	pEffect_->BeginPass(0);

	//厚みのない部分を描画するために両面描画
	Direct3D::pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	Model::Draw(hModel_);

	//表のみ描画（初期に戻す）
	Direct3D::pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	pEffect_->EndPass();

	//シェーダを使った描画終わり
	//以降はDirectXのデフォルト描画に戻る
	pEffect_->End();
}

//開放
void Water::Release()
{
}