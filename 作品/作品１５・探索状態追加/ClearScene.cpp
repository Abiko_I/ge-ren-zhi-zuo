#include "ClearScene.h"
#include "Engine/Image.h"

//コンストラクタ
ClearScene::ClearScene(IGameObject * parent)
	: IGameObject(parent, "ClearScene"), hPict_(-1)
{
}

//初期化
void ClearScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Data/Picts/Clear.jpg");
	assert(hPict_ >= 0);
}

//更新
void ClearScene::Update()
{
	//エンターキーが押されていたら
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//シーンを変える
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void ClearScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void ClearScene::Release()
{
}