#pragma once

#include <string>
#include <vector>
#include "Sprite.h"
#include "Direct3D.h"

namespace Image
{
	struct ImageData
	{
		//ファイル名
		std::string fileName;

		//ロードした画像データのアドレスを格納
		Sprite*		pSprite;

		//行列
		D3DXMATRIX	matrix;

		//コンストラクタ
		ImageData() : pSprite(nullptr)
		{
			//なにもしない行列にして初期化
			D3DXMatrixIdentity(&matrix);
		}
	};

	int Load(std::string fileName);

	//Draw関数に画像の番号を渡す
	void Draw(int handle);

	void SetMatrix(int handle, D3DXMATRIX & matrix);

	void AllRelease();
}