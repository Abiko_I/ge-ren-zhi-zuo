#include "Quad.h"
#include "Direct3D.h"
																				 //↓この記述で構造体は初期化できる
Quad::Quad() : pVertexBuffer_(nullptr), pIndexBuffer_(nullptr), pTexture_(nullptr), material_({ 0 })
{
}


Quad::~Quad()
{
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);
}

void Quad::Load(const char *file)
{
	Vertex vertexList[] = {
		D3DXVECTOR3(-1, 1, 0),	D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 0),	//左上　
		D3DXVECTOR3(1, 1, 0),	D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 0),	//右上
		D3DXVECTOR3(1, -1, 0),	D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 1),	//右下
		D3DXVECTOR3(-1, -1, 0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 1),	//左下
	};

	VertexAlloc(vertexList, sizeof(vertexList));

	int indexList[] = { 0, 2, 3, 0, 1, 2 };				//頂点が時計回り

	IndexAlloc(indexList, sizeof(indexList));

	LoadTexture(file);
}

void Quad::VertexAlloc(Vertex *vertexList, int listSize)
{
	//										↓上で作った配列と同じサイズを指定
	Direct3D::pDevice->CreateVertexBuffer(listSize, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED, &pVertexBuffer_, 0);
	//	　↑位置		↑法線			↑uv情報

	//エラー処理
	assert(pVertexBuffer_ != nullptr);

	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);		//バーテックスバッファをロック（捕まえる）
	memcpy(vCopy, vertexList, listSize);		//メモリデータのコピー　

	pVertexBuffer_->Unlock();							//バーテックスバッファのロック解除
}

void Quad::IndexAlloc(int *indexList, int listSize)
{
	Direct3D::pDevice->CreateIndexBuffer(listSize, 0, D3DFMT_INDEX32,
		D3DPOOL_MANAGED, &pIndexBuffer_, 0);

	//エラー処理
	assert(pIndexBuffer_ != nullptr);

	DWORD *iCopy;
	pIndexBuffer_->Lock(0, 0, (void**)&iCopy, 0);		//インデックスバッファをロック（捕まえる）
	memcpy(iCopy, indexList, listSize);		//メモリデータのコピー
	
	pIndexBuffer_->Unlock();							//インデックスバッファのロック解除
}

void Quad::LoadTexture(const char *file)
{
	//テクスチャ作成
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, file,
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);

	assert(pTexture_ != nullptr);

	//マテリアルの設定
	material_.Diffuse.r = 1.0f;			//Rの光を何％反射するかを示す（1で100%）
	material_.Diffuse.g = 1.0f;			//Gの光を何％反射するかを示す（1で100%）
	material_.Diffuse.b = 1.0f;			//Bの光を何％反射するかを示す（1で100%)

	//環境光追加　影が柔らかくなる
	material_.Ambient.r = 0.2f;
	material_.Ambient.g = 0.2f;
	material_.Ambient.b = 0.2f;
}

void Quad::Draw(const D3DXMATRIX &matrix)
{
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);

	Direct3D::pDevice->SetTexture(0, pTexture_);								//使いたいテクスチャを指定
	Direct3D::pDevice->SetMaterial(&material_);									//使用するマテリアルを指定
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));	//SetStreamSource第一引数は０枚目という意味
	Direct3D::pDevice->SetIndices(pIndexBuffer_);
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);	//フラグ管理
						//      ↑位置			↑法線			↑uv情報
	Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertex_, 0, polygon_);	//0,0,4（頂点数）,0,2（ポリゴン数→三角形二つ）
}