#pragma once
#include "Collider.h"

//箱型の当たり判定
class BoxCollider : public Collider
{
private:
	//自由に呼び出せるように
	friend class Collider;

	//判定サイズ（幅、高さ、奥行）
	D3DXVECTOR3 size_;

public:
	//コンストラクタ
	//引数１：当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
	//　　２：当たり判定のサイズ（幅、高さ、奥行）
	BoxCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size);

	//当たり判定
	//引数：相手の当たり判定
	//戻り値：接触していれば真、接触していなければ偽
	bool IsHit(Collider* target) override;

	//引数で渡された値に対応した幅、高さ、奥行の半分の長さを返す
	//引数：知りたい方向に対応した値
	//戻り値：長さ
	float GetLength(int element);

	//引数で渡された値に対応した方向を返す
	//引数：知りたい方向に対応した値
	//戻り値：方向
	D3DXVECTOR3 GetDirection(int element);
};

