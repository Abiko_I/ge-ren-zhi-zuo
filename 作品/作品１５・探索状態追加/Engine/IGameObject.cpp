#include "IGameObject.h"
#include "Global.h"
#include "Collider.h"

//コンストラクタ（親も名前もなし）
IGameObject::IGameObject() :
	IGameObject(nullptr, "")
{
	//コライダーリストを空にしておく
	colliderList_.clear();
}

//コンストラクタ（名前なし）
IGameObject::IGameObject(IGameObject * parent) :
	IGameObject(parent, "")

{
}

//コンストラクタ（標準）
IGameObject::IGameObject(IGameObject * parent, const std::string& name):
	pParent_(parent), name_(name), position_(D3DXVECTOR3(0, 0, 0)), rotate_(D3DXVECTOR3(0, 0, 0)),
	scale_(D3DXVECTOR3(1, 1, 1)), dead_(false)
{
}

//デストラクタ
IGameObject::~IGameObject()
{
	for (auto it = colliderList_.begin(); it != colliderList_.end(); it++)
	{
		SAFE_DELETE(*it);
	}
	colliderList_.clear();
}

void IGameObject::UpdateSub()
{
	//自分自身のアップデートを呼び出す
	Update();

	//コライダーリストの中身が存在するなら
	if (!(colliderList_.empty()))
	{
		//現在のシーンから当たり判定を行う
		Collision(SceneManager::GetCurrentScene());
	}

	//行列関数を呼び出す
	Transform();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		//イテレータの中身（子供）のアップデートサブを呼び出す
		(*it)->UpdateSub();
	}

	for (auto it = childList_.begin(); it != childList_.end();)
	{
		//子供が死にたいかどうかを確認する
		if ((*it)->dead_ == true)
		{
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			it = childList_.erase(it);
		}

		else
		{
			it++;
		}
	}
}

void IGameObject::DrawSub()
{
	//自分自身のドローを呼び出す
	Draw();

	//リリース時は削除
#ifdef _DEBUG
		//コリジョンの描画
	if ((Direct3D::isDrawCollision_))
	{
		CollisionDraw();
	}
#endif

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		//イテレータの中身（子供）のドローサブを呼び出す
		(*it)->DrawSub();
	}
}

void IGameObject::ReleaseSub()
{
	//自分自身のリリースを呼び出す
	Release();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		//イテレータの中身（子供）のリリースサブを呼び出す
		(*it)->ReleaseSub();

		//子供の it 番目をデリート
		SAFE_DELETE(*it);
	}
}

void IGameObject::KillMe()
{
	dead_ = true;
}

//コライダーを追加する
void IGameObject::AddCollider(Collider* collider)
{
	//コライダーをセットする
	collider->SetGameObject(this);

	//リストに追加
	colliderList_.push_back(collider);
}

//当たり判定
void IGameObject::Collision(IGameObject* targetObject)
{
	for (auto i = this->colliderList_.begin(); i != this->colliderList_.end(); i++)
	{
		for (auto j = targetObject->colliderList_.begin(); j != targetObject->colliderList_.end(); j++)
		{
			if ((*i)->IsHit(*j))
			{
				this->OnCollision(targetObject);
			}
		}
	}

	//子供との当たり判定
	for (auto itr = targetObject->childList_.begin(); itr != targetObject->childList_.end(); itr++)
	{
		//再起させる
		Collision(*itr);
	}
}

//pObjを渡してchildListに入れる
void IGameObject::PushBackChild(IGameObject* pObj)
{
	childList_.push_back(pObj);
}

//コリジョンを表示
void IGameObject::CollisionDraw()
{
	//ワイヤーフレーム
	Direct3D::pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

	//ライティングをOFFに
	Direct3D::pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	//テクスチャはなし
	Direct3D::pDevice->SetTexture(0, nullptr);

	for (auto it = this->colliderList_.begin(); it != this->colliderList_.end(); it++)
	{
		//コリジョンを描画
		(*it)->Draw(position_);
	}

	Direct3D::pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);

	Direct3D::pDevice->SetRenderState(D3DRS_LIGHTING, Direct3D::isLighting_);
}

void IGameObject::Transform()
{
	D3DXMATRIX matT, matRX, matRY, matRZ, matS;

	//移動
	D3DXMatrixTranslation(&matT, position_.x, position_.y, position_.z);

	//回転 rotate_.〇を渡してラジアンから度に直す
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate_.x));	//X軸を回転させる行列
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate_.y));	//Y軸を回転させる行列
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate_.z));	//Z軸を回転させる行列

	//拡大・縮小
	D3DXMatrixScaling(&matS, scale_.x, scale_.y, scale_.z);

	//行列を掛け合わせる
	localMatrix_ = (matS * matRX * matRY * matRZ * matT);

	//ローカルマトリックスに親のワ−ルドマトリックスをかけて、自分のワールドマトリックスに入れる
	if (pParent_ == nullptr)
	{
		worldMatrix_ = localMatrix_;
	}
	else
	{
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	}
}