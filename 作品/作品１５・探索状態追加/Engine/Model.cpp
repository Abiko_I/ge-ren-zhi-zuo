#include <vector>
#include "Model.h"

namespace Model
{
	//構造体のModelDataのベクター型のdataList
	std::vector<ModelData*> dataList;

	int Load(std::string fileName)
	{
		ModelData* pData = new ModelData;
		pData->fileName = fileName;

		//開いたファイル一覧から同じファイルがな無いか探す
		bool isExist = false;

		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			if (dataList[i]->fileName == fileName)
			{
				//i番目のアドレスを渡す
				pData->pFbx = dataList[i]->pFbx;

				//データを見つけたことを知らせる
				isExist = true;
				break;
			}
		}

		//同じデータがなかったら
		if (isExist == false)
		{
			//新たにロード
			pData->pFbx = new Fbx;
			pData->pFbx->Load(fileName.c_str());
		}

		//dataListの後ろのほうに入れる
		dataList.push_back(pData);

		//dataListの要素数を返す
		return dataList.size() - 1;
	}
	void Draw(int handle)
	{
		//dataListのハンドル番目のマトリックスを渡す
		dataList[handle]->pFbx->Draw(dataList[handle]->matrix);
	}
	void SetMatrix(int handle, D3DXMATRIX & matrix)
	{
		//dataListのハンドル番目のマトリックスに、受け取ったマトリックスを入れる
		dataList[handle]->matrix = matrix;
	}


	void Release(int handle)
	{
		bool isExist = false;

		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//自分のファイル名以外で、同じものがあるかどうか調べる
			if (i != handle && dataList[i] != nullptr && dataList[i]->pFbx == dataList[handle]->pFbx)
			{
				//データを見つけたことを知らせる
				isExist = true;
				break;
			}
		}

		//同じモデルを他でも使っていなければモデル解放
		if (isExist == false)
		{
			//dataListのハンドル番目のpFbxをデリート
			SAFE_DELETE(dataList[handle]->pFbx);
		}
		//dataListそのものをデリート
		SAFE_DELETE(dataList[handle]);
	}

	//全てのモデルを解放
	void AllRelease()
	{
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//まだ削除していなければ
			if (dataList[i] != nullptr)
			{
				//リリースを呼ぶ　i （ハンドル番号）を渡す
				Release(i);
			}
		}

		//全て削除
		dataList.clear();
	}

	//レイキャスト
	void RayCast(int handle, RayCastData *data)
	{
		//レイが当たる対象までのベクトルを求める
		D3DXVECTOR3 target = data->start + data->dir;

		//逆行列を計算する
		D3DXMATRIX matInv;
		D3DXMatrixInverse(&matInv, 0, &dataList[handle]->matrix);

		//求めた逆行列を使用して、レイの発射位置からのベクトルを変形する
		D3DXVec3TransformCoord(&data->start, &data->start, &matInv);

		//逆行列を使用して、対象に当たるまでのベクトルを変形する
		D3DXVec3TransformCoord(&target, &target, &matInv);

		//レイを撃つ方向を求める
		data->dir = target - data->start;

		//引数で渡された番号のモデルのレイキャストを呼ぶ
		dataList[handle]->pFbx->RayCast(data);
	}
};