#pragma once
#include <d3dx9.h>
#include <list>
#include <string>

class Collider;
class SphereCollider;

#define HEIGHT	19		//ステージの縦の長さ（csvの縦）
#define WIDTH	19		//ステージの横の長さ（csvの横）

class IGameObject
{
//継承先でも使えるように
protected:
	//親 そのままでは使えないのでポインタに
	IGameObject* pParent_;

	//子供は数がわからない、途中で消去・追加することがあるのでlist型で宣言
	std::list<IGameObject*> childList_;

	//文字列を入れる型
	std::string name_;

	//ローカルマトリックスで使う
	D3DXVECTOR3 position_;	//位置
	D3DXVECTOR3 rotate_;	//角度
	D3DXVECTOR3 scale_;		//サイズ

	//自分の動き
	D3DXMATRIX localMatrix_;

	//位置向き行列を入れる　世界の中心からの〇〇
	//自分のローカルマトリックスに、親のワールドマトリックスをかけることでできる
	D3DXMATRIX worldMatrix_;

	//子供を殺すかどうか
	bool dead_;

	//コライダーリスト
	std::list<Collider*> colliderList_;


	//行列を変形する関数
	void Transform();

public:
	//コンストラクタ(名前、親ともに無し)
	IGameObject();
	//コンストラクタ(名前なし)
	IGameObject(IGameObject* parent);
	//コンストラクタ(標準)
	IGameObject(IGameObject* parent, const std::string& name);
	//デストラクタ 仮想関数にして、子供のクラスでは子供のデストラクタが呼ばれるようにする
	virtual ~IGameObject();

	//純粋仮想関数↓
	//初期化
	virtual void Initialize() = 0;

	//更新
	virtual void Update() = 0;

	//描画
	virtual void Draw() = 0;

	//解放
	virtual void Release() = 0;

	//衝突を検知 衝突した相手をもらう
	virtual void OnCollision(IGameObject *pTarget) {};

	void UpdateSub();
	void DrawSub();
	void ReleaseSub();

	void KillMe();

	//コライダーを追加する
	//引数：付けるコライダー
	//戻り値：なし
	void AddCollider(Collider* collider);

	void Collision(IGameObject* targetObject);

	//ゲッター↓
	D3DXVECTOR3 GetPosition() { return position_; }
	
	D3DXVECTOR3 GetRotate() { return rotate_; }

	D3DXVECTOR3 GetScale() { return scale_; }

	D3DXMATRIX GetMatrix() { return worldMatrix_; }

	//自分を教える
	//引数：なし
	//戻り値：自分
	IGameObject* GetSelf() { return this; }

	std::string GetName() { return name_; }

	//自分の親を教える
	//引数：なし
	//戻り値：自分の親
	IGameObject* GetParent() { return pParent_; }

	//セッター↓
	void SetPosition(D3DXVECTOR3 position) { position_ = position; }

	void SetRatate(D3DXVECTOR3 rotate) { rotate_ = rotate; }

	void SetScale(D3DXVECTOR3 scale) { scale_ = scale; }

	//classの型をT（何型かわからない）にして、指定した型で使えるようにする
	template <class T>
	//プレイヤーを作成　親を渡す
	T* CreateGameObject(IGameObject* parent)	//インライン関数にする
	{
		//新オブジェクト生成
		T* p = new T(parent);

		//子供一覧に追加する
		parent->PushBackChild(p);

		//初期化する
		p->Initialize();

		//作成したものを返す（指定した型になる）
		return p;
	}

	void PushBackChild(IGameObject* pObj);

	IGameObject* FindObject(const std::string& name) { return nullptr; }

	void CollisionDraw();
};