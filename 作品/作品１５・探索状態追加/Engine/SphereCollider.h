#pragma once
#include "Collider.h"

class SphereCollider : public Collider
{
private:
	//自由に呼び出せるようにする
	friend class Collider;

private:
	//半径
	float radius_;

public:
	SphereCollider(D3DXVECTOR3 center, float radius);

	//当たっているかの判定
	//引数：相手の当たり判定
	//戻り値：接触していれば真、接触していなければ偽
	bool IsHit(Collider* target) override;
};