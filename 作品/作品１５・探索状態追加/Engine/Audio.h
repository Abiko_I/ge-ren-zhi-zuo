#pragma once
#include <xact3.h> 

//音声を管理する
namespace Audio
{
	//初期化
	void Initialize();

	//ロード
	void Load();

	//再生
	void Play(char* pCueName);

	//停止
	void Stop(char* pCueName);

	//開放
	void Release();
}