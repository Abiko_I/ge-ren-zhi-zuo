#include <assert.h>
#include "Input.h"

namespace Input
{
	//外部からは使わないからソースで定義
	LPDIRECTINPUT8   pDInput = nullptr;
	//デバイス：キーボード
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr;
	//１バイト（unsigned char）の配列　キーの数が２５６を超えることはないから
	BYTE keyState[256] = { 0 };			//初期化もしている
	BYTE prevKeyState[256];    //前フレームでの各キーの状態

	void Initialize(HWND hWnd)
	{
		DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION,
		IID_IDirectInput8, (VOID**)&pDInput, nullptr);

		//エラー処理
		assert(pDInput != nullptr);

		pDInput->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr);					//デバイスオブジェクト作成
		pKeyDevice->SetDataFormat(&c_dfDIKeyboard);										//デバイスの種類を指定
		pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);	//これを動かしているときに裏の処理を動かすかどうかの強調レベル

		//エラー処理
		assert(pKeyDevice != nullptr);
	}

	void Update()
	{
		//前キーフレームでの各キーの状態に、現在の各キーのサイズ分の
		memcpy(prevKeyState, keyState, sizeof(keyState));

		pKeyDevice->Acquire();

		//GetDeviceStateが実行された瞬間の全てのキーの情報
		pKeyDevice->GetDeviceState(sizeof(keyState), &keyState);
	}

	//keyCodeにキーの状態が入る
	bool IsKey(int keyCode)
	{
		//引数に渡された番号のキーが押されてるか
		if (keyState[keyCode] & 0x80)		//2進数で128　→　16進で0x80にしてかっこよく
		{
			//右から8ビット目が1なら
			return true;
		}

		return false;
	}

	bool IsKeyDown(int keyCode)
	{
		//今は押してて、前回は押してない
		if ((keyState[keyCode] & 0x80) && !(prevKeyState[keyCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	bool IsKeyUp(int keyCode)
	{
		//今は押していなくて、前回は押していた
		if (!(keyState[keyCode] & 0x80) && (prevKeyState[keyCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	void Release()
	{
		//解放
		SAFE_RELEASE(pKeyDevice);
		SAFE_RELEASE(pDInput);
	}
}