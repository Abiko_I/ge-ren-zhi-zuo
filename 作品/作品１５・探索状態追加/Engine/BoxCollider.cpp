#include "BoxCollider.h"

BoxCollider::BoxCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size)
{
	center_ = basePos;
	size_ = size;
	type_ = COLLIDER_BOX;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateBox(Direct3D::pDevice, size.x, size.y, size.z, &pMesh_, 0);
#endif
}

//当たり判定
bool BoxCollider::IsHit(Collider* target)
{
	//相手の当たり判定が球体なら
	if (target->type_ == COLLIDER_CIRCLE)
	{
		//自分（箱型）と引数の球体を渡して、判定する
		return IsHitCircleVsBox(this, (SphereCollider*)target);
	}
}

float BoxCollider::GetLength(int element)
{
	//引数を見てどの方向を返すかを決める
	switch (element)
	{
		//0が渡されたら幅の半分の長さを返す
		case 0:
			return size_.x / 2;
			break;

		//1が渡されたら高さの半分の長さを返す
		case 1:
			return size_.y / 2;
			break;

		//2が渡されたら奥行の半分の長さを返す
		case 2:
			return size_.z / 2;
			break;

		default:
			return 0;
	}
}

//指定された方向を返す
D3DXVECTOR3 BoxCollider::GetDirection(int element)
{
	//引数を見てどの方向を返すかを決める
	switch(element)
	{
		//0が渡されたらxの方向を返す
		case 0:
			return D3DXVECTOR3(1, 0, 0);
			break;

		//1が渡されたらyの方向を返す
		case 1:
			return D3DXVECTOR3(0, 1, 0);
			break;

		//2が渡されたらzの方向を返す
		case 2:
			return D3DXVECTOR3(0, 0, 1);
			break;

		default:
			return D3DXVECTOR3(0, 0, 0);
	}
}
