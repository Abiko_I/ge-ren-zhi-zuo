#pragma once
#include "Engine/IGameObject.h"

//キャラクターの足を管理するクラス
class CharacterFoot : public IGameObject
{
protected:
	//親の速度
	enum
	{
		MODE_NORMAL,	//通常の状態
		MODE_FAST		//移動速度が上がっている状態
	};

	const float MOVE_FRONT_LIMIT_;	//足を前に出す時の限界
	const float MOVE_BACK_LIMIT_;	//足を後ろに出す時の限界

	//モデル番号
	int hModel_;

	//符号
	int sign_;

	//足を動かす速度
	float moveSpeed_;

	//親の位置
	D3DXVECTOR3 parentPos_;

	//親の前の位置
	D3DXVECTOR3 prevParentPos_;

public:
	//コンストラクタ
	CharacterFoot(IGameObject * parent, std::string name);

	//デストラクタ
	~CharacterFoot();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//モデルを読み込む
	//引数：モデルのパス
	//戻り値：なし
	void LoadModel(std::string modelPath);

	//足を動かす
	//引数：なし
	//戻り値：なし
	void Move();

	//足を止める
	//引数：なし
	//戻り値：なし
	void Stop();

	//足を動かす速度を変更する
	//引数：親クラスの状態
	//戻り値：なし
	void ChangeSpeed(float speed) { moveSpeed_ = speed; }

	//親の位置をもらう
	//引数：親の位置
	//戻り値：なし
	void SetParentPos(D3DXVECTOR3 pos) { parentPos_ = pos; }
};