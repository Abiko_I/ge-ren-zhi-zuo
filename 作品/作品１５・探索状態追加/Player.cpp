#include "Player.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"
#include "Stage.h"
#include "Wall.h"
#include "CharacterFootR.h"
#include "CharacterFootL.h"
#include "Engine/Audio.h"

Player::Player(IGameObject * parent) : Character(parent, "Player"),
	RADIUS_(0.35f), NORMAL_SPEED_(0.05), ACCELE_SPEED_(0.06), NORMAL_SPEED_FOOT_(2.0f),
	ACCELE_SPEED_FOOT_(3.0f), STAMINA_MAX_(300), moveSpeed_(0.05f), crystalCnt_(0),
	moveX_(0.0f), moveZ_(0.0f), currentDir_(FORWARD), nextDir_(NONE),
	toAccelerate_(false), state_(FINE), rotateValue_(0.0f), nextAddMove_(0, 0, 1),
	isInput_(false), prevPos_(0.0f, 0.0f, 0.0f)
{
	hModel_[FINE] = -1;
	hModel_[TIRED] = -1;
}


Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//モデルデータのロード
	hModel_[FINE] = Model::Load("Data/Models/Player/Player_Fine_Body.fbx");
	assert(hModel_[FINE] >= 0);
	hModel_[TIRED] = Model::Load("Data/Models/Player/Player_Tired_Body.fbx");
	assert(hModel_[TIRED] >= 0);

	//最初は前を向かせる
	direction_[0] = FORWARD;
	direction_[1] = NONE;
	rotate_.y = 0.0f;

	//初期は通常速度
	moveSpeed_ = NORMAL_SPEED_;

	//スタミナは最大
	stamina_ = STAMINA_MAX_;

	//球体コライダーをつける
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), RADIUS_);
	AddCollider(collision);

	//足のモデルのパスを格納
	footModelPath_[0] = "Player/Player_Foot_R";
	footModelPath_[1] = "Player/Player_Foot_L";

	//右足を生成し、パスを渡してモデルを読み込ませる
	pFootR = CreateGameObject<CharacterFootR>(this);
	pFootR->LoadModel(footModelPath_[0]);

	//速度を決める
	pFootR->ChangeSpeed(NORMAL_SPEED_FOOT_);

	//左足を生成し、パスを渡してモデルを読み込ませる
	pFootL = CreateGameObject<CharacterFootL>(this);
	pFootL->LoadModel(footModelPath_[1]);

	//速度を決める
	pFootL->ChangeSpeed(NORMAL_SPEED_FOOT_);
}

//更新
void Player::Update()
{
	//移動ベクトルの初期化
	addMove_ = D3DXVECTOR3(0, 0, moveSpeed_);

	//スタミナが空になったら
	if (stamina_ <= 0)
	{
		//疲労状態に遷移
		state_ = TIRED;
		stamina_ = 0;

		//走れなくなる
		toAccelerate_ = false;

		//速度変更
		ChangeSpeed();
	}
	//スタミナが全回復したら
	else if (stamina_ >= STAMINA_MAX_)
	{
		//元気な状態に遷移
		state_ = FINE;
		stamina_ = STAMINA_MAX_;

		//速度変更
		ChangeSpeed();
	}

	//現在の向きを示すベクトルになる
	DirectionRotate(rotate_.y, &addMove_);

	//今の方向を決める
	currentDir_ = JudgeDirection(addMove_.x, addMove_.z);

	//入力処理
	Input();

	//スタミナの管理をする
	StaminaManagement();

	//次の行動が指定されている時
	if (nextDir_ != NONE)
	{
		//次の方向に曲がれるか確認
		CheckAround();
	}

	//四捨五入して、さらにその絶対値が360度になったら最初に戻す
	if (fabs(round(rotate_.y)) == 360)
	{
		rotate_.y = 0.0f;
	}

	//入力があったら動き始める
	if (isInput_)
	{
		//移動
		AddMove();
	}

	//位置を更新する
	pFootL->SetParentPos(position_);
	pFootR->SetParentPos(position_);
}

//描画
void Player::Draw()
{
	//モデル番号を格納する
	int num;

	switch(state_)
	{
		//元気な時と走っている時は
		case FINE:
		case RUN:
			//元気な時のモデルを描画
			num = FINE;
			break;

		//疲労状態の時は
		case TIRED:
			//疲労状態の時のモデルを描画
			num = TIRED;
			break;
	}
	//モデルを描画
	Model::SetMatrix(hModel_[num], worldMatrix_);
	Model::Draw(hModel_[num]);
}

//開放
void Player::Release()
{
}

//何かに当たった時の処理
void Player::OnCollision(IGameObject * pTarget)
{
	//壁と当たった時
	if (pTarget->GetName() == "Wall")
	{
		//めり込みを直す
		PositionAdjustment(pTarget, RADIUS_);
	}

	//クリスタルと当たった時
	if (pTarget->GetName() == "Crystal")
	{
		//効果音を再生する
		Audio::Play("GetCrystal");

		pTarget->KillMe();

		crystalCnt_--;

		//ステージ上のクリスタルを全て取得したら
		if (crystalCnt_ == 0)
		{
			//クリアシーンへ遷移
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_CLEAR);
		}
	}
}

//足を動かすべきかどうかを判断する
bool Player::IsMoveFeet()
{
	//今の位置と前の位置が違うなら
	if (position_ != prevPos_)
	{
		//今の位置を前回の位置として保存
		prevPos_ = position_;

		//動いているので足も動かす
		return true;
	}

	//足は動かさなくていい
	return false;
}

//入力に対する行動を起こす
void Player::Input()
{
	//前進
	if (Input::IsKeyDown(DIK_W) || Input::IsKeyDown(DIK_UP))
	{
		rotateValue_ = 0.0f;

		//コピー
		nextAddMove_ = addMove_;

		//次の向きを示すベクトルになる
		DirectionRotate(rotateValue_, &nextAddMove_);

		//次の方向を決める
		nextDir_ = JudgeDirection(nextAddMove_.x, nextAddMove_.z);
	}
	//後退
	else if (Input::IsKeyDown(DIK_S) || Input::IsKeyDown(DIK_DOWN))
	{
		rotateValue_ = 180.0f;

		//コピー
		nextAddMove_ = addMove_;

		//次の向きを示すベクトルになる
		DirectionRotate(rotateValue_, &nextAddMove_);

		//次の方向を決める
		nextDir_ = JudgeDirection(nextAddMove_.x, nextAddMove_.z);
	}
	//右移動
	else if (Input::IsKeyDown(DIK_D) || Input::IsKeyDown(DIK_RIGHT))
	{
		rotateValue_ = 90.0f;

		//コピー
		nextAddMove_ = addMove_;

		//次の向きを示すベクトルになる
		DirectionRotate(rotateValue_, &nextAddMove_);

		//次の方向を決める
		nextDir_ = JudgeDirection(nextAddMove_.x, nextAddMove_.z);
	}
	//左移動
	else if (Input::IsKeyDown(DIK_A) || Input::IsKeyDown(DIK_LEFT))
	{
		rotateValue_ = -90.0f;

		//コピー
		nextAddMove_ = addMove_;

		//次の向きを示すベクトルになる
		DirectionRotate(rotateValue_, &nextAddMove_);

		//次の方向を決める
		nextDir_ = JudgeDirection(nextAddMove_.x, nextAddMove_.z);
	}

	//加速
	if (Input::IsKeyDown(DIK_SEMICOLON))
	{
		//元気な時は
		if (state_ == FINE)
		{
			//走る状態に遷移
			state_ = RUN;

			//加速する
			toAccelerate_ = true;

			//速度変更
			ChangeSpeed();
		}
	}
}


//方向を更新する
void Player::DirectionUpdate()
{
	//最新にする
	currentDir_ = nextDir_;
	nextDir_ = NONE;
}

//速度を変える
void Player::ChangeSpeed()
{
	//足を動かす速度
	float footSpeed = 0.0f;

	//加速するなら
	if (toAccelerate_)
	{
		//速度を上げる
		moveSpeed_ = ACCELE_SPEED_;
		footSpeed = ACCELE_SPEED_FOOT_;
	}
	else
	{
		//速度を元に戻す
		moveSpeed_ = NORMAL_SPEED_;
		footSpeed = NORMAL_SPEED_FOOT_;
	}

	//足の速度も変える
	pFootL->ChangeSpeed(footSpeed);
	pFootR->ChangeSpeed(footSpeed);
}

//スタミナの管理をする
void Player::StaminaManagement()
{
	switch (state_)
	{
		//疲労状態の時
		case TIRED:
			//スタミナを回復する
			stamina_++;
			break;

		//走っている時
		case RUN:
			//徐々にスタミナを減らす
			stamina_--;
			break;

		default: 
			return;
	}
}

//次の方向に曲がれるか確認
void Player::CheckAround()
{
	//プレイヤーの位置と、プレイヤーが立っている床の位置を記録
	float posA = 0;
	float posB = 0;
	
	//隣接するステージパーツのタイプ
	int stageType = TYPE_WALL;

	//自分の立っている位置と、床の位置を記録する
	if (fabs(addMove_.x) > fabs(addMove_.z))
	{
		//移動ベクトルのx成分が0より大きかったら
		if (addMove_.x > 0)
		{
			posA = position_.x;
			posB = round(position_.x);
		}
		else
		{
			posA = round(position_.x);
			posB = position_.x;
		}
	}
	else
	{
		//移動ベクトルのz成分が0より大きかったら
		if (addMove_.z > 0)
		{
			posA = position_.z;
			posB = round(position_.z);
		}
		else
		{
			posA = round(position_.z);
			posB = position_.z;
		}
	}

	//確認するべきは前後か左右か
	if (fabs(nextAddMove_.x) > fabs(nextAddMove_.z))
	{
		//左右どちらか
		if (nextAddMove_.x > 0)
		{
			//自分の右
			stageType = stage_[(int)round(position_.x) + 1][(int)round(position_.z)];
		}
		else
		{
			//自分の左
			stageType = stage_[(int)round(position_.x) - 1][(int)round(position_.z)];
		}
	}
	else
	{
		//前後どちらか
		if (nextAddMove_.z > 0)
		{
			//自分の前
			stageType = stage_[(int)round(position_.x)][(int)round(position_.z) + 1];
		}
		else
		{
			//自分の後ろ
			stageType = stage_[(int)round(position_.x)][(int)round(position_.z) - 1];
		}
	}

	//隣接するステージのタイプが壁以外なら
	if (stageType != TYPE_WALL)
	{
		//進行可能な方向への入力があった（最初のみ）
		isInput_ = true;

		//次の方向に変更する
		Execution(posA, posB, &nextAddMove_);
	}
}

//内積を求め、角度を返す
float Player::CalcAngle(D3DXVECTOR3* dirA, D3DXVECTOR3* dirB)
{
	//正規化
	D3DXVec3Normalize(dirA, dirA);
	D3DXVec3Normalize(dirB, dirB);

	//今と次のベクトルの内積を求める
	float dot = D3DXVec3Dot(dirA, dirB);

	//角度を求める
	float angle = acos(dot);

	return angle;
}

//移動ベクトルを回転させる
void Player::DirectionRotate(float angle, D3DXVECTOR3* vec)
{
	//引数の角度で行列を回転
	D3DXMATRIX m;
	D3DXMatrixRotationY(&m, D3DXToRadian(angle));

	//行列でベクトルを変形
	D3DXVec3TransformCoord(vec, vec, &m);
}

void Player::AddMove()
{
	position_ += addMove_;
}

//引数の値の大きさによって+か-なのかを判断する
int Player::JudgeSign(float vecX)
{
	int sign = 0;

	//引数のxが0より小さければ
	if (vecX < 0.0f)
	{
		//-回転させる
		return sign = -1;
	}

	//+回転させる
	return sign = 1;
}

//次の方向を向く
void Player::Execution(float posA, float posB, D3DXVECTOR3* copyVec)
{
	//引数二つの距離の絶対値を測る
	float dist = fabs(posA - posB);

	//一定距離以内なら
	if (dist <= 0.2)
	{
		//回転させる
		rotate_.y += rotateValue_;

		//移動させる
		addMove_ = *copyVec;

		//次の方向に変更
		DirectionUpdate();
	}
}

//初期位置を教えてもらう
void Player::SetStartPos(float * pos)
{
	position_.x = pos[1];
	position_.y = RADIUS_;
	position_.z = pos[0];

	prevPos_ = position_;
}

//ステージをコピーする
void Player::SetStage(Stage* pStage)
{
	for (float i = 0; i < HEIGHT; i += 1.0)
	{
		for (float j = 0; j < WIDTH; j += 1.0)
		{
			//ステージの情報をもらう
			stage_[(int)j][(int)i] = pStage->GetStage((int)i, (int)j);
		}
	}
}


//今の方向に対応するベクトルを返す
D3DXVECTOR3 Player::GetCurrentDir()
{
	//移動ベクトルが方向ベクトルでもあるので、移動ベクトルを返す
	return addMove_;
}

//渡された引数の方向に対応するベクトルを返す
D3DXVECTOR3 Player::GetCurrentDir(int dir)
{
	//現在の方向を見る
	switch (dir)
	{
		case FORWARD:
			return D3DXVECTOR3(0, 0, 1);

		case BACKWARD:
			return D3DXVECTOR3(0, 0, -1);

		case RIGHT:
			return D3DXVECTOR3(1, 0, 0);

		case LEFT:
			return D3DXVECTOR3(-1, 0, 0);

			//方向がなし（NONE）の時は前を向いている
		default:
			return D3DXVECTOR3(0, 0, 1);
	}
}

//モデル番号を返す
int Player::GetModelHandle()
{
	switch (state_)
	{
		//疲労状態なら
		case TIRED:
			//疲労状態のモデルの番号を返す
			return hModel_[TIRED];
			break;

		//元気な時、走っている時なら
		case FINE:
		default:
			//元気な時のモデルの番号を返す
			return hModel_[FINE];
			break;
	}
}
