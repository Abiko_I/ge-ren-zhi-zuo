#include "Floor.h"
#include "Engine/Model.h"

//コンストラクタ
Floor::Floor(IGameObject * parent)
	:IGameObject(parent, "Floor"), hModel_(-1)
{
}

//デストラクタ
Floor::~Floor()
{
}

//初期化
void Floor::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Data/Models/Floor.fbx");
	assert(hModel_ >= 0);
}

//更新
void Floor::Update()
{
}

//描画
void Floor::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Floor::Release()
{
}