#include "Crystal.h"
#include "Stage.h"
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"

//コンストラクタ
Crystal::Crystal(IGameObject * parent)
	:IGameObject(parent, "Crystal"), hModel_(-1)
{
}

//デストラクタ
Crystal::~Crystal()
{
	//ステージが持つクリスタルの数を減らす
	((Stage*)pParent_)->DecreaseCrystal();
}

//初期化
void Crystal::Initialize()
{
	//モデルのロード
	hModel_ = Model::Load("Data/Models/Crystal.fbx");
	assert(hModel_ >= 0);

	collision_ = new BoxCollider(
		D3DXVECTOR3(position_.x, position_.y, position_.z),
		D3DXVECTOR3(0.16f, 0.33f, 0.16f));

	AddCollider(collision_);
}

//更新
void Crystal::Update()
{
	//回転しながら上下に揺れる
	static float cnt = 0.001;
	position_.y = ((sin(cnt)) / 100) + 0.3f;

	rotate_.y += 0.2f;

	//回転度のリセット
	if (rotate_.y >= 360.0f)
	{
		rotate_.y = 0.0f;
	}

	if (cnt >= 10.0)
	{
		cnt = 0.001f;
	}

	cnt += 0.001f;
}

//描画
void Crystal::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Crystal::Release()
{
}