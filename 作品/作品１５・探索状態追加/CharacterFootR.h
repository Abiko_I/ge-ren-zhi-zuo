#pragma once
#include "CharacterFoot.h"
#include "Engine/IGameObject.h"

//キャラクターの足を管理するクラス
class CharacterFootR : public CharacterFoot
{
private:

public:
	//コンストラクタ
	CharacterFootR(IGameObject* parent);

	//デストラクタ
	~CharacterFootR();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};