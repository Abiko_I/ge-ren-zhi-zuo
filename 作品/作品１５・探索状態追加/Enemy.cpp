#include "Enemy.h"
#include "Stage.h"
#include "Player.h"
#include "CameraTarget.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"
#include "CharacterAI.h"
#include "NavigationAI.h"
#include "CharacterFootR.h"
#include "CharacterFootL.h"

//コンストラクタ
Enemy::Enemy(IGameObject * parent) : Character(parent, "Enemy"),
	RADIUS_(0.35f), START_POS_(0.0f, 0.0f, 0.0f), NORMAL_SPEED_(0.04f),
	ACCELE_SPEED_(0.055f),NORMAL_SPEED_FOOT_(2.0f),ACCELE_SPEED_FOOT_(3.0f),
	WAIT_TIME_(15), isCallSearch_(true), state_(PATROL), isMove_(true),
	isAccept_(false), waitCount_(0)
{
	hModel_[CHASE] = -1;
	hModel_[PATROL] = -1;
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//未発見状態のモデルロード
	hModel_[PATROL] = Model::Load("Data/Models/Enemy/Enemy_Patrol_Body.fbx");
	assert(hModel_[PATROL] >= 0);

	//発見状態のモデルロード
	hModel_[CHASE] = Model::Load("Data/Models/Enemy/Enemy_Chase_Body.fbx");
	assert(hModel_[CHASE] >= 0);

	//発見状態のモデルロード
	hModel_[SEARCH] = Model::Load("Data/Models/Enemy/Enemy_Search_Body.fbx");
	assert(hModel_[SEARCH] >= 0);

	//向きの初期化
	rotate_.y = 0.0f;

	//初期は通常速度
	moveSpeed_ = NORMAL_SPEED_;

	//球体コライダーをつける
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), RADIUS_);
	AddCollider(collision);

	//キャラクターAIを生成してポインタを所持
	pCharaAI_ = CreateGameObject<CharacterAI>(this);

	//速度を教える
	pCharaAI_->SetSpeed(moveSpeed_);

	//ナビゲーションAIを生成してポインタを所持
	pNaviAI_ = CreateGameObject<NavigationAI>(this);

	//足のモデルのパスを格納
	footModelPath_[0] = "Enemy/Enemy_Foot_R";
	footModelPath_[1] = "Enemy/Enemy_Foot_L";

	//右足を生成し、パスを渡してモデルを読み込ませる
	pFootR = CreateGameObject<CharacterFootR>(this);
	pFootR->LoadModel(footModelPath_[0]);

	//速度を決める
	pFootR->ChangeSpeed(NORMAL_SPEED_FOOT_);

	//左足を生成し、パスを渡してモデルを読み込ませる
	pFootL = CreateGameObject<CharacterFootL>(this);
	pFootL->LoadModel(footModelPath_[1]);

	//速度を決める
	pFootL->ChangeSpeed(NORMAL_SPEED_FOOT_);

	//現在の方向
	currentDir_ = FORWARD;

	//次の方向
	nextDir_ = NONE;
}

//更新
void Enemy::Update()
{
	//移動ベクトルの初期化
	addMove_ = D3DXVECTOR3(0, 0, 0);

	//視界にプレイヤーがいるかを確認
	pCharaAI_->VisualConfirmation();

	//状態を見て、呼ぶ更新を変える
	switch (state_)
	{
		case PATROL:
			isMove_ = true;
			PatrolUpdate();
			break;

		case CHASE:
			isMove_ = true;
			ChaseUpdate();
			break;

		case SEARCH:
			SearchUpdate();
			break;
	}

	if (isMove_)
	{
		//向きを変える
		pCharaAI_->ChangeDirection();

		//移動
		position_ += addMove_;

		//位置を更新する
		pFootL->SetParentPos(position_);
		pFootR->SetParentPos(position_);
	}
}

//描画
void Enemy::Draw()
{
	Model::SetMatrix(hModel_[state_], worldMatrix_);
	Model::Draw(hModel_[state_]);
}

//開放
void Enemy::Release()
{
}

//何かに当たった時の処理
void Enemy::OnCollision(IGameObject * pTarget)
{
	//プレイヤーと当たった時
	if (pTarget->GetName() == "Player")
	{
		//相手を殺す
		pTarget->KillMe();

		//プレイヤーを倒したらゲームオーバーシーンに遷移
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
	}
}

//足を動かすべきかどうかを判断する
bool Enemy::IsMoveFeet()
{
	//キャラクターAIからの命令を受け付ける状態なら
	if (isMove_)
	{
		//移動中であるので「動かす」
		return true;
	}

	//回転中であるので「動かさない」
	return false;
}

//巡回状態のときの更新
void Enemy::PatrolUpdate()
{
	//「目標地点が欲しい」と要請が出ているなら
	if (pCharaAI_->GetRequest())
	{
		//ランダムで目標地点を決める
		pCharaAI_->DecideTarget(pNaviAI_->GetFloorList());

		//経路探査
		RouteSearch();
	}
}

//追跡状態のときの更新
void Enemy::ChaseUpdate()
{
	//プレイヤーの位置を目標とする
	D3DXVECTOR3 playerPos = pNaviAI_->GetPlayerPos();

	//「目標地点が欲しい」と要請が出ているなら
	if (pCharaAI_->GetRequest())
	{
		//プレイヤーと自分が同じマス上にいたら
		if (pCharaAI_->IsTargetSamePos(playerPos.z, playerPos.x))
		{
			pCharaAI_->TargetSamePosMovement();
		}
		else
		{
			//キャラクターAIの目標地点を最新のプレイヤーの位置に設定する
			pCharaAI_->SetTarget((int)round(playerPos.z), (int)round(playerPos.x));

			if (isCallSearch_)
			{
				//経路探査
				RouteSearch();
			}
		}
	}
}

//探索状態のときの更新
void Enemy::SearchUpdate()
{
	if (isCallSearch_)
	{
		//するべき動作をもらう
		switch (pCharaAI_->GetBehavior())
		{
			case CharacterAI::BEHAVIOR_WALK:
				isMove_ = true;

				isAccept_ = false;
				break;

			case CharacterAI::BEHAVIOR_ROTATE:
				isMove_ = false;

				isAccept_ = false;

				//次の動作に移るタイミングが来たら
				if (waitCount_ <= 0)
				{
					//見渡す
					Survey();
				}

				waitCount_--;

				break;
		}
	}
	
	//「目標地点が欲しい」と要請が出ているなら
	if (pCharaAI_->GetRequest())
	{
		if (isCallSearch_)
		{
			//経路探査
			RouteSearch();
		}
	}
}

//近くを見渡す
void Enemy::Survey()
{
	//今するべき動作を行う
	switch (task_)
	{
		case 0:
			pCharaAI_->GetRotateForSurvey(currentDir_);
			waitCount_ = WAIT_TIME_;
			task_ = 1;
			break;

		case 1:
			pCharaAI_->GetRotateForSurvey(currentDir_);
			waitCount_ = 0;
			task_ = 0;
			isAccept_ = true;
			break;
	}
}

//目標地点に向かうための経路を調べる
void Enemy::RouteSearch()
{
	//キャラクターAIが探索すると判断したなら
	if (isCallSearch_)
	{
		//キャラクターAIからナビゲーションAIに位置のリストを渡す
		pNaviAI_->SetPosList(pCharaAI_->GetPosList());

		//探索する
		pNaviAI_->SearchTargetPos(pCharaAI_->GetTarget());

		//要請に応じたので、要請を解除する
		pCharaAI_->ResetRequest();

		//キャラクターAIがもつ「現在の目標地点」と「次の目標地点」を初期化
		pCharaAI_->InitIterator();
	}
}

//ステージのポインタをもらう
void Enemy::SetStage(Stage * pStage, int num)
{
	//ナビゲーションAIに渡す
	pNaviAI_->SetStage(pStage, num);

	//位置をAI達に渡す
	pCharaAI_->SetEnemyPos(position_);
	pNaviAI_->SetEnemyPos(position_);
}

//プレイヤーのポインタをもらう
void Enemy::SetPlayer(Player * pPlayer)
{
	//ナビゲーションAIに渡す
	pNaviAI_->SetPlayer(pPlayer);
}

//方向をもらう
void Enemy::SetDirection(int nextDir, float degree)
{
	//引数の方向が自分の方向とは違うとき
	if (currentDir_ != nextDir)
	{
		//回転する
		rotate_.y += degree;

		//四捨五入して、さらにその絶対値が360度になったら最初に戻す
		if (fabs(round(rotate_.y)) == 360)
		{
			rotate_.y = 0.0f;
		}

		//方向を更新する
		currentDir_ = nextDir;
	}
}

//指定された方向を向いて進む
void Enemy::ChangeSpeed()
{
	//足を動かす速度
	float footSpeed = 0.0f;

	//状態によって移動速度を変更する
	switch (state_)
	{
		//発見していないなら
		case PATROL:
		case SEARCH:

			//通常の移動速度
			moveSpeed_ = NORMAL_SPEED_;
			footSpeed = NORMAL_SPEED_FOOT_;
			break;

		//発見しているなら
		case CHASE:

			//加速時の移動速度
			moveSpeed_ = ACCELE_SPEED_;
			footSpeed = ACCELE_SPEED_FOOT_;
			break;
	}

	//速度を教える
	pCharaAI_->SetSpeed(moveSpeed_);

	//足の速度も変える
	pFootL->ChangeSpeed(footSpeed);
	pFootR->ChangeSpeed(footSpeed);
}

//引数の状況から、状態を遷移する
void Enemy::ChangeState(int situation)
{
	D3DXVECTOR3 playerPos;

	//今の状況を取得
	switch (situation)
	{
		//状況が「プレイヤーを発見した」の場合
		case CharacterAI::FOUND_PLAYER:
			state_ = CHASE;
			ChangeSpeed();
			break;

		//状況が「プレイヤーを見失った」の場合
		case CharacterAI::LOST_SIGHT_PLAYER:
			state_ = SEARCH;
			isMove_ = true;
			isAccept_ = false;
			waitCount_ = 0;
			task_ = 0;
			ChangeSpeed();

			//プレイヤーを見失ったときの道の行き止まりの地点を目標とする
			CallGetBranchList();
			break;

		//状況が「プレイヤーを見つけられなかった」の場合
		case CharacterAI::NOT_FOUND_PLAYER:
			state_ = PATROL;
			ChangeSpeed();
			break;
	}
}

//指定された位置の部品タイプを教えてもらい、呼び出し元に返す
int Enemy::GetStage(int z, int x)
{
	return pNaviAI_->GetStage(z, x);
}

//プレイヤーのモデル番号を教えてもらう
int Enemy::GetPlayerModel()
{
	return pNaviAI_->GetPlayerModel();
}

//壁のモデル番号リストを教えてもらう
std::vector<int>* Enemy::GetWallModel()
{
	return pNaviAI_->GetWallModel();
}

//ナビゲーションAIの分岐を数える関数を呼ぶ
int Enemy::CallCountBranch(int posZ, int posX, int dir, int branchCnt)
{
	//一旦分岐地点一覧の中身を削除する
	pNaviAI_->ClearBranchList();

	//分岐地点を数える
	return pNaviAI_->CountBranch(posZ, posX, dir, branchCnt);
}

//ナビゲーションAIの、プレイヤーが通った道のコストを下げる関数を呼ぶ
void Enemy::CallCostDown()
{
	pNaviAI_->CostDown();
}

//ナビゲーションAIの、分岐のある道のリストのポインタを返す関数を呼ぶ
void Enemy::CallGetBranchList()
{
	pCharaAI_->SetBranchPosList(pNaviAI_->GetBranchPosList());
}
