#include "TitleScene.h"
#include "Engine/Image.h"

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"), hPict_(-1)
{
}

//初期化
void TitleScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Data/Picts/Title.jpg");
	assert(hPict_ >= 0);
}

//更新
void TitleScene::Update()
{
	//エンターキーが押されていたら
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//シーンを変える
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void TitleScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void TitleScene::Release()
{
}