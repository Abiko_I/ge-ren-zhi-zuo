#pragma once
#include "Engine/IGameObject.h"

class BoxCollider;

//壁を管理するクラス
class Wall : public IGameObject
{
private:

	//モデル番号
	int hModel_;

	//当たり判定用のコライダーを入れる
	BoxCollider* collision_;

public:
	//コンストラクタ
	Wall(IGameObject* parent);

	//デストラクタ
	~Wall();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//自分の位置を決めてもらう
	//引数：位置
	//戻り値：なし
	void SetPos(D3DXVECTOR3 pos) { position_ = pos; }

	//コライダーリストを渡す
	//引数：なし
	//戻り値：コライダーのリスト
	Collider* GetCollider();
};