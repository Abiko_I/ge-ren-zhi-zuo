#include "Player.h"
#include "CameraTarget.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"
#include "Stage.h"
#include "Wall.h"

Player::Player(IGameObject * parent) : Character(parent, "Player"), MOVE_SPEED_(0.07f),
	ROTATE_SPEED_(2.0f),RADIUS_(0.35f), START_POS_(1.0f, 0.0f, 1.0f), hModel_(-1)
{
}


Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Data/Models/Player_Cen.fbx");
	assert(hModel_ >= 0);

	//左下
	position_ = START_POS_;
	position_ += D3DXVECTOR3(0.0f, RADIUS_, 0.0f);

	rotate_.y = 0.0f;

	//最初は前を向かせる
	direction_[0] = FORWARD;
	direction_[1] = NONE;

	//球体コライダーをつける
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), RADIUS_);
	AddCollider(collision);

	//カメラが基準とするオブジェクトを子供として生成
	CreateGameObject<CameraTarget>(this);
}

//更新
void Player::Update()
{
	//移動ベクトルの初期化
	addMove_ = D3DXVECTOR3(0, 0, 0);

	//前進
	if (Input::IsKey(DIK_W))
	{
		direction_[F_B] = FORWARD;

		//移動する値を渡す
		Move(0, MOVE_SPEED_);
	}
	//後退
	else if (Input::IsKey(DIK_S))
	{
		direction_[F_B] = BACKWARD;

		//移動する値を渡す
		Move(0, (MOVE_SPEED_ * -1));
	}
	else
	{
		//W,Sどちらも押されていない時
		direction_[F_B] = NONE;
	}

	//右移動
	if (Input::IsKey(DIK_D))
	{
		direction_[R_L] = RIGHT;

		//移動する値を渡す
		Move(MOVE_SPEED_, 0);
	}
	//左移動
	else if (Input::IsKey(DIK_A))
	{
		direction_[R_L] = LEFT;

		//移動する値を渡す
		Move((MOVE_SPEED_ * -1), 0);
	}
	else
	{
		//A,Dどちらも押されていない時
		direction_[R_L] = NONE;
	}

	//回転
	Rotate();

	//移動
	position_ += addMove_;
}

//描画
void Player::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
}

//何かに当たった時の処理
void Player::OnCollision(IGameObject * pTarget)
{
	//壁と当たった時
	if (pTarget->GetName() == "Wall")
	{
		PositionAdjustment(pTarget, RADIUS_);
	}

	if (pTarget->GetName() == "Crystal")
	{
		pTarget->KillMe();

		crystalCnt_--;

		if (crystalCnt_ == 0)
		{
			//クリスタルを全て入手したらクリアシーンへ遷移
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_CLEAR);
		}
	}
}