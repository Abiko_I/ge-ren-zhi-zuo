#include "CameraTarget.h"
#include "Engine/Model.h"
#include "Engine/Camera.h"
#include "Engine/SphereCollider.h"
#include "Stage.h"

CameraTarget::CameraTarget(IGameObject * parent)
	:IGameObject(parent, "CameraTarget"), hModel_(-1), hitHModel_(-1),
	MOVE_SPEED_(0.2f), ROTATE_SPEED_(2.0f), move_(0, 0, 0)
{
}


CameraTarget::~CameraTarget()
{
}

//初期化
void CameraTarget::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Data/Models/Player_Cen.fbx");
	assert(hModel_ >= 0);

	pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0.0f, 2.0f, -1.0f));	//カメラの位置（真上から）
	pCamera->SetTarget(position_);							//カメラの焦点
}

//更新
void CameraTarget::Update()
{
	//自機（親）と逆に回転して相殺する
	rotate_.y = -pParent_->GetRotate().y;

	//カメラ上
	if (Input::IsKey(DIK_UP))
	{
		//カメラの位置（真上から）
		pCamera->SetPosition(D3DXVECTOR3(0.0f, 10.0f, -0.01f));
	}
	//カメラ上斜め後ろ
	else if (Input::IsKey(DIK_SPACE))
	{
		//カメラの位置（上斜め後ろ）
		pCamera->SetPosition(D3DXVECTOR3(0.0f, 2.0f, -1.0f));
	}
}

//描画
void CameraTarget::Draw()
{
	//自機と同じ位置に配置する（描画はしない）
	Model::SetMatrix(hModel_, worldMatrix_);
}

//開放
void CameraTarget::Release()
{
}

//どれくらい移動するか
void CameraTarget::Move(float x, float z)
{
	//奥に移動するベクトルを取得
	move_ = D3DXVECTOR3(x, 0, z);

	//移動
	position_ += move_;
}