#pragma once
#include "Engine/IGameObject.h"

//床を管理するクラス
class Floor : public IGameObject
{
private:

	//モデル番号
	int hModel_;

public:
	//コンストラクタ
	Floor(IGameObject* parent);

	//デストラクタ
	~Floor();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//自分の位置を決めてもらう
	//引数：位置
	//戻り値：なし
	void SetPos(D3DXVECTOR3 pos) { position_ = pos; }
};