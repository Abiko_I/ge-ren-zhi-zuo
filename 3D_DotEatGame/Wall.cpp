#include "Wall.h"
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"

//コンストラクタ
Wall::Wall(IGameObject * parent)
    :IGameObject(parent, "Wall"), hModel_(-1), collision_(nullptr)
{
}

//デストラクタ
Wall::~Wall()
{
}

//初期化
void Wall::Initialize()
{
	hModel_ = Model::Load("Data/Models/Wall_Cen.fbx");
	assert(hModel_ >= 0);

	collision_ = new BoxCollider(
		D3DXVECTOR3(position_.x, position_.y, position_.z),
		D3DXVECTOR3(1.0f, 1.0f, 1.0f));

	AddCollider(collision_);
}

//更新
void Wall::Update()
{
}

//描画
void Wall::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Wall::Release()
{
}

Collider* Wall::GetCollider()
{
	/*D3DXVECTOR3 pos;

	pos.x = x;
	pos.y = 0.0f;
	pos.z = z;

	for (auto it = colliderList_.begin(); it != colliderList_.end(); it++)
	{
		if ((*it)->GetCenter() == pos)
		{
			return *it;
		}
	}*/

	return collision_;
}

//D3DXVECTOR3 Wall::GetPos(int x, int z)
//{
//	//返す位置
//	int posX = x * 10;
//	int posZ = z;
//
//	for (auto it = colliderList_.begin(); it != colliderList_.end(); it++)
//	{
//		((*it)->GetOwner()).;
//	}
//
//	return D3DXVECTOR3();
//}
