#include "Audio.h"
#include "Global.h"

namespace Audio
{
	IXACT3Engine* pXactEngine;
	IXACT3WaveBank* pWaveBank;
	IXACT3SoundBank*pSoundBank;

	void* soundBankData;

	void Initialize()
	{
		CoInitializeEx(NULL, COINIT_MULTITHREADED);

		XACT3CreateEngine(0, &pXactEngine);
		XACT_RUNTIME_PARAMETERS xactParam = { 0 };
		xactParam.lookAheadTime = XACT_ENGINE_LOOKAHEAD_DEFAULT;
		pXactEngine->Initialize(&xactParam);
	}

	void Load()
	{
		//ファイルを開く
		HANDLE hFile = CreateFile("Data/Sounds/Wave Bank.xwb", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

		//開いたファイルのサイズ
		DWORD fileSize = GetFileSize(hFile, NULL);

		//ファイルをマッピングする
		HANDLE hMapFile = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, fileSize, NULL);
		void* mapWaveBank;
		mapWaveBank = MapViewOfFile(hMapFile, FILE_MAP_READ, 0, 0, 0);

		//WAVEバンク作成
		pXactEngine->CreateInMemoryWaveBank(mapWaveBank, fileSize, 0, 0, &pWaveBank);

		//ファイルを閉じる
		CloseHandle(hMapFile);
		CloseHandle(hFile);

		//ファイルを開く
		hFile = CreateFile("Data/Sounds/Sound Bank.xsb", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

		//ファイルのサイズを調べる
		fileSize = GetFileSize(hFile, NULL);

		//ファイルの中身をいったん配列に入れる
		soundBankData = new BYTE[fileSize];
		DWORD byteRead;
		ReadFile(hFile, soundBankData, fileSize, &byteRead, NULL);

		//サウンドバンク作成
		pXactEngine->CreateSoundBank(soundBankData, fileSize, 0, 0, &pSoundBank);

		//ファイルを閉じる
		CloseHandle(hFile);
	}

	void Play(char* pCueName)
	{
		XACTINDEX cueIndex = pSoundBank->GetCueIndex(pCueName);
		pSoundBank->Play(cueIndex, 0, 0, NULL);
	}

	void Release()
	{
		if (pSoundBank != nullptr)
		{
			pSoundBank->Destroy();
		}
		if (pWaveBank != nullptr)
		{
			pWaveBank->Destroy();
		}
		pXactEngine->ShutDown();
		CoUninitialize();
		SAFE_DELETE(soundBankData);
	}
};

