#pragma once
#include <xact3.h> 

namespace Audio
{
	void Initialize();
	void Load();
	void Play(char* pCueName);
	void Release();
}