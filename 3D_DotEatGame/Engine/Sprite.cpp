#include "Sprite.h"
#include "Direct3D.h"

//この書き方が早い（宣言と初期化を一緒にできる）
Sprite::Sprite() : pSprite_(nullptr), pTexture_(nullptr)
{
}


Sprite::~Sprite()
{
	pTexture_->Release();
	pSprite_->Release();
}

//画像の読み込み
void Sprite::Load(const char *file)
{
	//スプライト作成
	D3DXCreateSprite(Direct3D::pDevice, &pSprite_);

	//エラー処理
	assert(pSprite_ != nullptr);

	//テクスチャ作成
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, file,
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);

	//エラー処理
	assert(pTexture_ != nullptr);

}

//画面の描画
void Sprite::Draw(D3DXMATRIX& matrix)
{
	//行列をセット
	pSprite_->SetTransform(&matrix);

	//ゲーム画面の描画
	pSprite_->Begin(0);
	pSprite_->Draw(pTexture_, nullptr, nullptr, nullptr, D3DXCOLOR(1, 1, 1, 1));
	pSprite_->End();
}
