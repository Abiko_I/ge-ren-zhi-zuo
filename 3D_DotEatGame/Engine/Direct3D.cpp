#include "Direct3D.h"

LPDIRECT3D9	Direct3D::pD3d = nullptr;			//初期化
LPDIRECT3DDEVICE9 Direct3D::pDevice = nullptr;	//初期化
bool Direct3D::isDrawCollision_ = true;					//初期化
bool Direct3D::isLighting_ = true;					//初期化

void Direct3D::Initialize(HWND hWnd)
{
	//Direct3Dオブジェクトの作成
	pD3d = Direct3DCreate9(D3D_SDK_VERSION);

	//エラー処理
	assert(pD3d != nullptr);

	//DIRECT3Dデバイスオブジェクトの作成
	D3DPRESENT_PARAMETERS d3dpp;	                //専用の構造体
	ZeroMemory(&d3dpp, sizeof(d3dpp));	          //中身を全部0にする
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.BackBufferWidth = g.screenWidth;
	d3dpp.BackBufferHeight = g.screenHeight;
	pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice);

	//エラー処理
	assert(pDevice != nullptr);

	//アルファブレンド（半透明表示）
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);


	if (isLighting_)
	{
		//ライティング　↓記述無しだとTRUE
		pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);		//TRUE → 明暗有り、FALSE → 明暗無し・ポリゴンやテクスチャの色そのまま表示

		//ライトを設置
		D3DLIGHT9 lightState;
		ZeroMemory(&lightState, sizeof(lightState));		//構造体の中身を０にする　ZeroMemory
		lightState.Type = D3DLIGHT_DIRECTIONAL;				//平行高原
		lightState.Direction = D3DXVECTOR3(1, -1, 1);
		lightState.Diffuse.r = 1.0f;						//Diffuse（拡散反射光）
		lightState.Diffuse.g = 1.0f;
		lightState.Diffuse.b = 1.0f;
		lightState.Ambient.r = 1.0f;						//環境光追加
		lightState.Ambient.g = 1.0f;
		lightState.Ambient.b = 1.0f;
		pDevice->SetLight(0, &lightState);					//第一引数はライトの番号
		pDevice->LightEnable(0, TRUE);						//ライトのスイッチON　〇〇Enable→やるかやらないか

	}
	else
	{
		pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	}


	//カメラ
	//ビュー、プロジェクション行列
	D3DXMATRIX view, proj;

	//ビュー行列を入れる　LookAtLH(入れる行列、視点、焦点、上方向＝カメラの回転(デフォルトは0, 1, 0))
	D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));
	//ビュー行列として &view を使ってという意味
	pDevice->SetTransform(D3DTS_VIEW, &view);

	//プロジェクション行列を入れる　Perspective（遠近感をつける
	//PerspectiveFovLH（入れる行列、視野角（広いと遠近感が強くなる）、アスペクト比（縦横の比率）、カメラからの描画開始位置（ニアクリッピング面）、描画終了位置（ファークリッピング面））
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 0.5f, 1000.0f);
	//プロジェクション行列として &proj を使ってという意味
	pDevice->SetTransform(D3DTS_PROJECTION, &proj);

	//コリジョン表示するか（デバッグ時には表示する）
	//isDrawCollision_ = GetPrivateProfileInt("DEBUG", "ViewCollider", 0, ".\\Data\\setup.ini") != 0;
	//isDrawCollision_ = true;
}

void Direct3D::BeginDraw()
{
	//画面をクリア															  R  G  B
	pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 255), 1.0f, 0);

	//描画開始
	pDevice->BeginScene();
}

void Direct3D::EndDraw()
{
	//描画終了
	pDevice->EndScene();

	//スワップ（交換という意味）
	pDevice->Present(NULL, NULL, NULL, NULL);
}

void Direct3D::Release()
{
	//開放処理（作った順と逆に削除）
	SAFE_RELEASE(pDevice);
	SAFE_RELEASE(pD3d);
}
