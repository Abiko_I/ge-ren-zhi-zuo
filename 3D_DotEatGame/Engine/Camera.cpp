#include "Camera.h"
#include "Direct3D.h"
#include "../Player.h"


//コンストラクタ
Camera::Camera(IGameObject * parent)
	:IGameObject(parent, "Camera"), target_ (0, 0, 0)/*, player_(nullptr)*/
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化
void Camera::Initialize()
{
	//projection行列を入れる変数
	D3DXMATRIX proj;
	//変数、視野角、アスペクト比、ニアクリッピング面、ファークリッピング面
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 0.5f, 200.0f);
	//プロジェクション行列として proj を使ってという意味
	Direct3D::pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

//更新
void Camera::Update()
{
	//行列変形
	//this->Transform();
	Transform();

	/*D3DXVECTOR3 targetPos = player_->GetPosition();

	target_ = targetPos;

	position_.x = targetPos.x;
	position_.y = targetPos.y + 1.0f;
	position_.z = targetPos.z - 5.0f;*/

	//position_ = D3DXVECTOR3(targetPos.x, targetPos.y, targetPos.z);

	//view行列
	D3DXVECTOR3 worldPosition, worldTarget;

	//worldPosition　に　position_　と　worldMatrix_　を掛け合わせた行列を入れる
	D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
	//				 ↑コード：コーディネートの略

	//worldTarget　に　target_　と　worldMatrix_　を掛け合わせた行列を入れる
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);

	//view行列を入れる変数
	D3DXMATRIX view;
	//変数、視点、焦点、上方向
	D3DXMatrixLookAtLH(&view, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	//D3DXMatrixLookAtLH(&view, &position_, &target_, &D3DXVECTOR3(0, 1, 0));
	//ビュー行列として view を使ってという意味
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view);
}

//描画
void Camera::Draw()
{
}

//開放
void Camera::Release()
{
}

//void Camera::SetPlayer(Player* obj)
//{
//	player_ = obj;
//}

//void Camera::Transform()
//{
//	D3DXMATRIX matT, matRX, matRY, matRZ, matS;
//
//	//移動
//	D3DXMatrixTranslation(&matT, position_.x, position_.y, position_.z);
//
//	//回転 rotate_.〇を渡してラジアンから度に直す
//	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate_.x));	//X軸を回転させる行列
//	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate_.y));	//Y軸を回転させる行列
//	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate_.z));	//Z軸を回転させる行列
//
//	//拡大・縮小
//	D3DXMatrixScaling(&matS, scale_.x, scale_.y, scale_.z);
//
//	//行列を掛け合わせる
//	localMatrix_ = (matS * matRX * matRY * matRZ * matT);
//
//	//ローカルマトリックスに親のワ−ルドマトリックスをかけて、自分のワールドマトリックスに入れる
//	//ワールド行列
//	/*if (pParent_ == nullptr)
//	{
//		worldMatrix_ = localMatrix_;
//	}
//	else
//	{*/
//	//}
//
//	D3DXMATRIX parentMatrix = player_->GetMatrix();
//	D3DXVECTOR3 parentRotate = player_->GetRotate();
//	D3DXVECTOR3 r;
//
//	r.x = -parentRotate.x;
//	r.y = -parentRotate.y;
//	r.z = -parentRotate.z;
//
//	D3DXMATRIX matX, matY, matZ;
//	D3DXMatrixRotationX(&matX, D3DXToRadian(r.x));
//	D3DXMatrixRotationX(&matY, D3DXToRadian(r.y));
//	D3DXMatrixRotationX(&matZ, D3DXToRadian(r.z));
//
//	parentMatrix *= (matX * matY * matZ);
//
//	worldMatrix_ = localMatrix_ * parentMatrix;
//}
