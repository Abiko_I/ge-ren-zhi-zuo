#include "SceneManager.h"
#include "../TitleScene.h"
#include "../PlayScene.h"
#include "../ClearScene.h"
#include "../GameoverScene.h"

//static変数達の初期化
SCENE_ID SceneManager::currentSceneID_ = SCENE_ID_TITLE;
SCENE_ID SceneManager::nextSceneID_ = SCENE_ID_PLAY;
IGameObject* SceneManager::pCurrentScene_ = nullptr;

//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	:IGameObject(parent, "SceneManager")
{
	currentSceneID_ = SCENE_ID_TITLE;
	nextSceneID_ = SCENE_ID_TITLE;
}

//デストラクタ
SceneManager::~SceneManager()
{
}

//初期化
void SceneManager::Initialize()
{
	//プレイシーンを生成・親は自分
	pCurrentScene_ = CreateGameObject<TitleScene>(this);
}

//更新
void SceneManager::Update()
{
	if (currentSceneID_ != nextSceneID_)
	{
		//シーンに今のシーンの子供の位置を入れる
		auto scene = childList_.begin();
		//今の子供のリリースサブを呼ぶ
		(*scene)->ReleaseSub();

		//開放
		SAFE_DELETE(*scene);
		//シーンの箱そのものを削除する
		childList_.clear();

		switch (nextSceneID_)
		{
			case SCENE_ID_TITLE:
				//タイトルシーンを生成・親は自分
				pCurrentScene_ = CreateGameObject<TitleScene>(this);
				break;

			case SCENE_ID_PLAY:
				//プレイシーンを生成・親は自分
				pCurrentScene_ = CreateGameObject<PlayScene>(this);
				break;

			case SCENE_ID_CLEAR:
				//クリアシーンを生成・親は自分
				pCurrentScene_ = CreateGameObject<ClearScene>(this);
				break;

			case SCENE_ID_GAMEOVER:
				//ゲームオーバーシーンを生成・親は自分
				pCurrentScene_ = CreateGameObject<GameoverScene>(this);
				break;
		}

		//シーンを更新
		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

//シーン切り替え
void SceneManager::ChangeScene(SCENE_ID next)
{
	//次のシーンを入れる
	nextSceneID_ = next;
}
