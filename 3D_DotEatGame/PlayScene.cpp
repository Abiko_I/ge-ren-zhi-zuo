#include "PlayScene.h"
#include "Stage.h"
#include "Enemy.h"
#include "Player.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{
	pPlayer_ = CreateGameObject<Player>(this);
	pStage_ = CreateGameObject<Stage>(this);

	pPlayer_->SetCrystalCnt(pStage_->GetCrystalCnt());

	pEnemy_ = CreateGameObject<Enemy>(this);

	//ステージのポインタを敵に渡す
	pEnemy_->SetStage(pStage_);
	pEnemy_->SetPlayer(pPlayer_);
}

//更新
void PlayScene::Update()
{
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}