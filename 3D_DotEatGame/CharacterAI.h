#pragma once
#include "AI.h"
#include <vector>

//キャラクターAIを管理するクラス
class CharacterAI : public AI
{
private:
	//探索を行う間隔（基準）
	const int SEARCH_INTERVAL_;

	//敵の移動速度
	float enemySpeed_;

	//カウントにおいての探索するかどうか
	bool isOverTime_;

	//目標地点
	CharaPos targetPos_;

	//自分が通れる場所を記録する
	std::list<PosInfo> pos_;

	//目標地点を指す
	std::list<PosInfo>::iterator it_;

	//次の目標地点を指す
	std::list<PosInfo>::iterator nextIt_;

public:
	//コンストラクタ
	CharacterAI(IGameObject* parent);

	//デストラクタ
	~CharacterAI();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//敵の移動速度をもらう
	//引数：敵の移動速度
	//戻り値：なし
	void SetSpeed(float speed) { enemySpeed_ = speed; }

	//位置リストのポインタを渡す
	//引数：なし
	//戻り値：位置リストのポインタ
	std::list<PosInfo>* GetPosList() { return &pos_; }

	//目標地点を返す
	//引数：なし
	//戻り値：目標地点
	CharaPos GetTarget() { return targetPos_; }

	//向きを変える
	//引数：なし
	//戻り値：なし
	void ChangeDirection();

	//目標地点に到着したかどうかを判断し、目標地点に移動するか、更新する
	//引数：自分の位置（zかx）と目標地点の位置（zかx）が格納された配列、前後の方向、左右の方向、
	//		左右の移動量、前後の移動量
	//戻り値：なし
	void JudgeNextMove(float * pos, DIRECTION dirFB, DIRECTION dirRL, float moveX, float moveZ);

	//イテレーターを初期化する
	//引数：なし
	//戻り値：なし
	void InitIterator();
};