#pragma once
#include <list>
#include <vector>
#include "Engine/IGameObject.h"
#include "Character.h"

class Stage;
class Player;
class CharacterAI;
class NavigationAI;

//敵を管理するクラス
class Enemy : public Character
{
private:
	//ステージの種類
	enum
	{
		TYPE_WALL = 0,	//壁
		TYPE_FLOOR,		//床
		TYPE_PLAYER,	//プレイヤーの初期位置
		TYPE_ENEMY,		//敵の初期位置
		TYPE_MAX
	};

	//敵の状態
	enum STATE
	{
		DISCOVERED,		//プレイヤーを発見した
		UNDISCOVERED	//プレイヤーをまだ発見していない
	};
	
	float MOVE_SPEED_;				//敵の移動速度
	const float ROTATE_SPEED_;		//敵の旋回速度	
	const D3DXVECTOR3 START_POS_;	//敵の初期位置
	const float RADIUS_;			//敵の半径

	//ステージの情報
	struct StageInfo
	{
		int type_;		//ステージ上に生成されたオブジェクトの種類
		int cost_;		//その位置のコスト
		bool moveFlag_;	//その位置に進むかどうか
	};

	//ステージの部品の情報
	struct PosInfo
	{
		D3DXVECTOR3 pos;	//その部品の位置
		DIRECTION dir;		//自分から見てどの方向にあるのか
	};

	//キャラクターの位置（引数で渡す時に使う）
	struct CharaPos
	{
		int z_;
		int x_;
	};

	//探索用（ステージ上での方向）
	int searchPos_[4][2] = {
		{-1, 0},	//左
		{0, -1},	//下
		{0, 1},		//上
		{1, 0}		//左
	};

	//位置、歩数、推定値、実歩数をまとめたもの
	struct Info
	{
		int z_;			//縦
		int x_;			//横
		int dist_;		//歩数
		int est_;		//推定値
		int realDist_;	//推定歩数
		int prevZ_;		//自分の一つ前のy
		int prevX_;		//自分の一つ前のx
		DIRECTION dir_;	//方向
	};

	//モデル番号
	int hModel_;

	//探索関数を呼ぶかどうか
	bool isCallSearch_;

	//敵の状態
	STATE state_;

	CharacterAI* pCharaAI_;

	NavigationAI* pNaviAI_;

public:
	//コンストラクタ
	Enemy(IGameObject* parent);

	//デストラクタ
	~Enemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった時の処理
	//引数：当たった相手
	//戻り値：なし
	void OnCollision(IGameObject* pTarget) override;

	//ステージのポインタをもらう
	//引数：ステージのポインタ
	//戻り値：なし
	void SetStage(Stage* pStage);

	//プレイヤーのポインタをもらう
	//引数：プレイヤーのオブジェクトのポインタ
	//戻り値：なし
	void SetPlayer(Player* pPlayer);

	//向くべき方向をもらう
	//引数：前後左右の向きが格納された配列のポインタ
	//戻り値：なし
	void SetDirection(int* dirFB);

	//初期位置を教えてもらい、決定する
	//引数：xとzの位置
	//戻り値：なし
	void SetStartPos(float x, float z) { position_ = D3DXVECTOR3(x, RADIUS_, z); }

	//探索関数を呼ぶかを教えてもらう
	//引数：呼ぶかどうかの真偽
	//戻り値：なし
	void SetCallFlag(bool flag) { isCallSearch_ = flag; }

	//引数で指定された位置の部品タイプをナビゲーションAIから教えてもらい、呼び出し元に返す
	//引数：zの位置、xの位置
	//戻り値：指定された位置の部品タイプ
	int GetStage(int z, int x);

	//自分の状態を教える
	//引数：なし
	//戻り値：自分の状態
	STATE GetState() { return state_; }
};