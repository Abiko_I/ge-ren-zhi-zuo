#pragma once
#include <vector>
#include "Engine/Global.h"

class Player;
class Stage;
class Enemy;

//プレイシーンを管理するクラス IGameObjectを継承
class PlayScene : public IGameObject
{
private:
	//プレイヤーのポインタ
	Player* pPlayer_;

	//ステージのポインタ
	Stage* pStage_;

	//敵のポインタ
	Enemy* pEnemy_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};