#include <time.h>
#include "CharacterAI.h"
#include "NavigationAI.h"
#include "Enemy.h"

//コンストラクタ
CharacterAI::CharacterAI(IGameObject * parent)
	:AI(parent, "CharacterAI"), SEARCH_INTERVAL_(180), enemySpeed_(0.0f),
	isOverTime_(true)
{
}

//デストラクタ
CharacterAI::~CharacterAI()
{
}

//初期化
void CharacterAI::Initialize()
{
	//最初は探索する
	isSearch_ = true;
}

//更新
void CharacterAI::Update()
{
	static int time = SEARCH_INTERVAL_;

	//初回と、3秒毎に探索する
	if((time % SEARCH_INTERVAL_) == 0)
	{
		isOverTime_ = true;
	}

	//探索する基準のカウントになっているか、次の目標地点がリストの最後なら
	if (isOverTime_ || (nextIt_ == pos_.end()))
	{
		if (isSearch_)
		{
			//探索するよう伝える
			((Enemy*)pParent_)->SetCallFlag(true);

			isOverTime_ = false;
		}

		//タイムを初期化
		time = 0;
	}

	time++;
}

//描画
void CharacterAI::Draw()
{
}

//開放
void CharacterAI::Release()
{
}

//向きを変える
void CharacterAI::ChangeDirection()
{
	//敵と目標地点のxかzの位置を格納する配列
	float pos[2] = { 0, 0 };

	enemyPos_ = pParent_->GetPosition();

	//目標地点の方向を見て次の行動を決める
	switch (it_->dir)
	{
		case FORWARD:
			pos[0] = enemyPos_.z;
			pos[1] = (*it_).pos.z;

			JudgeNextMove(pos, FORWARD, NONE, 0, enemySpeed_);
			break;

		case BACKWARD:
			pos[0] = (*it_).pos.z;
			pos[1] = enemyPos_.z;

			JudgeNextMove(pos, BACKWARD, NONE, 0, enemySpeed_ * -1);
			break;

		case RIGHT:
			pos[0] = enemyPos_.x;
			pos[1] = (*it_).pos.x;

			JudgeNextMove(pos, NONE, RIGHT, enemySpeed_, 0);
			break;

		case LEFT:
			pos[0] = (*it_).pos.x;
			pos[1] = enemyPos_.x;

			JudgeNextMove(pos, NONE, LEFT, enemySpeed_ * -1, 0);
			break;
	}
}

//目標地点に移動するか、目標地点を更新するか
void CharacterAI::JudgeNextMove(float* pos, DIRECTION dirFB, DIRECTION dirRL, float moveX, float moveZ)
{
	float firstVal = pos[0];
	float secondVal = pos[1];

	//今いる場所が目標地点と重なるか、超えたら
	if (firstVal > secondVal)
	{
		std::list<PosInfo>::iterator it = it_;

		//目標地点を更新
		it_++;
		nextIt_++;
		pos_.erase(it);

		//探索する
		isSearch_ = true;
	}
	//まだ目標地点にたどりついていないなら
	else
	{
		int dir[2];
		dir[0] = dirFB;
		dir[1] = dirRL;

		//向きを教える
		((Enemy*)pParent_)->SetDirection(dir);

		//移動させる
		((Enemy*)pParent_)->Move(moveX, moveZ);

		//まだ探索しない
		isSearch_ = false;
	}
}

//イテレーターを初期化する
void CharacterAI::InitIterator()
{
	//探索で得た情報の一番最初の場所を取得
	it_ = pos_.begin();
	nextIt_ = pos_.begin();
	nextIt_++;
}