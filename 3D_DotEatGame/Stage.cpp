#include "Stage.h"
#include "Floor.h"
#include "Wall.h"
#include "Crystal.h"
#include "Engine/Model.h"
#include "Engine/CsvReader.h"
#include "Engine/Camera.h"


//コンストラクタ
Stage::Stage(IGameObject * parent) : IGameObject(parent, "Stage"), crystalCount_(0)
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//csv形式でステージを読み込む
	CsvReader csv;
	csv.Load("Data/Stage.csv");

	for (int z = 0; z < HEIGHT; z++)
	{
		for (int x = 0; x < WIDTH; x++)
		{
			//csvデータから指定した場所のデータを取り出す
			stage_[(HEIGHT - 1) - z][x] = csv.GetValue(x, z);

			float xPos = x;
			float zPos = (HEIGHT - 1) - z;
			float yPos = 0.0f;


			//if (stage_[(HEIGHT - 1) - z][x] == TYPE_FLOOR ||
			//	stage_[(HEIGHT - 1) - z][x] == TYPE_PLAYER ||
			//	stage_[(HEIGHT - 1) - z][x] == TYPE_ENEMY)
			//{
			//	yPos = -0.1f;

			//	//部品ごとの位置を記録する
			//	partsPos_[x][z] = D3DXVECTOR3(xPos, yPos + 0.1f, zPos);
			//	Floor* pFloor = CreateGameObject<Floor>(this);
			//	pFloor->SetPos(partsPos_[x][z]);
			//}
			//else
			//{
			//	partsPos_[x][z] = D3DXVECTOR3(xPos, yPos + 0.5f, zPos);
			//	Wall* pWall = CreateGameObject<Wall>(this);
			//	pWall->SetPos(partsPos_[x][z]);
			//}

			//壁の時
			if (stage_[(HEIGHT - 1) - z][x] == TYPE_WALL)
			{
				//部品ごとの位置を記録する
				partsPos_[x][z] = D3DXVECTOR3(xPos, yPos + 0.5f, zPos);

				//壁を生成
				Wall* pWall = CreateGameObject<Wall>(this);
				pWall->SetPos(partsPos_[x][z]);
			}
			//壁以外の時
			else
			{
				partsPos_[x][z] = D3DXVECTOR3(xPos, yPos + 0.4, zPos);

				//クリスタルを生成
				Crystal* pCrystal = CreateGameObject<Crystal>(this);
				pCrystal->SetPosition(partsPos_[x][z]);

				crystalCount_++;
			}

			//床を生成
			Floor* pFloor = CreateGameObject<Floor>(this);
			pFloor->SetPos(D3DXVECTOR3(xPos, yPos - 0.1f, zPos));
		}
	}
}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
}

//開放
void Stage::Release()
{
}

D3DXVECTOR3 Stage::GetPartsPos(float x, float z, int type)
{
	int posX, posZ;

	if ((x - (int)x) >= 0.5f)
	{
		posX = (int)x + 1;
	}
	else
	{
		posX = (int)x;
	}

	if ((z - (int)z) >= 0.5f)
	{
		posZ = (int)z + 1;
	}
	else
	{
		posZ = (int)z;
	}

	//１マス分の範囲を求める
	/*int upX = x += 1.0f;
	int upZ = z += 1.0f;
	int downX = x -= 1.0f;
	int downZ = z -= 1.0f;*/

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		if ((*it)->GetName() == "Wall")
		{
			//位置を取得
			D3DXVECTOR3 partsPos = ((Wall*)(*it))->GetPosition();

			//Collider* col = ((Wall*)(*it))->GetColliderList().begin();

			//D3DXVECTOR3 partsPos = ;

			//イテレーターで見ている位置が、範囲内の値なら
			/*if ((upX > partsPos.x) && (downX < partsPos.x) &&
				(upZ > partsPos.z) && (downZ < partsPos.z))*/
			if((posX == partsPos.x) && (posZ == partsPos.x))
			{
				int i = 0;
				//その位置を返す
				return D3DXVECTOR3(partsPos.x, partsPos.y, partsPos.z);
			}
		}
	}

	return D3DXVECTOR3(x, 0.0f, z);
}