#pragma once
#include "Engine/IGameObject.h"

//AI（基底となるAI）を管理するクラス
class AI : public IGameObject
{
protected:
	//キャラクターの向きを指す
	enum DIRECTION
	{
		FORWARD,	//前を向いている
		RIGHT,		//右を向いている
		LEFT,		//左を向いている
		BACKWARD,	//後ろを向いている
		NONE		//方向無し
	};

	//敵の状態
	enum STATE
	{
		DISCOVERED,		//プレイヤーを発見した
		UNDISCOVERED	//プレイヤーをまだ発見していない
	};

	//キャラクターの方向配列の添え字
	enum
	{
		F_B,	//上下
		R_L	//左右
	};

	//ステージの種類
	enum
	{
		TYPE_WALL = 0,	//壁
		TYPE_FLOOR,		//床
		TYPE_PLAYER,	//プレイヤーの初期位置
		TYPE_ENEMY,		//敵の初期位置
		TYPE_MAX
	};

	//ステージの部品の情報
	struct PosInfo
	{
		D3DXVECTOR3 pos;	//その部品の位置
		DIRECTION dir;		//自分から見てどの方向にあるのか
	};

	//キャラクターの位置（引数で渡す時に使う）
	struct CharaPos
	{
		int z_;
		int x_;
	};

	//敵の位置
	D3DXVECTOR3 enemyPos_;

	//探索するかどうか
	bool isSearch_;

public:
	//コンストラクタ
	AI(IGameObject * parent, std::string name);

	//デストラクタ
	~AI();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//敵の位置をもらう
	//引数：敵の位置
	//戻り値：なし
	void SetEnemyPos(D3DXVECTOR3 pos) { enemyPos_ = pos; }
};

