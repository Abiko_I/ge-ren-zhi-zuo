#pragma once
#include "Engine/IGameObject.h"

class BoxCollider;

//クリスタルを管理するクラス
class Crystal : public IGameObject
{
private:
	//モデル番号
	int hModel_;

	//当たり判定用のコライダーを入れる
	BoxCollider* collision_;

public:
	//コンストラクタ
	Crystal(IGameObject* parent);

	//デストラクタ
	~Crystal();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};