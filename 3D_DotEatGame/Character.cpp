#include "Character.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"
#include "Stage.h"
#include "Wall.h"

Character::Character(IGameObject * parent, std::string name) : IGameObject(parent, name),
	addMove_(0, 0, 0), addRotate_(0, 0, 0)
{
}


Character::~Character()
{
}

//初期化
void Character::Initialize()
{
}

//更新
void Character::Update()
{
}

//描画
void Character::Draw()
{
}

//開放
void Character::Release()
{
}

//移動
void Character::Move(float x, float z)
{
	addMove_ += D3DXVECTOR3(x, 0, z);
}

//自機の進む方向２つから、自機が向くべき角度を求める
void Character::Rotate()
{
	//右斜め上
	if ((direction_[F_B] == FORWARD) && (direction_[R_L] == RIGHT))
	{
		rotate_.y = 45.0f;
	}
	//左斜め上
	else if ((direction_[F_B] == FORWARD) && (direction_[R_L] == LEFT))
	{
		rotate_.y = -45.0f;
	}
	//右斜め下
	else if ((direction_[F_B] == BACKWARD) && (direction_[R_L] == RIGHT))
	{
		rotate_.y = 135.0f;
	}
	//左斜め下
	else if ((direction_[F_B] == BACKWARD) && (direction_[R_L] == LEFT))
	{
		rotate_.y = -135.0f;
	}
	//上
	else if ((direction_[F_B] == FORWARD) && (direction_[R_L] == NONE))
	{
		rotate_.y = 0.0f;
	}
	//下
	else if ((direction_[F_B] == BACKWARD) && (direction_[R_L] == NONE))
	{
		rotate_.y = 180.0f;
	}
	//右
	else if ((direction_[R_L] == RIGHT) && (direction_[F_B] == NONE))
	{
		rotate_.y = 90.0f;
	}
	//左
	else if ((direction_[R_L] == LEFT) && (direction_[F_B] == NONE))
	{
		rotate_.y = -90.0f;
	}
}

//めり込んだ時の位置調節
void Character::PositionAdjustment(IGameObject * pTarget, float radius)
{
	//相手の位置を取得
	D3DXVECTOR3 targetPos = ((Wall*)pTarget)->GetCollider()->GetCenter();

	//プレイヤーの位置調整用
	D3DXVECTOR3 CharacterPos = D3DXVECTOR3(position_.x, 0.5f, position_.z);

	//当たった相手から自分に向かうベクトルを求める
	D3DXVECTOR3 returnPos = CharacterPos - targetPos;

	//コライダーを取得
	Collider* col = ((Wall*)pTarget)->GetCollider();

	//求めたベクトルの、x、zの長い方向を見る
	if (fabs(returnPos.x) > fabs(returnPos.z))
	{
		//xの方向を取得
		D3DXVECTOR3 dir;

		if (returnPos.x >= 0)
		{
			//xの方向を取得（+）
			dir = ((BoxCollider*)col)->GetDirection(0);
		}
		else
		{
			//xの方向を取得（-）
			dir = -((BoxCollider*)col)->GetDirection(0);
		}

		//横幅の半分の長さを求める
		float len = ((BoxCollider*)col)->GetLength(0);

		//xのめり込んだ分を戻す
		position_.x = dir.x * (len + radius) + targetPos.x;
	}
	else
	{
		//xの方向を取得
		D3DXVECTOR3 dir;

		if (returnPos.z >= 0)
		{
			dir = ((BoxCollider*)col)->GetDirection(2);
		}
		else
		{
			//zの方向を取得
			dir = -((BoxCollider*)col)->GetDirection(2);
		}

		//横幅の半分の長さを求める
		float len = ((BoxCollider*)col)->GetLength(2);

		//xのめり込んだ分を戻す
		position_.z = dir.z * (len + radius) + targetPos.z;
	}
}