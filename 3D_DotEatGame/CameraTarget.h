#pragma once
#include "Engine/IGameObject.h"

class Camera;

//プレイヤーを管理するクラス
class CameraTarget : public IGameObject
{
private:
	const float MOVE_SPEED_;		//自機の移動速度
	const float ROTATE_SPEED_;	//自機の旋回速度

	//ステージとの当たり判定に使用する
	enum
	{
		TYPE_WALL = 0,	//壁
		TYPE_FLOOR,	//床
		TYPE_MAX
	};

	//モデル番号
	int hModel_;

	//レイを撃って当たった対象のモデル番号
	int hitHModel_;

	//どれくらい移動するのか
	D3DXVECTOR3 move_;

	Camera* pCamera;

public:
	//コンストラクタ
	CameraTarget(IGameObject* parent);

	//デストラクタ
	~CameraTarget();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//どれくらい移動するか
	//引数	：移動する値
	//戻り値：なし
	void Move(float x, float z);
};