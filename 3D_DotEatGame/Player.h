#pragma once
#include "Character.h"

//プレイヤーを管理するクラス
class Player : public Character
{
private:
	float MOVE_SPEED_;				//自機の移動速度
	const float ROTATE_SPEED_;		//自機の旋回速度	
	const D3DXVECTOR3 START_POS_;	//自機の初期位置
	const float RADIUS_;			//自機の半径

	//モデル番号
	int hModel_;

	//クリスタルの数
	int crystalCnt_;

public:
	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった時の処理
	//引数：当たった相手
	//戻り値：なし
	void OnCollision(IGameObject* pTarget) override;

	//クリスタルの数をもらう
	//引数：クリスタルの数
	//戻り値：なし
	void SetCrystalCnt(int cnt) { crystalCnt_ = cnt; }
};