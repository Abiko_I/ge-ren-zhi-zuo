#include "Enemy.h"
#include "Stage.h"
#include "Player.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"
#include "CharacterAI.h"
#include "NavigationAI.h"

//コンストラクタ
Enemy::Enemy(IGameObject * parent) : Character(parent, "Enemy"), MOVE_SPEED_(0.05f), ROTATE_SPEED_(2.0f),
	RADIUS_(0.35f), START_POS_(0.0f, 0.0f, 0.0f), hModel_(-1), isCallSearch_(true), state_(UNDISCOVERED)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	hModel_ = Model::Load("Data/Models/Enemy_Cen.fbx");
	assert(hModel_ >= 0);

	position_ = START_POS_;
	position_ += D3DXVECTOR3(0.0f, RADIUS_, 0.0f);

	rotate_.y = 0.0f;

	//球体コライダーをつける
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), RADIUS_);
	AddCollider(collision);

	//キャラクターAIを生成してポインタを所持
	pCharaAI_ = CreateGameObject<CharacterAI>(this);

	//速度を教える
	pCharaAI_->SetSpeed(MOVE_SPEED_);

	//ナビゲーションAIを生成してポインタを所持
	pNaviAI_ = CreateGameObject<NavigationAI>(this);
}

//更新
void Enemy::Update()
{
	//移動ベクトルの初期化
	addMove_ = D3DXVECTOR3(0, 0, 0);

	//毎フレーム重力の1/10ずつ下げる
	//position_.y -= gravity;

	//キャラクターAIが探索すると判断したなら
	if (isCallSearch_)
	{
		//キャラクターAIからナビゲーションAIに位置のリストを渡す
		pNaviAI_->SetPosList(pCharaAI_->GetPosList());

		//探索する
		pNaviAI_->SearchTargetPos();

		//キャラクターAIがもつ「現在の目標地点」と「次の目標地点」を初期化
		pCharaAI_->InitIterator();

		isCallSearch_ = false;
	}

	//向きを変える
	pCharaAI_->ChangeDirection();

	//回転
	Rotate();

	//移動
	position_ += addMove_;
}

//描画
void Enemy::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Enemy::Release()
{
}

//何かに当たった時の処理
void Enemy::OnCollision(IGameObject * pTarget)
{
	//壁と当たった時
	if (pTarget->GetName() == "Wall")
	{
		//めり込みを直す
		PositionAdjustment(pTarget, RADIUS_);
	}

	//プレイヤーと当たった時
	if (pTarget->GetName() == "Player")
	{
		pTarget->KillMe();
		KillMe();

		//プレイヤーを倒したらゲームオーバーシーンに遷移
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
	}
}

//ステージのポインタをもらう
void Enemy::SetStage(Stage * pStage)
{
	//ナビゲーションAIに渡す
	pNaviAI_->SetStage(pStage);

	//位置をAI達に渡す
	pCharaAI_->SetEnemyPos(position_);
	pNaviAI_->SetEnemyPos(position_);
}

//プレイヤーのポインタをもらう
void Enemy::SetPlayer(Player * pPlayer)
{
	//ナビゲーションAIに渡す
	pNaviAI_->SetPlayer(pPlayer);
}

//方向をもらう
void Enemy::SetDirection(int* dir)
{
	for (int i = 0; i < 2; i++)
	{
		//引数の方向から、向きを決める
		switch (dir[i])
		{
		case FORWARD:
			direction_[F_B] = FORWARD;
			direction_[R_L] = NONE;
			break;

		case BACKWARD:
			direction_[F_B] = BACKWARD;
			direction_[R_L] = NONE;
			break;

		case RIGHT:
			direction_[F_B] = NONE;
			direction_[R_L] = RIGHT;
			break;

		case LEFT:
			direction_[F_B] = NONE;
			direction_[R_L] = LEFT;
			break;
		}
	}
}

//指定された位置の部品タイプを教えてもらい、呼び出し元に返す
int Enemy::GetStage(int z, int x)
{
	return pNaviAI_->GetStage(z, x);
}