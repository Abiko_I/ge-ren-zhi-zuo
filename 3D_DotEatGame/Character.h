#pragma once
#include "Engine/IGameObject.h"

//キャラクターを管理するクラス
class Character : public IGameObject
{
protected:
	

	//ステージとの当たり判定に使用する
	enum
	{
		TYPE_WALL = 0,	//壁
		TYPE_FLOOR,		//床
		TYPE_MAX
	};

	//キャラクターの向きを指す
	enum DIRECTION
	{
		FORWARD,	//前を向いている
		RIGHT,		//右を向いている
		LEFT,		//左を向いている
		BACKWARD,	//後ろを向いている
		NONE		//方向無し
	};

	//キャラクターの方向配列の添え字
	enum
	{
		F_B,	//上下
		R_L	//左右
	};

	//どれくらい移動するのか
	D3DXVECTOR3 addMove_;

	//どれくらい移動するのか
	D3DXVECTOR3 addRotate_;

	//キャラクターの方向
	DIRECTION direction_[2];

public:
	//コンストラクタ
	Character(IGameObject* parent, std::string name);

	//デストラクタ
	~Character();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//void CollisionDetection();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//どれくらい移動するか
	//引数	：移動する値
	//戻り値：なし
	void Move(float x, float z);
	
	//どれくらい回転するか
	//引数：移動のベクトル
	//戻り値：なし
	void Rotate();

	//何かに当たった時にめり込んだ部分を戻す
	//引数：当たった相手、自分の半径
	//戻り値：なし
	void PositionAdjustment(IGameObject * pTarget, float radian);
};