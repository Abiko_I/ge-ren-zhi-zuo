#include "GameoverScene.h"
#include "Engine/Image.h"

//コンストラクタ
GameoverScene::GameoverScene(IGameObject * parent)
	: IGameObject(parent, "GameoverScene"), hPict_(-1)
{
}

//初期化
void GameoverScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Data/Picts/Gameover.jpg");
	assert(hPict_ >= 0);
}

//更新
void GameoverScene::Update()
{
}

//描画
void GameoverScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void GameoverScene::Release()
{
}