#pragma once
#include "AI.h"
#include <list>
#include <vector>

class Stage;
class Player;

//ナビゲーションAIを管理するクラス
class NavigationAI : public AI
{
private:
	//ステージの情報
	struct StageInfo
	{
		int type_;		//ステージ上に生成されたオブジェクトの種類
		int cost_;		//その位置のコスト
		bool moveFlag_;	//その位置に進むかどうか
	};

	//探索用（ステージ上での方向）
	int searchPos_[4][2] = {
		{-1, 0},	//左
		{0, -1},	//下
		{0, 1},		//上
		{1, 0}		//左
	};

	//位置、歩数、推定値、実歩数をまとめたもの
	struct Info
	{
		int z_;			//縦
		int x_;			//横
		int dist_;		//歩数
		int est_;		//推定値
		int realDist_;	//推定歩数
		int prevZ_;		//自分の一つ前のy
		int prevX_;		//自分の一つ前のx
		DIRECTION dir_;	//方向
	};

	//敵の位置
	D3DXVECTOR3 enemyPos_;

	Stage* pStage_;

	Player* pPlayer_;

	//ステージを格納
	StageInfo stage_[HEIGHT][WIDTH];

	//床だけの位置のリスト（壁が乗っている位置は該当しない）
	std::vector<CharaPos> floorList_;

	//計算中のマスたちを格納
	std::vector<Info> openList_;

	//計算済みのマスたちを格納
	std::vector<Info> closeList_;

	//自分が通れる場所を記録する
	std::list<PosInfo>* pos_;

	//キャラクターの位置
	CharaPos pPos_;

public:
	//コンストラクタ
	NavigationAI(IGameObject* parent);

	//デストラクタ
	~NavigationAI();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//親からステージ情報をもらう
	//引数：ステージのポインタ
	//戻り値：なし
	void SetStage(Stage* pStage);

	//プレイヤーのポインタをもらう
	//引数：プレイヤーのオブジェクトのポインタ
	//戻り値：なし
	void SetPlayer(Player* pPlayer) { pPlayer_ = pPlayer; }

	//位置のリストのポインタをもらう
	//引数：位置のリストのポインタ
	//戻り値：なし
	void SetPosList(std::list<PosInfo>* pos) { pos_ = pos; }

	//プレイヤーを探す関数達を呼び出す
	//引数：なし
	//戻り値：なし
	void SearchTargetPos();

	//敵の位置をもらう
	//引数：敵の位置
	//戻り値：なし
	void SetEnemyPos(D3DXVECTOR3 pos) { enemyPos_ = pos; }

	//引数で指定された位置の部品タイプを教える
	//引数：zの位置、xの位置
	//戻り値：指定された位置の部品タイプ
	int GetStage(int z, int x) { return stage_[z][x].type_; }

	//床の位置リストのポインタを渡す
	//引数：なし
	//戻り値：床の位置リスト
	std::vector<CharaPos>* GetFloorList() { return &floorList_; }

	//A*アルゴリズム法でプレイヤーの位置までの最短経路を調べる
	//引数：敵の現在地の情報、探索開始時のプレイヤーの位置、調査中リスト、調査済みリスト
	//戻り値：なし
	void Search(Info currentPos, CharaPos playerPos, std::vector<Info>* openList, std::vector<Info>* closeList);

	//通る道を記録させ、調査済みリストから通るべき位置を探す
	//引数：敵の現在地の情報、調査済みリスト
	//戻り値：なし
	void Mark(Info currentPos, std::vector<Info>* closeList);

	//通る位置を記録する
	//引数：位置や距離、方向が１つになった構造体、yの位置（キャラクターの半径）
	//戻り値：なし
	void addMark(Info pos, float y);
};