#pragma once
#include <vector>
#include "Engine/IGameObject.h"

class Floor;
class Wall;

//Stageを管理するクラス
class Stage : public IGameObject
{
private:
	//ステージとして表示する種類
	enum
	{
		TYPE_WALL = 0,	//壁
		TYPE_FLOOR,		//床
		TYPE_PLAYER,	//プレイヤーの初期位置
		TYPE_ENEMY,		//敵の初期位置
		TYPE_MAX
	};

	//ステージ
	int stage_[HEIGHT][WIDTH];

	//ステージ上のクリスタルの数
	int crystalCount_;

	//ステージの部品ごとの位置
	D3DXVECTOR3 partsPos_[HEIGHT][WIDTH];

public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//レイを撃った時に当たったモデルの番号を取得
	//引数：調べたい位置
	//戻り値：引数の位置の真下に存在するモデル
	int GetModelHandle(float x, float z) { return stage_[(int)x][(int)z]; }

	//床、壁などの部品の位置を返す
	//引数：調べたい位置
	//戻り値：引数の位置の真下に存在するモデルの位置
	//D3DXVECTOR3 GetPartsPos(int x, int z) { return partsPos_[x][z]; }
	D3DXVECTOR3 GetPartsPos(float x, float z, int type);

	//引数で渡された場所のステージ情報を渡す
	//引数：ステージのx位置とz位置
	//戻り値：指定された場所のステージ情報
	int GetStage(int x, int z) { return stage_[x][z]; }

	//クリスタルの数を教える
	//引数：なし
	//戻り値：クリスタルの数
	int GetCrystalCnt() { return crystalCount_; }
};
