#include "AI.h"

//コンストラクタ
AI::AI(IGameObject * parent, std::string name)
	:IGameObject(parent, name)
{
}

//デストラクタ
AI::~AI()
{
}

//初期化
void AI::Initialize()
{
}

//更新
void AI::Update()
{
}

//描画
void AI::Draw()
{
}

//開放
void AI::Release()
{
}
