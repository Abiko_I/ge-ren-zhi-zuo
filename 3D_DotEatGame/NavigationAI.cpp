#include "NavigationAI.h"
#include "Stage.h"
#include "Enemy.h"
#include "Player.h"

//コンストラクタ
NavigationAI::NavigationAI(IGameObject * parent)
	:AI(parent, "NavigationAI"), enemyPos_(0.0f, 0.0f, 0.0f)
{
}

//デストラクタ
NavigationAI::~NavigationAI()
{
}

//初期化
void NavigationAI::Initialize()
{
	//自分が持つステージの初期化
	for (int i = 0; i < HEIGHT; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			stage_[i][j].cost_ = 99;
			stage_[i][j].type_ = -1;
			stage_[i][j].moveFlag_ = false;
		}
	}
}

//更新
void NavigationAI::Update()
{
}

//描画
void NavigationAI::Draw()
{
}

//開放
void NavigationAI::Release()
{
}

//ステージ情報をもらい、コピーする
void NavigationAI::SetStage(Stage * pStage)
{
	pStage_ = pStage;

	for (float i = 0; i < HEIGHT; i += 1.0)
	{
		for (float j = 0; j < WIDTH; j += 1.0)
		{
			//ステージの情報をもらう
			stage_[(int)i][(int)j].type_ = pStage_->GetStage((int)i, (int)j);

			//敵の初期位置が記されていたら
			if (stage_[(int)j][(int)i].type_ == TYPE_ENEMY)
			{
				//教える
				((Enemy*)pParent_)->SetStartPos(j, i);

				//敵の初期位置も床に該当するので位置を保存
				CharaPos pos = { j, i };
				floorList_.push_back(pos);
			}
			//床のみの位置なら
			else if (stage_[(int)j][(int)i].type_ == TYPE_FLOOR)
			{
				//位置を保存
				CharaPos pos = {j, i};
				floorList_.push_back(pos);
			}
		}
	}
}

//プレイヤーを探す関数達を呼び出す
void NavigationAI::SearchTargetPos()
{
	D3DXVECTOR3 playerPos = pPlayer_->GetPosition();

	enemyPos_ = pParent_->GetPosition();

	//探索の初期位置の情報
	Info pos = { (int)round(enemyPos_.z), (int)round(enemyPos_.x), 0, 0, 0, 0, 0, NONE };

	//プレイヤーの位置を構造体にまとめる
	pPos_ = { (int)round(playerPos.z), (int)round(playerPos.x) };

	//リストを空にする
	openList_.clear();
	closeList_.clear();
	pos_->clear();

	//終点に着いていないのなら
	if (!(pos.z_ == pPos_.z_ && pos.x_ == pPos_.x_))
	{
		//初期値を調査中リストに追加
		openList_.push_back(pos);
	}

	//プレイヤーを探す
	Search(pos, pPos_, &openList_, &closeList_);
}

//A*アルゴリズム法でプレイヤーの位置までの最短経路を調べる
void NavigationAI::Search(Info currentPos, CharaPos playerPos, std::vector<Info>* openList, std::vector<Info>* closeList)
{
	//上下左右の位置を記録する
	Info nextPos[4];

	for (int i = 0; i < 4; i++)
	{
		//有り得ない値で初期化
		nextPos[i].z_ = -1;
		nextPos[i].x_ = -1;
		nextPos[i].dist_ = 9999;
		nextPos[i].est_ = 9999;
		nextPos[i].realDist_ = 9999;
		nextPos[i].prevZ_ = -1;
		nextPos[i].prevX_ = -1;
		nextPos[i].dir_ = NONE;
	}

	//再帰処理をする時に引数として渡す
	Info retPos = { 0, 0, 0, 0, 9999, 0, 0, NONE };

	//上下左右を探索
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			//上下
			if (j == 0)
			{
				nextPos[i].z_ = currentPos.z_ + searchPos_[i][j];
			}
			//左右
			else
			{
				nextPos[i].x_ = currentPos.x_ + searchPos_[i][j];
			}
		}

		//カウント数を見て方向を決定
		switch (i)
		{
		case 0:
			nextPos[i].dir_ = BACKWARD;
			break;

		case 1:
			nextPos[i].dir_ = LEFT;
			break;

		case 2:
			nextPos[i].dir_ = RIGHT;
			break;

		case 3:
			nextPos[i].dir_ = FORWARD;
			break;
		}

		//調査中リストにあるかどうか
		bool isExistOpen = false;

		//調査済みリストにあるかどうか
		bool isExistClose = false;

		//始点かどうか
		bool isStartPos = false;

		//計算中リストに、今見ている位置と同じ情報があるなら
		for (auto it = openList->begin(); it < openList->end(); it++)
		{
			if ((it->z_ == nextPos[i].z_) && (it->x_ == nextPos[i].x_))
			{
				//計算中リストにある
				isExistOpen = true;
			}
		}

		//探索済みリストに、今見ている位置と同じ情報があるなら
		for (auto it = closeList->begin(); it < closeList->end(); it++)
		{
			if ((it->z_ == nextPos[i].z_) && (it->x_ == nextPos[i].x_))
			{
				//探索済みリストにある
				isExistClose = true;
			}
		}

		//始点であるなら
		if (nextPos[i].z_ == (int)round(position_.z) && nextPos[i].x_ == (int)round(position_.x))
		{
			isStartPos = true;
		}

		//壁でなく、調査中リストになく、調査済みリストになく、始点でない位置の時は
		if (stage_[nextPos[i].z_][nextPos[i].x_].type_ != TYPE_WALL &&
			!isExistOpen &&
			!isExistClose &&
			!isStartPos)
		{
			//実歩数を計算
			nextPos[i].dist_ = currentPos.dist_ + 1;

			//目的地までの推定値を計算
			nextPos[i].est_ = abs(nextPos[i].z_ - playerPos.z_) + abs(nextPos[i].x_ - playerPos.x_);

			//目的地までの推定歩数を計算
			nextPos[i].realDist_ = nextPos[i].dist_ + nextPos[i].est_;

			//前の位置を保存
			nextPos[i].prevZ_ = currentPos.z_;
			nextPos[i].prevX_ = currentPos.x_;

			//調査中リストに追加
			openList->push_back(nextPos[i]);
		}
	}

	//上下左右を探索したら、調査終了
	closeList->push_back(currentPos);

	//計算中リストから同じ位置を探して
	for (auto it = openList->begin(); it < openList->end(); it++)
	{
		if ((it->z_ == currentPos.z_) && (it->x_ == currentPos.x_))
		{
			//調査中リストから削除
			openList->erase(it);
			break;
		}
	}

	//計算中リスト中の最小推定実歩数のものを選択
	for (auto it = openList->begin(); it < openList->end(); it++)
	{
		//現在見ている情報の推定実歩数が
		//引数として使おうとしている情報の中の推定歩数より小さいなら
		if (retPos.realDist_ > it->realDist_)
		{
			//引数として使う情報を更新
			retPos = *it;
		}
	}

	//終点に着いたのなら
	if ((retPos.z_ == playerPos.z_) && (retPos.x_ == playerPos.x_))
	{
		//終点から逆探索して通る道に印をつける
		Mark(retPos, closeList);

		//関数を終了
		return;
	}
	//まだ終点ではないのなら
	else
	{
		//再帰呼び出し
		Search(retPos, playerPos, openList, closeList);
	}
}

//通る道を記録させ、調査済みリストから通るべき位置を探す
void NavigationAI::Mark(Info currentPos, std::vector<Info>* closeList)
{
	//その位置に印をつける
	addMark(currentPos, enemyPos_.y);

	//再帰させるか
	bool isCallFunc = false;

	//前の位置が始点ではない時
	if (!((currentPos.prevZ_ == (int)round(enemyPos_.z)) && (currentPos.prevX_ == (int)round(enemyPos_.x))))
	{
		//再帰させる
		isCallFunc = true;
	}

	//再帰させるなら
	if (isCallFunc)
	{
		//計算済みリストから、今の位置の前の位置と同じ位置を探して
		for (auto it = closeList->begin(); it < closeList->end(); it++)
		{
			if ((it->z_ == currentPos.prevZ_) && (it->x_ == currentPos.prevX_))
			{
				//再帰で渡す引数を、今の位置の前の位置に更新
				currentPos = *it;
			}
		}

		//再帰呼び出し
		Mark(currentPos, closeList);
	}
}

//通る位置を記録する
void NavigationAI::addMark(Info pos, float y)
{
	//要素を格納
	PosInfo info;
	info.dir = pos.dir_;
	info.pos = D3DXVECTOR3(pos.x_, y, pos.z_);

	//先頭に追加
	//pos_->push_front(info);

	auto it = pos_->begin();

	if (pos_->size() > 0)
	{
		pos_->insert(it, info);
	}
	else
	{
		pos_->push_front(info);
	}
}